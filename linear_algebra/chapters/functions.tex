\section{Certesian product}
\begin{quote}
\begin{definition}[Certesian product]
The Certesian product of two sets \textit{A} and \textit{B} are the set of all pairs (a, b) where $a \in A$ and $b \in B$
\end{definition}

\begin{example}{Example of Certesian products}{}
\begin{small}

For all the sets $A = \lbrace 1, 2, 3\rbrace $ and $B= \lbrace \heartsuit , \spadesuit , \clubsuit , \diamondsuit \rbrace$, the Certesian product will be 
$$
\lbrace (1, \heartsuit ), (2, \heartsuit ), (3, \heartsuit ), (1, \spadesuit ), (2, \spadesuit ), (3, \spadesuit ), (1, \clubsuit ), (2, \clubsuit ), (3, \clubsuit ), (1, \diamondsuit ), (2, \diamondsuit ), (3, \diamondsuit ) \rbrace
$$

\end{small}
\end{example}
\end{quote}

\section{The function}
\begin{quote}
\begin{definition}[Function]
Let A and B be nonempty sets. A function \textbf{f} from A to B is an assignment of exactly one element of B to each element of A.
\begin{quote}
Note: functions are sometimes called \textbf{mappings} or \textbf{transformations}
\end{quote}
\end{definition}


Formally, a \textit{function} is a (possibly infinite) set of pairs \textit{(a,b)} where each input has exactly one output.


\begin{example}{Example of doubling function}{}
The doubling of function with domain $\lbrace 1,2,3, ... \rbrace$ is
$$
\lbrace (1,2), (2,4), (3,6), (4,8),...\rbrace
$$
The domain (of outputs) consists of pairs of numbers
\end{example}

\begin{example}{Example of Multiplication function}{}
The multiplication function with domain $\lbrace 1,2,3, ... \rbrace \times \lbrace1,2,3,...\rbrace$ is
$$
\lbrace ((1,1),1),((1,2),2), ... , ((2,1),2), ((2,2),4), ((2,3),6),... \rbrace
$$
\end{example}

\end{quote}

\subsection{Function notations}
\begin{quote}

\begin{myformula}{\textit{q} under \textit{f}}{}
For a function named \textit{f}, the image of \textit{q} under \textit{f} is denoted by
$$
f(q)
$$
\textit{q} is the input into function \textit{f}
\end{myformula}

\begin{myformula}{\textit{q} maps to \textit{r} under \textit{f}}{}

$$
r = f(q)
$$
Here we say that "\textit{q maps to r under f}". This means \textit{q} is the same as \textit{r} when it has been passed through function \textit{f}. We could also write it as
$$
f(q) = r
$$

\end{myformula}

\begin{myformula}{\textit{q} maps to \textit{r}}{}
The notation for "\textit{q} maps to \textit{r}" is
$$
q \mapsto r
$$
 This notation omits specifying the function; it is useful when there is no ambiguity about which function is intended.
\end{myformula}

\begin{myformula}{Function co-domains}{}
$$
f : D \longrightarrow F
$$
This notation means that \textit{f} is a function whose domain is the set \textit{D} and whose co-domain (the set of possible outputs) is the set \textit{F}.

More briefly:
\begin{itemize}
\item "a function from \textit{D} to \textit{F}"
\item "a function that maps \textit{D} to \textit{F}"
\end{itemize}
\end{myformula}


\begin{small}
\begin{extra}{Speaking of functions (overview)}{}

\begin{center}
\includegraphics[width=0.5\textwidth]{images/function-mapping.png}
\end{center}

\begin{itemize}
\item If \textit{f} is a function from \textit{A} to \textit{B}, we say:
\begin{itemize}
\item \textit{A} is the \textbf{domain} of \textit{f} (containing all the possible inputs of \textit{f})
\item \textit{B} is the \textbf{codomain} of \textit{f} (containing all the possible outputs of \textit{f}).
\item \textit{f} \textbf{maps} \textit{A} to \textit{B}
\end{itemize}
\item If $f(a) = b$, we say: 
\begin{itemize}
\item  \textit{b} is the \textbf{image} of \textit{a} and \textit{a} is a pre-\textbf{image} of \textit{b}. 
\item  \textit{a} is the \textbf{input} of the function \textit{f}, and \textit{b} is the \textbf{output}
\end{itemize}
\item The \textbf{range}, or \textbf{image}, of \textit{f} is the set of all images of elements of \textit{A}.  
\end{itemize}
\end{extra}

\end{small}

\begin{example}{Ceasar cypher}{}
Ceasar was said to have used a cryptosystem in which each letter was replaced with the one three steps forward in the alphabet (wrapping around for X, Y, and Z). Thus the plaintext MATRIX would be encrypted as cyphertext PDWULA. The function that maps each plaintext letter to its cyphertext replacement would be written as
$$
A\mapsto D, B \mapsto E, C\mapsto F, D\mapsto G, ... , W\mapsto Z, X\mapsto A, Y\mapsto B, Z\mapsto C
$$
This function's domain and co-domain are both the alphabet $\lbrace A,B,...,Z\rbrace$.
\end{example}

\begin{example}{Cosine function}{}
The cosine function, \textit{cos}, maps from the set of real numbers (indicated by $\mathbb{R}$) to the set of real numbers. We would therefor write
$$
cos : \mathbb{R} \longrightarrow \mathbb{R}
$$
Of course, the outputs of the \textit{cos} function do not include all real numbers, only those between -1 and 1 (because cos always stays within that range).
\end{example}


\end{quote}


\section{Matrices and Vectors}
\begin{quote}
\begin{itemize}
\item A single number that might be used for multiplication is called a \textbf{scalar}, because it scales a matrix or vector by that number.
\item A \textbf{vector} is a \textit{list} of numbers (can be a row or column)
\item A \textbf{matrix} (or matrices for multiple) is an array of numbers, usually having multiple rows/columns.
\end{itemize}
\begin{center}
\includegraphics[width=0.6\textwidth]{images/matrix-vector-scalar.png}
\end{center}
And when you think about it, vectors are just single row/column matrices, and all the rules that apply to matrices can be applied to vectors.

\begin{extra}{Matrices and computer graphics}{}
Matrices are essential in computer graphics.  They allow us to perform operations on vectors such as:
\begin{itemize}
\item translations
\item transformations
\item rotations
\item scaling
\end{itemize}
So of course this is interesting and important if you like that subject. Everything that has to do with computers is based in math after all.
\end{extra}

\end{quote}


\subsection{Multiplying matrices and vectors together}
\begin{quote}

\begin{extra}{It is still just matrix multiplication}{}

It is good to keep in mind that these are simply two matrices, and you are multiplying them together, so their sizes need to match in certain dimensions: The width of the first matrix needs to match the height of the second matrix. 
\begin{itemize}
\item if the vector comes first, then it needs to match the matrix in height.
\item if the vector comes second, it needs to match the matrix in width.
\item Think of the vector elements as scalars
\item Vector doesn't care if it's a row or column. We just transpose it if needed.
\item Think of the matrix as multiple vectors (their orientation being rows if the vector goes first, or columns if the vector comes second).
\item When we're done scaling, we will add all the lines in the matrix  together as they were individual vectors, resulting in a single vector.
\end{itemize}
\end{extra}
\end{quote}
\subsubsection{Matrix-vector multiplication}
\begin{quote}
Every element in the vector will be used to scale the corresponding column in the matrix, and then we add all the columns together (as they were individual vectors).

It will result in a vector that is equal in length to the height of the matrix.

\begin{example}{Matrix-vector multiplication}{}
As the name implies, the matrix comes first.
\[
\begin{bmatrix}
	1 & 2 & 3 \\ 10 & 20 & 30
\end{bmatrix}
\cdot
\begin{bmatrix}
	7 & 0 & 4
\end{bmatrix}
=
\]
\[
7
\begin{bmatrix}
1\\10
\end{bmatrix}
+
0
\begin{bmatrix}
2\\20
\end{bmatrix}
+
4
\begin{bmatrix}
3\\30
\end{bmatrix}
=
\]
\[
\begin{bmatrix}
7\\30
\end{bmatrix}
+ 
\begin{bmatrix}
0\\0
\end{bmatrix}
+ 
\begin{bmatrix}
12\\120
\end{bmatrix}
=
\begin{bmatrix}
19\\190
\end{bmatrix}
\]
The vector doesn't care if it's a row or column, but I find it more natural to keep the orientation as they were in the matrix for less confusion.
\end{example}
\begin{example}{Doing it the matrix way}{}
We can also take a look at how the multiplication would be done as normal matrix multiplication. 

\[
\begin{bmatrix}
	1 & 2 & 3 \\ 10 & 20 & 30
\end{bmatrix}
\cdot
\begin{bmatrix}
	7 \\0\\ 4
\end{bmatrix}
=
\]
\[
\begin{bmatrix}
(1*7) + (2*0) + (3*4)\\
(10*7) + (20*0) + (30*4)
\end{bmatrix}
= 
\begin{bmatrix}
7+0+12\\70+0+120
\end{bmatrix}
= 
\begin{bmatrix}
19\\190
\end{bmatrix}
\]
The matrix multiplication cares about the orientation of the vector, we just transpose the vector if needed. 

It is good to know this, to explain why the resulting vector is of the size it is.
\end{example}

\end{quote}
\subsubsection{Vector-matrix multiplication}
\begin{quote}
Every element in the vector will be used to scale the corresponding row in the matrix, and then we add all the rows together (as they were individual vectors).

It will result in a vector that is equal in length as the width of the matrix.

\begin{example}{Vector-matrix multiplication}{}

\[
[3,4]
\cdot
\begin{bmatrix}
	1 & 2 & 3 \\ 10 & 20 & 30
\end{bmatrix}
=
\]
$$
3[1,2,3] + 4[10,20,30] =
$$
$$
[3,6,9] + [40,80,120] = [43,86,129]
$$

It is following the rules of the matrix multiplication: The vector is as wide as the matrix is tall, and the resulting matrix is as tall as the vector and as wide as the matrix. 
\end{example}

\begin{small}
\begin{example}{Vector-matrix, the matrix way}{}
This example simply shows how the in-between steps would be done in matrix multiplication.

\[
[3,4]
\cdot
\begin{bmatrix}
	1 & 2 & 3 \\ 10 & 20 & 30
\end{bmatrix}
=
\]
\[
\begin{bmatrix}
(3*1)+(4*10) & (3*2)+(4*20) & (3*3) + (4*30) 
\end{bmatrix}
=
\]
\[
\begin{bmatrix}
3+40 & 6+80 & 9+120
\end{bmatrix}
=
\begin{bmatrix}
43 & 86 & 129
\end{bmatrix}
\]
Previous example is actually simpler, visually at least, to reach the same conclusion.
\end{example}
\end{small}
\end{quote}

\subsection{Solving matrix-vector equation (linear systems)}
\begin{quote}

Let's say we have the linear combinations that would come from a matrix-vector multiplication, essentially the in-between steps, we could actually figure out what the matrix, vector and resulting vector would be.

\begin{small}

\begin{small}
\begin{example}{Solving a 2x2 matrix-vector equation}{}
We have the simple formula for doing matrix-vector multiplication:
\[
\begin{bmatrix}
a&b\\c&d
\end{bmatrix}
\cdot
[x,y] = [p,q]
\]
\begin{enumerate}
\item if $ad\neq bc$
$$
x = \dfrac{dp - cq}{ad - bc} \text{ and } y=\dfrac{aq-bp}{ad - bc}
$$
\item If we then use that to solve the following example
\[
\begin{bmatrix}
1&2\\3&4
\end{bmatrix}
\cdot [x,y] = [-1,1]
\]
\item We could solve for both \textit{x} and \textit{y}
$$
x = \dfrac{(4\cdot -1) - (2\cdot 1)}{(1\cdot 4)-(2\cdot 3)} = \dfrac{-6}{-2} = 3
$$
$$
y = \dfrac{(1\cdot 1) - (3\cdot - 1)}{(1\cdot 4) - (2\cdot 3)} = \dfrac{4}{-2} = -2
$$
\end{enumerate}
\end{example}
\end{small}

\begin{example}{Going from linear system to matrix-vector equation}{}
Let's find out \textit{A} and \textit{b} such that \textit{Ax = b}. We have the corresponding following linear system.
$$
4x_1 - 1x_2 + 6 x_3 = -9
$$
$$
-4x_1 + 3x_2 - 7x_3 = -4
$$
\begin{enumerate}
\item We should consider what is asked. \textit{A} is a matrix, \textit{x} is a vector of scalars, and \textit{b} is the resulting vector.
\item If we consider every formula in the system to be a row in a matrix, and we extract the scalars: $[x_1, x_2, x_3]$. 
\item And we want things to be in the format of $Ax = b$ (notice that we are not asked to solve x)
\[
\begin{bmatrix}
4 & -1 & 6\\
-4 & 3 & -7
\end{bmatrix}
\cdot
[x_1,x_2,x_3] = 
\begin{bmatrix}
-9\\
-4
\end{bmatrix}
\]
Hope you can spot what happened.

The matrix and resulting vector could be as many rows as we have equations
\end{enumerate}
\end{example}

\begin{example}{Vector-matrix multiplication in terms of linear combinations}{}
\begin{small}
We want to know how many of each item we're making, assuming that we know how much materials we need (\textit{b}). Then we are trying to solve \textit{x} in the equation
$x\cdot M = b$

\begin{center}M = 
\begin{tabular}{c | c c c c c}
&metal&concrete&plastic&water&electricity\\
\hline
statue & 0 & 1.3 & .2 & .8 & .4\\
hoop & 0 & 0 & 1.5 & .4 & .3\\
putty & 0 & 0 & .3 & .7 & .5\\
shooter & .15 & 0 & .5 & .4 & .8
\end{tabular}
\end{center}
$$
b = [\alpha_{statue}, \alpha_{hoop}, \alpha_{putty}, \alpha_{shooter}] \cdot M
$$
\begin{itemize}
\item Let \textit{M} be a matrix where each column is a type of material, and the rows are essentially recipes of items that can be created from those materials (listing how much of each material is needed to produce the item). 
\item The vector \textit{b} would be the total of each material we need to produce all the items we want to. The missing \textit{x} vector would hold information about how many of each item we would be producing then.
\item We should then be able to deduce how many of each item we're going to create. Remember that \textit{b}=[metal, concrete, plastic, water, electricity] 
\[
[\alpha_{statue}, \alpha_{hoop}, \alpha_{putty}, \alpha_{shooter}]
\cdot
\begin{bmatrix}
0 & 1.3 & .2 & .8 & .4\\
0 &0&1.5& .4 & .3\\
0&0&.3&.7 &.5\\
.15&0&.5&.4&.8
\end{bmatrix}
=
[b_1,b_2,b_3,b_4,b_5]
\]
\item We could convert it into a system of linear equations and solve for each variable $\alpha$. We are just combining the scalars $\alpha$ with the values of \textit{M} using matrix multiplication. 
\begin{align*}
0.15\alpha_{shooter} = b_1\\
1.3\alpha_{statue} = b_2\\
0.2\alpha_{statue} + 1.5\alpha_{hoop} + 0.3\alpha_{putty} + 0.5\alpha_{shooter} = b_3\\
0.8\alpha_{statue} + 0.4\alpha_{hoop} + 0.7\alpha_{putty} + 0.4\alpha_{shooter} = b_4\\
0.4\alpha_{statue} + 0.3\alpha_{hoop} + 0.5\alpha_{putty} + 0.8\alpha_{shooter} = b_5
\end{align*}
\item if \textit{b} was $[0.15, 1.3, 2.5, 2.3, 2]$, then we would be making one of each item.
\end{itemize}
\end{small}
\end{example}
\end{small}
\end{quote}

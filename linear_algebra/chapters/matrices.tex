\section{Fylki í fleirrtölu ('matrices')}
\begin{quote}
Ef \textit{A} er \textit{m$\times$n} fylki (m = línur, n = dálkar), þá er stak úr fylki A táknað sem {$a_{ij}$} (i = lína, j = dálkur).
\[
A = 
\begin{bmatrix}
	1 & 2 & 3 \\ 4 & 5 & 6
\end{bmatrix}
= 2\times3 \text{ fylki}
\]
\begin{enumerate}[  ]
	\item ef A*B er til, þá er fjöldi dálka í A = fjöldi lína í B
	\item ef B*A er til, þá er fjöldi dálka í B = fjöldi lína í A
	\item ef fjöldi dálka er jafn og fjöldi lína, þá kallast fylkið ferningsfylki
	\item Tengiregla gildir fyrir fylki, {$ (A*B) * C = A * (B * C)$}
	\item En víkslreglan gildir almennt ekki. {$ AB \neq BA $}
\end{enumerate}

\begin{extra}{Notation}{}
\begin{itemize}
\item A matrix is usually shown by a \textbf{capital letter} for a name
\item Placeholders for each element would be shown as the lower case letter of the name
\item The subscript would be indication of their position of the element within the matrix, as \textit{row} and \textit{column}
\item Rows are left to right
\item Columns are top to bottom
\end{itemize}
\begin{center}
\includegraphics[width=0.4\textwidth]{images/matrix-notation.png}
\end{center}

\end{extra}
\end{quote}

\subsection{Samlagning/Frádráttur fylkja ('addition/subtraction of matrices')}
\begin{quote}

\begin{definition}[Samlagning/Frádráttur fylkja ]

Ef {$A = [a_{ij}]$} og svo {$B = [b_{ij}]$} eru jafn stór, þá er hægt að leggja þau saman eða draga frá.
\begin{enumerate}[ ]
	\item A+B = {$[a_{ij} + b_{ij}]$}
	\item A-B = {$[a_{ij} - b_{ij}]$}
\end{enumerate}
\end{definition}

\end{quote}

\subsection{Núllfylki ('zero matrix')}
\begin{quote}

Núllfylki hefur öll stökin núll og táknað \textit{0}

Þá gildir: {$ A + 0 = 0 + A = A$} (þarf ekki að taka fram stærð, þar sem öll stök eru 0). Ekkert gerist ef þú leggur núllfylki við annað fylki, því núll bætir engu.

\begin{example}{Margföldun sem býr til núllfylki}{}
Þar sem hver röð er margfölduð með hverjum dálk, þá verða allar tölurnar paraðar með núlli, sem leiðir af sér að allir reitir verða núll.	
	\[
	\begin{bmatrix}
		5 & 0 \\ 2 & 0
	\end{bmatrix}
	*
	\begin{bmatrix}
		0 & 0 \\ 3 & 4
	\end{bmatrix}
	=
	\begin{bmatrix}
		(5*0)+(0*3) & (5*0)+(0*4) \\
		(2*0)+(0*3) & (2*0)+(0*4) 
	\end{bmatrix}
	=
	\begin{bmatrix}
		0 & 0 \\ 0 & 0
	\end{bmatrix}
	= 0
	\]
\end{example}

\begin{example}{Samlagning við Núllfylki}{}
\[
	A+0 = 
	\begin{bmatrix}
		1 & 3 \\ 2 & 3
	\end{bmatrix}
	+
	\begin{bmatrix}
		0 & 0 \\ 0 & 0
	\end{bmatrix}
	=
	\begin{bmatrix}
		1+0 & 3+0\\
		2+0 & 4+0
	\end{bmatrix}
	=
	\begin{bmatrix}
		1 & 3 \\ 2 & 4
	\end{bmatrix}
	= A
\]
\end{example}
\end{quote}

\subsection{Margföldun fylkja ('matrix product')}
\begin{quote}
\begin{itemize}
\item \href{https://www.mathsisfun.com/algebra/matrix-multiplying.html}{Mathisfun.com: Matrix multiplication}
\end{itemize}

\begin{definition}[Margföldun fylkja]
Til þess að hægt sé að margfalda fylki saman, þá verður \textbf{fjöldi dálka í A að vera jafn fjölda lína í B}. 

Ef \textit{A} er \textit{m$\times$k} fylki og B er \textit{k$\times$n} fylki, þá væri \textit{AB} = \textit{C} \textit{m$\times$n} fylki, 

þar sem {$c_{ij} = (a_{i1}*b_{1j}) + (a_{i2}*b_{2j}) + ... + (a_{ik}*b_{kj})$}
\end{definition}


\begin{example}{Multiplying A and B}{}

\[
AB = 
\begin{bmatrix}
	1 & 2 \\ 3 & 4 \\ 5 & 6
\end{bmatrix}
\begin{bmatrix}
	1 & 2 & 3 \\
	4 & 5 & 6
\end{bmatrix}
\]
\[
= 
\begin{bmatrix}
	(1*1)+(2*4) & (1*2)+(2*5) & (1*3)+(2*6) \\
	(3*1)+(4*4) & (3*2)+(4*5) & (3*3)+(4*6) \\
	(5*1)+(6*4) & (5*2)+(6*5) & (5*3)+(6*6) 
\end{bmatrix}
\]
\[
= 
\begin{bmatrix}
	1+8 & 2+10 & 3+12 \\
	3+16 & 6+20 & 9+24\\
	5+24 & 10+30 & 15+36
\end{bmatrix}
=
\begin{bmatrix}
	9 & 12 & 15 \\
	19 & 26 & 33 \\
	29 & 40 & 51
\end{bmatrix}
\]

\end{example}

\begin{extra}{Summarising properties of matrix multiplication}{}

Let's just reference the matrix multiplication as $A\cdot B = C$ 
\begin{itemize}
\item The number of \textbf{columns} in \textit{A} needs to be equal to number of \textbf{rows} in \textit{B}
\item The \textbf{height} of \textit{A} doesn't matter (it becomes the height of \textit{C})
\item The \textbf{width} of \textit{B} doesn't matter (it becomes the width of \textit{C})
\item Every element $c_{ij}$ is a \textbf{dot product} of row $i$ in \textit{A} and column \textit{j} in \textit{B}

\begin{center}
\includegraphics[width=0.3\textwidth]{images/matrix-dot-product.png}
\end{center}

\end{itemize}
\end{extra}


\begin{definition}[Dot product]
The dot product (or scalar product) is an algebraic operation that takes two equal length sequences of numbers (usually vectors) and returns a single number. That number depends on the length of both vectors and their angles. A dot product essentially tells us how much of the force vector is applied in the direction of the motion vector.
\end{definition}

\end{quote}



\subsection{Veldi fylkja ('matrix powers')}
\begin{quote}
Ef \textit{A} er \textit{n$\times$n} fylki (ferningsfylki) þá er hægt að skilgreina veldi af \textit{A}. 
\begin{enumerate}[ ]
	\item {$A^0 = I_n$}
	\item {$A^r = \underbrace{AAA... A }_\text{\textit{r} þættir}$}
	\item {$ A^2 = A * A
	\\A^3 = A * A^2$}
\end{enumerate}
\end{quote}

\subsection{Einingarfylki ('identity matrix')}
\begin{quote}

\begin{definition}[Einingarfylki]
Einingarfylki af stærð \textit{n$\times$n} (ferningsfylki) kallast {$ I_n$}. Það hefur \textit{1} á hornlínunni en \textit{0} annarstaðar.
\end{definition}
\[
I_2 =
\begin{bmatrix}
	1 & 0 \\ 0 & 1
\end{bmatrix}
, I_3 =
\begin{bmatrix}
	1 & 0 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 1
\end{bmatrix}
\]
Þegar fylki er margfaldað með einingarfylki af viðeigandi stærð þá breytist fylkið ekki. 
\\Ef A er \textit{m$\times$n} fylki, þá gildir 
{$$ AI_n = I_mA = A$$}

\begin{example}{The identity of A is A}{}
\begin{small}
\[
A = 
\begin{bmatrix}
	1 & 2 & 3 \\ 4 & 5 & 6
\end{bmatrix}
\]
\[
I_2 * A = 
\begin{bmatrix}
	1 & 0 \\ 0 & 1
\end{bmatrix}
\begin{bmatrix}
	1 & 2 & 3 \\ 4 & 5 & 6
\end{bmatrix}
\]
\[
=
\begin{bmatrix}
	(1*1)+(0*4) & (1*2)+(0*5) & (1*3)+(0*6) \\
	(0*1)+(1*4) & (0*2)+(1*5) & (0*3)+(1*6)
\end{bmatrix}
= 
\begin{bmatrix}
	1 & 2 & 3 \\ 4 & 5 & 6
\end{bmatrix}
= A
\] 
\end{small}

\end{example}

\end{quote}

\subsection{Að bylta fylki ('transpose')}
\begin{quote}

\begin{definition}[Bylta fylki]

Ef {$ A = [a_{ij}]$} er \textit{m$\times$m} fylki að stærð, \\
þá er {$A^t = B = [b_{ij}]$} \textit{n$\times$m} fylki, þar sem {$b_{ij} = a_{ji}$}

Þannig að við getum sagt að það snýr það fylkinu á hlið þannig að \textit{dálkur 1} verður að \textit{röð 1} og koll af kolli. Táknað sem {$ A^t $}
\end{definition}

\begin{example}{Transposing a matrix}{}
\[
A =
\begin{bmatrix}
	1 & 2 & 3 \\ 4 & 5 & 6
\end{bmatrix}
, A^t = 
\begin{bmatrix}
	1 & 4 \\ 2 & 5 \\ 3 & 6
\end{bmatrix}
, (A^t)^t = 
\begin{bmatrix}
	1 & 2 & 3 \\ 4 & 5 & 6
\end{bmatrix}
= A
\]

\textit{A} er 2$\times$3 fylki og {$A^t$} er 3$\times$2 fylki. 
\end{example}
\begin{itemize}
\item hver einasta röð verður að dálk: fyrsta röð verður fyrsti dálkur, önnur röð verður annar dálkur... Það myndi halda áfram þannig, ef fylkið er lengra
\item Að bylta byltu fylki gefur þér upphaflega fylkið, þá er maður komin aftur á byrjun.
\end{itemize}
\end{quote}
\subsection{Samhverf fylki ('symmetric matrix')}
\begin{quote}

\begin{definition}[Samhverf fylki ('symmetric matrix')]
\textit{A} er ferningsfylki, \\
{$ A = [a_{ij}]$} er samhverft ef {$a_{ij} = a_{ji}$}, þ.e. að stökin sitt hvoru megin við hornlínuna (\textit{'diagonal'}) speglast. Einingarfylki eru einnig samhverf samkvæmt þessu.

\end{definition}
\begin{example}{Speglast á hornlínuna}{}
Í þessu dæmi er \textit{A} samhverft fylki. 
\[
A = 
\begin{bmatrix}
	\underline{2} & 8 & 5 \\
	8 & \underline{7} & 4 \\
	5 & 4 & \underline{9}
\end{bmatrix}
\] 
Hornlínan er undirstrikuð, og þá sést að allar tölurnar eru eins sem speglast yfir hornlínuna. 
\end{example}
\end{quote}

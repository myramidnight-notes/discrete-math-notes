
\section{Linear dependencies}
\begin{quote}
\begin{definition}[Linear independence]
A set of vectors is said to be \textbf{linearly dependent} if there is a non-trivial linear combination of the vectors that equals the zero vector. If there exists no such linear combination, then the vectors are said to be \textbf{linearly independent}
\end{definition}


\begin{extra}{Linear dependence}{}
Vectors $\mathbf{v}_1,...,\mathbf{v}_n$ are \textbf{linearly dependent} if the zero vector can be written as a \textit{non-trivial} linear combination of the vectors:
$$
0 = \alpha_1\mathbf{v}_1 + ... + \alpha_n \mathbf{v}_n
$$
\begin{itemize}
\item In this case, we refer to the linear combination as a \textbf{linear dependency} in $\mathbf{v}_1, ..., \mathbf{v}_n$
\end{itemize}
On the other hand, if the \textit{only} linear combination that equals the zero vector is the trivial linear combination, then we say that $\mathbf{v}_1,...,\mathbf{v}_n$ are \textbf{linearly independent}
\end{extra}


Just as a reminder, \textit{trivial} would mean zero. 

\begin{itemize}
\item It essentially means that if the \textit{only} way for you to get the zero vector from the linear combination is by having all the coefficients/scalars be zero, then the set is \textbf{linearly independent}
\item If you \textit{can} get the zero vector from the linear combination where at least one of the scalars is non-zero, then the set is \textbf{linearly dependent}.
\end{itemize}

\begin{example}{Linearly dependent}{}
If we have the set of vectors 
$$
\lbrace[1,0,0], [0,2,0], [2,4,0]\rbrace
$$
And to show that they are linearly dependent, we will create the zero vector with at least one non-trivial scalar in a linear combination of the vectors:

\begin{align*}
2[1,0,0] + 2[0,2,0] - 1[2,4,0] = [0,0,0]\\
\Rightarrow [2,0,0] + [0,4,0] - [2,4,0] = [0,0,0]\\
\Rightarrow [2,4,0] - [2,4,0] = [0,0,0]
\end{align*}
\end{example}


\begin{example}{Linear dependence}{}
\begin{small}

If we have the set of vectors in $\mathbb{R}^2$
\[
S=
\left\{
\begin{bmatrix}
2\\ 3
\end{bmatrix}
, 
\begin{bmatrix}
4\\6
\end{bmatrix}
\right\}
\]
A span of those vectors are simply all vectors we might write as linear combinations of those vectors. Or in other words, if we add scalars to them to make a new vector, that new vector would be in the span of those vectors.
\begin{align*}
\alpha_1 
\begin{bmatrix}
2\\ 3
\end{bmatrix} 
+
\alpha_2 
\begin{bmatrix}
4\\6
\end{bmatrix}
\end{align*}
In this case, we can see that the vector $s_2$ is just a scaled version of $s_1$, scaled by 2 actually.
\begin{align*}
\alpha_1 
\begin{bmatrix}
2\\ 3
\end{bmatrix} 
+
\alpha_2 2
\begin{bmatrix}
2\\3
\end{bmatrix}\\
\Rightarrow
(\alpha_1 + 2\alpha_2)
\begin{bmatrix}
2\\ 3
\end{bmatrix} \\
\Rightarrow \alpha_3 
\begin{bmatrix}
2\\ 3
\end{bmatrix}
\end{align*}

So even if we started with two vectors in S, it will still be the same span if we remove $s_2$, because it is just $s_1$ scaled. So if we would graph any vector from that span will be overlapping vectors tracing a straight line. The vectors in the span are \textit{collinear} (they are multiples of each other).\\

This defines the set S as \textbf{linearly dependent}. No matter what scalars we use, we cannot escape the line that their vectors will draw. We are not really adding any new information when creating new vectors in the span, we are just scaling the single base vector\\

Same goes for vectors in $\mathbb{R}^3$. Two vectors would define a plane, and if their span all fall onto the same plane, then they are \textbf{linearly dependent}, because any new vector in that span would not give us any new information (the plane is already known).\\
\begin{footnotesize}
\href{https://www.khanacademy.org/math/linear-algebra/vectors-and-spaces/linear-independence/v/linear-algebra-introduction-to-linear-independence}{Introduction to linear independence (khan academy)}
\end{footnotesize}
\end{small}
\end{example}

\begin{example}{Linear dependence}{}
\begin{small}
What about these vectors? Is this span linearly dependent or not?
\begin{align*}
S =
\left\{
\begin{bmatrix}
2\\3
\end{bmatrix}
,
\begin{bmatrix}
7\\2
\end{bmatrix}
,
\begin{bmatrix}
9\\5
\end{bmatrix}
\right\}
\end{align*}
\begin{itemize}
\item $\vec{s}_1$ and $\vec{s}_2$ aren't multiples of each other.
\item $\vec{s}_1$ and $\vec{s}_3$ aren't multiples of each other.
\item $\vec{s}_2$ and $\vec{s}_3$ aren't multiples of each other.
\item However, if we look closer
$$
\vec{s}_1 + \vec{s}_2 = \vec{s}_3
$$
We can see that the 3rd vector is a combination of the other two. So it is a \textbf{linearly dependent} set of vectors.
\item So that means $\lbrace\vec{s}_1, \vec{s}_2\rbrace$ is a basis for S
\end{itemize}
\begin{footnotesize}
\href{https://www.khanacademy.org/math/linear-algebra/vectors-and-spaces/linear-independence/v/linear-algebra-introduction-to-linear-independence}{Introduction to linear independence (khan academy)}\\
\end{footnotesize}
\end{small}
\end{example}
\end{quote}


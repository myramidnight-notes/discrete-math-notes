# Linear Algebra with Computer Science
> The course is taught using material from [Coding the Matrix: Linear Algebra through Computer Science Applications](https://codingthematrix.com/)

## Index
* [Linear Algebra notes (pdf)](glosur_linear_algs.pdf)


## Teacher notes index 
>I will not include the slides from the course here, but I felt the need to index them for myself (so I can look things up in them, and perhaps combine them into my own notes)

* ### 0: The function
* ### 1: The field
    > Complex/Imaginary numbers, abstraction, Galios Field (GF)
* ### 2.1 - 2.6: The Vector
    > vectors are functions, sparsity, zero vector, vector math, scalars
* ### 2.6 - 2.9: Combining vector addition and scalar multiplication
    > Vectors over GF, Dot products, Linear equations
* ### 3.1: The vector space
    > Linear combinations
* ### 3.2: The Span
    > Generators, 
* ### 3.3: Geometry of sets of vectors
    >spans of vectors
* ### 3.4: Abstract vector spaces
* ### 3.5: Geometric objects excluding the origin
    > Affine space and affine combinations
* ### 3.6: Geometric objects excluding the origin: equations
    >Affine spaces, linear systems, checksum, convex hull
* ### 4.1: The Matrix
    >List of row-lists, lists of colum-lists, using dictionaries in python for matrices and vectors.
* ### 4.2 - 4.4: Column and Row space, Transpose 
* ### 4.5: Vector/Matrix multiplication
* ### 4.6: Matrix-vector multiplication in terms of dot-products
# Exponent rules
## Products of powers (Multiplication)
>__4<sup>2</sup> * 4<sup>5</sup>__ = 4<sup>2+5</sup> = 4<sup>7</sup>

>__(4x<sup>2</sup>)(2𝒙<sup>3</sup>)__ <br>= (4\*2)x<sup>2+3</sup> <br>= 8𝒙<sup>5</sup>

## Quotient of powers (Division)
> 5<sup>5</sup> / 5<sup>3</sup> = 5<sup>5-3</sup> = 5<sup>2</sup>

> __5x<sup>4</sup> / 10x<sup>2</sup>__ <br>= 1x<sup>4</sup> / 2x<sup>2</sup> <br>= x<sup>4</sup> / 2x<sup>2</sup> <br>= x<sup>4-2</sup>/2 <br>= x<sup>2</sup>/2

## Power of Power rule
> __(x<sup>3</sup>)<sup>3</sup>__ = x<sup>3*3</sup> = x<sup>9</sup>

## Power of product rule
>(xy)<sup>3</sup> = x<sup>3y<sup>3</sup>

>(x<sup>2</sup>y<sup>2</sup>)<sup>3</sup> <br>= x<sup>2\*3</sup>y<sup>2\*3</sup> <br>= x<sup>6</sup>y<sup>6</sup>

## Power of Quotient rule
>(4x<sup>3</sup> / 5y<sup>4</sup>)<sup>2</sup> <br>= 4<sup>2</sup>x<sup>6</sup> / 5<sup>2</sup>y<sup>8</sup> <br>= 16x<sup>6</sup> / 25y<sup>8</sup>

## Zero rule
>Anything that has the exponent `0` will be equal to `1`

>x<sup>0</sup> = 1

>(8<sup>2</sup>x<sup>4</sup>y<sup>6</sup>)<sup>0</sup> = 8<sup>0</sup>x<sup>0</sup>y<sup>0</sup> = 1\*1\*1 =  1

## Negative exponent
>The goal of equations with negative exponents is to __make them positive__

>4x<sup>-3</sup>y<sup>2</sup>/20xz<sup>-3</sup> <br>= x<sup>-3</sup>y<sup>2</sup> / 5xz<sup>-3</sup> <br> = y<sup>2</sup>z<sup>3</sup>/5x<sup>4</sup> 
>>x<sup>-3</sup> moves to the denominator (below the line), while z<sup>-3</sup> moves to to the numerator (above the line)
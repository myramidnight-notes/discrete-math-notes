
### Course book and notes
>* [__Active Calculus__](https://activecalculus.org/single/book-1.html) (The course _book_ that these notes are based on)
>   * You can find interactive problems to solve in the digital book
>* These notes were originally distributed by the math teacher, but they have some extra scribbles I have added (opening pdf in the Edge browser offers nice tools for that)
>
>I might add a extra `.md` file here and there with extra understanding

## My latex notes
> The notes I've put together myself 
* [Derivative and Integrals (pdf)](tex_notes/glosur_math_analysis.pdf)
* [Differential equations (pdf)](tex_notes/differential_equations/glosur_differential_equations.pdf)
* [Exponent/Logarithm (pdf)](tex_notes/expo_log/glosur_expo_log.pdf)

# Index to teachers notes
This for the most part is a index to the __teachers notes__, I have added my own scribbles and thoughts to them. Anything not tagged as a PDF would be my own extra notes.
## Basics
* Extra:
    >* [Formula Sheet (pdf)](formula_sheet.pdf) 
    >* [Exponent rules](exponent-rules.md)
* 0.1: [Sets and Lines (pdf)](teacher_notes/0-1.pdf)
    >* Absolute value 
    >* Distance, Slopes
    >* Equation of the line
* 0.2: [Functions (pdf)](teacher_notes/0-2.pdf)
    >* Polynomials
    >* Rational functions
* 0.3: [Trigonometric functions (pdf)](teacher_notes/0-3.pdf)
    >* Angles, Radians
    >* Cos, Sin, Tan
* 0.4: [Exponential and Logarithm functions (pdf)](teacher_notes/0-4_logarithm.pdf)
    >* Power rules
    >* Natural logarithm (ln)

## Understanding the Derivative
* 1.1: [Slopes (pdf)](teacher_notes/1-1_slopes.pdf)
* 1.2: [Limits (pdf)](teacher_notes/1-2_limits.pdf)
    >* The notion of limit
* 1.3: [Derivative of a function at a point (pdf)](teacher_notes/1-3.pdf)
    >* Average rate of change
    >* Tangent line
* 1.4: [Derivative function](teacher_notes/1-4.pdf)
    >* Derivative of a function
* 1.5: [Interpreting, estimating and using derivative (pdf)](teacher_notes/1-5.pdf)
    >* Central difference approximation
* 1.6: [Second derivative (pdf)](teacher_notes/1-6.pdf)
    >* Increasing or decreasing
    >* Concavity
* 1.7: [Limits, continuity and differentiability (pdf)](teacher_notes/1-7.pdf)    
    >* Having a limit at a point
    >* Continuous functions
    >* Being differentiable at a point
* 1.8: [Tangent line approximation (pdf)](teacher_notes/1-8.pdf)

## Computing Derivatives
* 2.1: [Elementary derivative rules (pdf)](teacher_notes/2-1.pdf)
    >* Notation
    >* Constant, Power and Exponential functions
    >* Constant multiples and sums of functions
* 2.2: [Sine and Cosine functions (pdf)](teacher_notes/2-2.pdf)
* 2.3: [Product and Quotient rules (pdf)](teacher_notes/2-3.pdf)
    >* Combining rules
* 2.4: [Derivatives of other trigonometric functions (pdf)](teacher_notes/2-4.pdf)
    >* Tangent, Cotangent, Secant and Cosecant rules
* 2.5: [Chain rule](teacher_notes/2-5.pdf)
* 2.6: [Derivatives of inverse functions (pdf)](teacher_notes/2-6.pdf)
    >* Basic facts about inverse functions
    >* Derivative of natural logarithm functions
* 2.7: [Derivatives of functions given implicitly (pdf)](teacher_notes/2-7.pdf)
    >* Implicit equation
    >* Implicit differentiation
* 2.8: [Using derivatives to evaluate limits (pdf)](teacher_notes/2-8.pdf)
    >* L'hôpital's Rule

## Using Derivatives
* 3.1: [Using derivatives to identify extreme values (pdf)](teacher_notes/3-1.pdf)
    >* Global,Absolute or relative minimum/maximum
    >* Critical numbers
    >* First and Second derivative test, 
* 3.3: [Global Optimization (pdf)](teacher_notes/3-3.pdf)
    >Finding maxima/minima of a continous function on a closed interval
    >* The extreme value theorem
* 3.4: [Applied optimization (pdf)](teacher_notes/3-4.pdf)
    >* Using it to calculate volume/quantity
* 3.5: [Related rates (pdf)](teacher_notes/3-5.pdf)
    >* Equation connecting two quantities

## Definite Integral
* 4.1: [Determining distance traveled from velocity (pdf)](teacher_notes/4-1.pdf) 
    >* Area under the graph of velocity function
    >* Two approaches: area and anti-differentiation
    >* When velocity is negative
* 4.2: [Riemann Sums (pdf)](teacher_notes/4-2.pdf)
    > _Getting the approximate area under a curve by partitioning the region into shapes and calculate the sum of the area they cover_
    >* Estimate area under given curve with Riemann sums
    >* Left, Right and Middle Riemann Sums
* 4.3: [Definite Integral (pdf)](teacher_notes/4-3.pdf)
    >Integral is oposite of derivative
    >* Limits of Integration
    >* A functions average value
* 4.4: [__Fundamental theorem of calculus__](fundamental-theorem-of-calculus.md)
    >* [Fundamental theorem of calculus (pdf)](teacher_notes/4-4.pdf)
    >* Anti-derivative (inverse derivative, indefinite integral)

## Evaluating Integrals
* 5.1: [Constructing accurate graphs of anti-derivatives (pdf)](teacher_notes/5-1.pdf)
    >* Integral functions
* 5.2: [Second fundamental theorem of calculus (pdf)](teacher_notes/5-2.pdf)
    >If `f` is a continuous function on [a,b] and `F` is any anti-derivative of `f` then: __F'(x) = f(x)__
* [Combined chapters: 5.3, 5.4, 5.5 (pdf)](teacher_notes/5-3_4_5.pdf)
    >* 5.3: Integration by substitution
    >* 5.4: Integration by parts
    >* 5.5: Other options for finding algebraic antiderivatives
* 5.6: [Numerical integration (pdf)](teacher_notes/5-6.pdf)

### Using Definite Integrals
* 6.1: [Using definite integrals to find area and length (pdf)](teacher_notes/6-1.pdf)
    >* Finding the area bounded between graphs

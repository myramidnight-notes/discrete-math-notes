>* [U-substitution with NancyPi](https://www.youtube.com/watch?v=8B31SAk1nD8)
>* [Intro to u-substitution (Khan Academy)](https://www.khanacademy.org/math/ap-calculus-ab/ab-integration-new/ab-6-9/v/u-substitution)
# U-substitution
It helps you solve a integral equation by substituting part of it as `u`. And usually it is easy to spot which part of the equation should be substituted

>You need to be able to find both `u` and `u'` in the original equation. And usually the part you want to substitute is the part that is seemingly in the more complicated part of the equation. Such as being raised to a power, or denominator, or under root, or denominator under a root. Something you might get a headace trying to solve traditionally.
>
>Let's just swap that out for `u`, and see if the derivative `u'` (or `du`) can be spotted somewhere in the original equation, to confirm that you picked your `u` correctly. Yup, it's magic...  (no, just find something that explains it so your brain makes some sense of it). 

lets say that our integral would be __&int;3x<sup>2</sup>(x<sup>3</sup>+5)<sup>7</sup> dx__

1. find your `u` 
    >Let's say it was __(x<sup>3</sup>+5)__
1. find the derivative of `u`and add `dx` (or whatever was being used in the original integral). 
    >so the u' substitution would be __du = 3x<sup>2</sup> dx__
1. We then plug in our substitutes.
    > __&int;  3x<sup>2</sup>(u)<sup>7</sup> dx__ <br>=> __&int; (u)<sup>7 </sup>3x<sup>2</sup>  dx__ <br>=> __&int; (u)<sup>7</sup> du__
    >
    >Just re-arranged things so it was more obvious that we could replace __3x<sup>2</sup> dx__ with `du`
1. Then we solve the integral.
    >(u<sup>8</sup>)/8 + C
1. And finally we replace the `u` with what we were substituting it for
    >(x<sup>3</sup> + 5)<sup>8</sup>/8 + C
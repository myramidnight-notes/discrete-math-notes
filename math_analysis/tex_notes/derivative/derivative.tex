\section{Notation}
\begin{quote}
There are various ways to write derivative notation, and it's good to be aware of them because they can tell you things. As we have learned, $\vartriangle x$ means the \textit{change in $x$}, also denoted as $dx$. 
$$\dfrac{d}{dx} (equation)$$ 

This is the basic notation for \textbf{derivative}. $f'(x)$ is the derivative of function $f$, and simplified version of the above notation would be $(equation)'$, which only really works if all are derived by the same axis (all being either $dx$, $dy$ or even the substitute $du$).
\end{quote}

\begin{quote}
It is good to have existing rules, to make the derivative process quicker.

\begin{definition}[Derivative]
The derivative is the rate of change of a function with respect to a variable, in other words, the derivative tells us the slope of a function at any point.
\end{definition}

\begin{definition}[Differentiation]
Differentiation is the act of getting a derivative of a function
\end{definition}

\begin{myformula}{The Derivative of a function}{}
$$
f'(x) = \lim_{\vartriangle x \rightarrow 0} \dfrac{f(x + \vartriangle x) - f(x)}{\vartriangle x}
$$

You might have seen the formula written like this

$$
f'(x) = \lim_{h\rightarrow 0} \dfrac{f(x+h) - f(x)}{h}
$$


Or sometimes the derivative is written like this:
$$
\frac{dy}{dx} = \frac{f(x+dx) - f(x)}{dx}
$$
\end{myformula}

We simplify this until we can't, get rid of the denominator ($h$ below the line) and finally replace all remaining $h$ with zero, since that is the limit.
\end{quote}


\section{Standard Derivatives}
\begin{quote}
\begin{myformula}{Standard Derivatives}{}
$x$ are variables, $a$ are constants. Be careful if the variable is actually a function.
\begin{multicols}{2}
$$
\dfrac{d}{dx} \ (x^{n}) = nx^{n-1}
$$

$$
\dfrac{d}{dx} \ (sin \ x) = cos \ x
$$

$$
\dfrac{d}{dx} \ (cos \ x) = -sin \ x
$$

$$
\dfrac{d}{dx} \ (tan \ x) = \dfrac{1}{cos^{2} x}
$$

$$
\dfrac{d}{dx} \ (arcsin \ x) = \dfrac{1}{\sqrt{1-x^{2}}}
$$

$$
\dfrac{d}{dx} \ (arccos \ x) = -\dfrac{1}{\sqrt{1-x^{2}}}
$$

$$
\dfrac{d}{dx} \ (arctan \ x) = \dfrac{1}{1+x^{2}}
$$

$$
\dfrac{d}{dx} \ (ln \mid x \mid) = \dfrac{1}{x}
$$

$$
\dfrac{d}{dx} \ (log_{b}x) = \dfrac{1}{x \ ln \ b}
$$

$$
\dfrac{d}{dx} \ (e^{x}) = e^{x}
$$

$$ 
\dfrac{d}{dx} \ (e^{-x}) = -e^{-x}
$$

$$
\dfrac{d}{dx} \ (a^{x}) = a^{x} \ ln \ a
$$

$$
\dfrac{d}{dx} \ (a) = 0
$$

$$
\dfrac{d}{dx} \ (x) = 1
$$
\end{multicols}
\end{myformula}

\end{quote}

\section{Derivative Rules}
\begin{quote}
When armed with derivative rules and standard derivatives, then  calculating derivatives a lot simpler than having to go through the defining formula, very efficient shot-cuts.

\subsection*{Product Rule}
\begin{myrule}{Product rule}{}
When finding the derivative of multiplication

$$
(f(x)g(x))' = f'(x)g(x) + f(x)g'(x)
$$

A little shortcut when multiplying a constant ($a$) with a variable. 
It is like with the \textit{integrals}, taking the constant outside of the equation in a sense.
$$
(a * x)' = a * (x)'
$$
\end{myrule}

\subsection*{Quotent Rule}
\begin{myrule}{Quotent rule}{}
When finding the derivative of division

$$
\bigg( \dfrac{f(x)}{g(x)} \bigg) ' = \dfrac{f'(x)g(x) - f(x)g'(x) }{(g(x)) ^{2}}
$$
\end{myrule}
\subsection*{Chain Rule}
\begin{myrule}{Chain rule}{}
When finding the derivative of nested functions
$$
(f(g(x)))' = f'(g(x))g'(x)
$$
The chain rule can also be written as:
$$
 \frac{dy}{dx} = \frac{dy}{du}\frac{du}{dx}
$$
\end{myrule}

\begin{example}{former chain rule formula}{}
We want to find the derivative of $sin(x^{2})$.

\begin{enumerate}
\item First we find the inner and outer functions
\begin{itemize}
\item $f(g) = sin(g)$
\item $g(x) = x^{2}$
\end{itemize}

\item Then we differentiate them individually
\begin{itemize}
\item $f'(g) = cos(g)$
\item $g'(x) = 2x$
\end{itemize}

\item Finally we plug them into the chain rule

$$
(f(g(x)))' = f'(g(x))g'(x)
$$
$$
\frac{d}{dx} sin(x^{2}) = cos(g(x))(2x) = 2x cos(x^{2})
$$

\end{enumerate}

\end{example}

\begin{example}{later chain rule formula}{}
\begin{enumerate}
\item First we substitute $x^{2}$ with  $u$, so that $sin(x^{2})$ becomes $sin(u)$
\item Plug in $y = sin(u)$ and $u = x^{2}$ into the formula

$$
 \frac{dy}{dx} = \frac{dy}{du}\frac{du}{dx}
$$
$$
\frac{d}{dx}sin(x^{2}) = \frac{d}{du}sin(u)\frac{d}{dx}x^{2}
$$
\item Then we differentiate each:
\begin{itemize}
\item $\frac{d}{du} sin(u) = cos(u)$
\item $\frac{d}{dx} x^{2} = 2x$
\end{itemize}
$$
 \frac{d}{du}sin(u)\frac{d}{dx}x^{2} = cos(u) (2x)
$$
\item finally we substitute back the $u = x^{2}$ and simplify
$$
 cos(u)(2x)= 2x cos(x^{2})
$$
\end{enumerate}
\end{example}
Note: No wonder that u-substitution is called the reverse chain rule (in \textbf{integrals}), can see the similarities.

\subsection*{Reciprocal Rule}

\begin{myrule}{Reciprocal rule}{}
The reciprocal rule gives the derivative of the reciprocal of a function $f$ in terms of the derivative of $f$.
$$
g'(x) = \frac{d}{dx} = \bigg(\dfrac{1}{f(x)}\bigg) = \dfrac{-f'(x)}{f(x)^{2}}
$$
\end{myrule}
Note: Found this rule somewhere, I can't even write the word reciprocal without staring at reference. Don't think we used it anywhere in the current course.

\begin{myformula}{Derivative of inverse}{}
$$
\big( f^{-1} \big) '(x) = \dfrac{1}{f'(f^{-1}(x))}
$$
\end{myformula}

\begin{myformula}{Linear approximation}{}
$$
y = f(a) + f'(a) (x-a)
$$
\end{myformula}

\subsection*{Reference/Useful links}
\begin{itemize}
\item \href{https://www.mathsisfun.com/calculus/derivatives-rules.html}{Derivative Rules (math is fun)}
\end{itemize}
\end{quote}



\newpage

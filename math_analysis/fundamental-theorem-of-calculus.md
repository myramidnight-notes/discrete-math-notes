* [Introduction to Integration (math is fun)](https://www.mathsisfun.com/calculus/integration-introduction.html)
# Fundamental theorem of calculus
A theorem that links the concept of differentiating a function (calculating the gradient) with the concpet of intergrating a function (calculating the area under the curve).

![](images\Fundamental_theorem_of_calculus.gif)
> __<sub>a</sub><sup>b</sup>&int;f(x)dx = F(b) - F(a)__
>* `a` and `b` are the lower and upper limits (the slices through x axis at either endpoint of the area we want to calculate)
>* __<sub>a</sub><sup>b</sup>&int;f(x)dx__ is the area we want to calculate. Which is essentially the total area to the upper limit,  subtracting the total area up to the lower limit.
>* It can be rewriten as: __F(b) = F(a) + <sub>a</sub><sup>b</sup>&int; f(x)dx__

* ### Anti derivative (indefinite integral)
    >__Anti-derivative__, _inverse derivative_ or _indefinite integral_ of a function `f` is a differentiable function `F` __whose derivative is equal to the original function `f`__. 
    >
    >And since the derivative of a __constant__ is zero, there will be infinite number of antiderivatives.

* ### Finding the antiderivative
    >The process of solving for antiderivatives is called __antidifferentiation (or indefinite integration)__, which is the oposite of doing derivative (hence _anti_). 
    >
    >![](images/integral-vs-derivative.svg)
    
    >![](images/integral-tap-tank-constant.svg)

## First fundamental theorem of calculus
> It states that one of the __antiderivatives__ of some function `f` may be obtained as the integral of `f` with a variable bound of integration. This implies the existence of _antiderivatives_ for _continuous functions_.


## Second fundamental theorem of calculus
> It states that the integral of a function `f` over some interval can be computed by using any one of it's _infinitely_ many antiderivatives.
[..back](../README.md)

> Working through the teachers notes and adding my own understanding. KhanAcademy is very useful and the teaching videos there are interesting.

I mainly use the `pdf` notes, where I've personally scribbled extra notes onto it. Recreating that in Markdown seems tedious, but might use them to add some extra understanding.

* ### [Basics](Basics/README.md)
    * #### 0.1: Sets and lines
    * #### 0.2: Functions 
    * #### 0.3: Trigonometric functions
    * #### 0.4: Exponential and logarithm functions
* ### [Derivative](Derivative/README.md)
    * #### 1.2: The notion of limit
    * #### 1.3: The derivative of a function at a point
    * #### 1.4: The derivative function
    * #### 1.6: The second derivative
    * #### 1.7: Limits, continuiity and differentiability
    * #### 1.8: The tangent line approximation
* ### More derivatives and rules
    * #### 2.1: Elementary derivative rules
        * 2.1.2 Constant, power and exponential functions
    * #### 2.2: The sine and cosine functions
    * #### 2.3: The product and quotient rules
        * 2.3.2 The quotient rule
        * 2.3.3 Combining rules
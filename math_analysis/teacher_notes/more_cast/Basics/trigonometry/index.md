## Cos Sin Tan
 &theta; is __angle__, which can be in __radians__ or __degrees__.

### Cos Sin Triangle
>![CosSin triangle](images/cos_sin_triangle.png)
>* cos&theta; is the `x` axis
>* sin&theta; is the `y` axis


### Sohcahtoa
Quick way to remember how to calculate the _Cos_ _Sin_ and _Tan_
>![tangine](images/sine-cosine-tangent.png)
>* __Opposite__ = oposite to the &theta; angle
>* __Adjacent__ is next to the &theta; angle (shorter side)
>* __Hypotenuse__ is the long side (it essentially is the radius of the circle)

| | translation
| -- | --
| SOH.. | Sine = Opposite / Hypotenuse
| ..CAH.. | Cosine = Adjacent / Hypotenuse
| ..TOA | Tangent = Oposite / Adjacent

## The Unit Circle

* You can just translate the &pi; to `180` when calculating
>![unit_circle](images/trig_unit_circle.png)
>
>| deg | x (radians) | sin x | cos x 
>| -- | -- | -- | --
>| 0° | 0 | 0 | 1
>| 30° | &pi;/6 | &half; | &Sqrt;3 / 2
>| 45° | &pi;/4 | &Sqrt;2 /2 | &Sqrt;2 / 2
>| 60° | &pi; / 3 | &Sqrt;3 / 2 | &half;
>| 90° | &pi; / 2 | 1 | 0
>| 120° | 2&pi; / 3 | &Sqrt;3 / 2 | -&half;
>| 135° | 3&pi; / 4 | &Sqrt;2 / 2 | - &Sqrt;2 / 2
>| 150° | 5&pi; / 6 | &half; | - &Sqrt;3 / 2
>| 180° | &pi; | 0 | -1

### Radians to degrees
>(radians * 180) / &pi; = degrees
>* &pi; = 180°
>* 2 &pi; = 360°

## On Calculator
* There are `sin`, `cos` and `tan` buttons on scientific calculators, and 2ndary functionality is usually the inverse (denoted with a <sup>-1</sup>).
    > You just give them the angle and they spit out the answer. 
* You should pay attention to if the calculator is going to calculate in radians or degrees (usually has a indicator somewhere in what mode it's in)
    * On my calculator there is a button `DRG` that swaps between these modes. So on my screen it will say `RAD` or `DEG` depending on the mode.
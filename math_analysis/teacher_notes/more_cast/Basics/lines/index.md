# (0.1) Sets and lines
* ### Distance in the plane
    >![line distance](images/line_distance.png)
    >![distance formula](images/line_distance_formula.png)
    
* ### Midpoint Formula
    > ![midpoint](images/line_midpoint.png)
## Slope
![Slope](images/line_slope.png)
>* Vertical lines:
>    >  if &#8710;x = 0 (x<sub>2</sub>=x<sub>1</sub>) then the line is vertical, and we either say the __slope is undefined__ or +&infin; (or -&infin;).
>* Horizontal lines
>    >If &#8710;y=0 (y<sub>2</sub>=y<sub>1</sub>) then the line is horizontal and the slope is 0.

## Equation for a line
>![equation for line](images/line_equation.png)
>* `m` indicates the slope, and the slope is &#8710;y/&#8710;x =  (y-y<sub>0</sub>)/(x-x<sub>0</sub>)

another formula to get the line, it is called the __slope intercept__

>__y = mx + b__ 
> * `m` is the slope
> * `b` is where the line intercepts the `y` axis (at what height do we go through the `y` axis?)

>1. To start finding the equation of a line through points, we start with finding their slope
>1. Then select either point to be the fixed point on the line ()

#### How we get the slope intercept from the equation of the line
>* m = (y-y<sub>0</sub>)/(x-x<sub>0</sub>)
>* m(x-x<sub>0</sub>) = y - y<sub>0</sub>
>* mx - mx<sub>0</sub> + y<sub>0</sub> = y
>* y = mx + (y<sub>0</sub> - mx<sub>0</sub>)
>* __y = mx + b__ 


## Parallel lines
two lines that have the same slope are parallel to each other.

## Perpendicular lines 
>A perpendicular line is a reversed line, it lies at a 90° degree angle.
>
>![perpendicular line](images/line_perpendicular.png)


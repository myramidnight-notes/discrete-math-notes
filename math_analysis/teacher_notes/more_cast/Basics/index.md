
## [Basics](Basics/index.md)
* ### 0.1: [Sets and lines](lines/index.md)
    * Distance, Midpoint, Slope...
* ### 0.2: [Functions](functions/index.md)
    * understanding derivative
* ### 0.3: [Trigonometric functions](trigonometry/index.md)
    * Radians, degrees, unit circle, angles
* ### 0.4: [Exponential and logarithm functions](expo_log/index.md)
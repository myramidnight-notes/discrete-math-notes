# Exponential and Logarithm functions
Exponent = Veldisvísir

### Power rules
>1. b<sup>x</sup> * b<sup>y</sup> = b<sup>x+y</sup>
>1. (b<sup>x</sup>)<sup>y</sup> = b<sup>xy</sup>
> 
>In this context `b` is called the __base__

## Logarithm
### Rules for logarithms
>* b<sup>log<sub>b</sub>(x)<sup>
>* log<sub>b</sub>(b<sup>x</sup>)=x

1. log<sub>b</sub>(xy) = log<sub>b</sub>(x) + log<sub>b</sub>(y)
    > log<sub>b</sub>(x/y) = log<sub>b</sub>(x) - log<sub>b</sub>(y)
1. log<sub>b</sub>(x<sup>y</sup>) = y log<sub>b</sub>(x)
1. log<sub>a</sub>(x) = log<sub>b</sub>(x)/log<sub>b</sub>(a)
    > change of base
    >
    >![example](images/log_example.png)


### Finding exponent using log on calculator
>Example: log<sub>2</sub>512
>
>What is the exponent of 2 that gives you 512? 
>* 512log / 2log 
>    > The answer should be 9, so 2<sup>9</sup> = 512

### The natural logarithm
> There is a real number e&asymp; 2.71828....
>
> We let __ln(x) = log<sub>e</sub>(x)__ 


## Exponents
### Exponential function

The graph of an exponential function f(x)=b<sup>x</sup> will depend heavily on whether `b` is greater, euqal or less than 1

|x | 2<sup>x</sup> | 1<sup>x</sup> | (&half;)<sup>x</sup>
| -- | -- | -- | --
| -2 | 1/4 | 1 | 4
| -1 | 1/2 | 1 | 2
| 0 | 1 | 1 | 1
| 1 | 2 | 1 | &half;
|2 | 4 | 1 | 1/4

>![Expo function](images/expo_function.png)

## Handy rules of thumb:
![](images/rules-log-exp.png)
* __log<sub>b</sub>xy = log<sub>b</sub>x + log<sub>b</sub>y__
* __log<sub>b</sub>(x/y) = log<sub>b</sub>x - log<sub>b</sub>y__
* __log<sub>b</sub>x<sup>y</sup> = y log<sub>b</sub> x__
* __x<sup>p</sup>x<sup>q</sup> = x<sup>p+q</sup>__


Here are the exercises for week 2. A subset of these will appear on the quiz in week 2. All the problems are from Essential Precalculus (see General resources at the top of Modules)

* Exercises 1.2 p. 16: 3, 4, 8
* Exercises 3.1 p. 53: 1*, 2*, 11*, 12*, 27, 28
    *(Just find the slope-intercept form, i.e., y = mx+b)
* Exercises 4.1 p. 114: 33, 36, 37
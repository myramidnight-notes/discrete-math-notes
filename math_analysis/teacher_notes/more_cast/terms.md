## _e_ (mathmetical constant)
The number _e_, also known as __Euler's number__, is the mathmetical constant approximately equal to `2.71828`, and can be characterized in many ways. 

It is the base of the _natural logarithm_ (sometimes written as `ln`). It is the limit of __(1+1/_n_)<sup>_n_</sup>__ as _n_ approaches _infinity_.

The (natural) exponential function ___f_(x) = e<sup>x</sup>__ is the unique function _f_ that equals its own derivative and satisfies the equation ___f_(0)=0__; hence one can also define _e_ as ___f_(1)__.

>### Interesting notes about base _e_ and Natural Log
>* The functions __y = e<sup>x</sup>__ and __y=ln x__ are inverse functions
>* A couple of interesting properties
>   * e<sup>ln x</sup> = x
>   * ln e<sup>x</sup> = x 
>       > Essentially _ln_ and _e_ cancel each other out. leaving the `x`
>   * ln(e) = 1
>
>You can get the Euler's number on calculator with __e<sup>1</sup>__ = 2.7182182.... 
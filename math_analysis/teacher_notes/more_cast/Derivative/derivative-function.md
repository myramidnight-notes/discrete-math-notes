# The derivative function

## Definition 1.4.2
>Let `f` be a function and `x` a value in the functions domain. We define the __derivative of `f`__, a new function denoted `f'`, by 
>
>![](images/deriv-function.png)
>
>provided this limit exists.

Notice this is almost the same as the derivative of `f` at the point `a`.
>![](images/deriv-function-at-a.png)

we just want to let the point (`x` or `a`) vary.

>![example](images/deriv-function-example.png)
>* On the left is graph for __f(x)=4x-x<sup>2</sup>__
>* On the right is tangent line __f'(x)=4-2x__
>   * Going through the `y` axis at 4, with slope of negative 2
# 1.2 The notion of limit

### Graphical viewpoint
![limits](images/notion_of_limit.png)

* the `x` axis is the input, and the `y` axis would be the output. 
* __empty circle__ in the graph indicates a __undefined__ 
* A __filled circle__ would indicate where the function jumps to when the conditions change or a limit is hit.
    > If there are two points that have the same `x` axis, the filled one will be the actual outcome, regardless if the _undefined_ one happens to be on the expected path visually. That is what the limits are.
* Depending on the direction we are approaching a point from, the limit can tell us different things.
    > "as `x` approaches `1`", if we are coming from the right, we would be approaching 2 in height. Coming from the left we are actually approaching 3. Even though we would get 3 when we input `x` as 1, but we are just talking about what point we are approaching until then. 

## Limit notation
* We write the limit in a specific way, `lim` and the function, and below that we tell the details
    > ![notation](images/lim_notation.png)
    * x &rarr; 1<sup>-</sup> would indicate we're coming from the __left__
        >"As `x` approaches `1`", the results can be different based on from which direction we approach the point.
    * x &rarr; 1<sup>+</sup> would be from the __right__
    * x &rarr; 1 is on the spot
        > If the there is neither a `+` or `-` directional indicator, then it is a question of what the limit is at that precise point, and if there is a gap on the map (a unfilled circle), then it's __undefined__, because we're not reading where it goes if we input that number, but what the number is actually at that point.
    * The point on the graph would translate the input on the x axis and the outcome would be the height on the `y` axis.
        > At what __height__(`y`) will the point be on the graph if we input `x`?

## Devinition 1.2.2
> Given a function `f` and a fixed input `x=a` and a real number `L`, 
>* we say that __`f` has limit `L` as `x` approaches `a`__
>* write:
>   >lim f(x)=L <br> x &rarr; a
>
>If we can make `f(x)` arbitarily close to `L` by taking `x` sufficiently close to (but not equal to) `a`
>
>![](images/limit-as-approaches.png)
> No limit at `x=1` because no "_single value L_" we are getting close to.
>
>If we had the actual formula for `g(x)` we could try writing down a table of values to see the behaviour of `g(x)` close to these points. Even better (and this is how we want to answer these questions and exam problems) is to ask the formula to prove something about the limit.
>
>![](images/limit-example1.png)
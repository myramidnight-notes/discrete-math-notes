
## Sine and Cosine rule
* __sin(x)' = cos(x)__
* __cos(x)' = -sin(x)__

![](images/rule-sine-cosine.png)


## The product and quotient rules
### Product Rule
> product: the result of one or more multiplications.

![](images/rule-product.png)

### Quotient rule
>quotient: a result obtained by dividing one quantity by another

Because quotients and products are closely linked, we can use the product rule to understand how to take the derivative of a quotient.


![](images/rule-quotient.png)

## Combining rules
In order to apply the derivative shortcut rules correctly we must recognize the fundimental structure of a function.

* If a function is a sum, product or quotient of simpler functions, then we can use the sum, product or quotient rules to differentiate it in terms of simpler functions and their derivative.
* 
# The Second Derivative



## Increase and Decrease
>#### Devinition 1.6.4
>Given a function `f(x)` on the interval `(a,b)` we say that `f` is __increasing on `(a,b)`__ if for all points &in; `(a,b)` with `x<y` we have:
>* `f(x) < f(y)`
>
>Similarily we say that `f` is __decreasing on `(a,b)`__ if for all points &in; `(a,b)` with `x<y` we have:
>*  `f(x) > f(y)`

>![](images/concavity-example.png)


## Concavity
>#### Definition 1.6.10 
>Let `f` be a differentiable function on the interval `(a,b)`. Then `f` is __concave up__ on `(a,b)` if `f'` is increasing on `(a,b)`. 
>* slope of `f'(x)` is increasing => `f''(x) > 0`
>* where the curve is like a _smile_ 
>
>Similarily, `f` is __concave down__ on `(a,b)` if `f'` is decreasing on `(a,b)`
>* slope of `f'(x)` is decreasing => `f''(x) < 0`
>* Where the curve is like a _frown_
>

* If `f'(x) > 0`, then the slope of the _tangent line_ is positive
* If `f'(x) < 0`, then the slope of the _tangent line_ is negative
* If `f''(x) < 0`, then the slope itself is decreasing, concave downwards
* If `f''(x) > 0`, then the slope itself is increasing, concave upwards

>The position of `f` on the graph will indcate what the input and output was, so negative means output puts you under the `y` axis, and positive is above the `y` axis.
>
> In simple words, `f'` indcates if the curve is increasing or decreasing (going up or down), positive means we're curving up, negative going down, 0 is the peak of the curve.
>
>`f''` can indicate if we're on the concave down or concave up part of the curve. positive would be a convave up, negative a concave down. So even if the point is on the peak, we're still either inside a concave up or down (unless it's a straight line)

So if just looking at the graph, and want to find interval where `f'(x) > 0` and `f''(x) < 0`, then we want to find a interval where the curve is increasing, but concaving downards.

![](images/concavity.png)

* In this image we can see the function `f(x)`, which makes the local __max__ and __min__ of the graph quite obvious visually.
* The derivative of that function being `f'(x)` actually puts the _min_ and _max_ points on the `0`, and the positive `y` indicates a positive slope, and the negative indicates a negative slope. 
* The __second derivative__ here actually puts the __inflection point__ on the 0 `y` axis. So we know where the inflection point is on the first graph now.


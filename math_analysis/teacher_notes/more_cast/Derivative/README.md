
> [KhanAcedamy: Derivatives: definition and basic rules](https://www.khanacademy.org/math/differential-calculus/dc-diff-intro)

* ## Derivative
    * ### 1.2: [The notion of limit](limits.md)
    * ### 1.3: [The derivative of a function at a point](derive-at-point.md)
        *  Average rate of change
        * Tangent line
    * ### 1.4: [The derivative function](derivative-function.md)
    * ### 1.6: The second derivative
    * ### 1.7: Limits, continuiity and differentiability
    * ### 1.8: The tangent line approximation
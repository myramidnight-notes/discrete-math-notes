# 1.3 The derivative of a function at a point

## Derivative
![](images/derivative-basics.png)


## Average rate of change (Average Velocity)
The __average rate of change__ of a function `f` on the interval `[a, a+h]` is

![](images/deriv-at-point.png)

![](images/average-velocity.png)

>#### Different look at AV (using table)
>![](images/deriv-average-rate.png)
>* &Delta;x is the difference/change in `x`, and &Delta;y is the change in `y`.

## Tangent Line
> Let `f` be a function and `x=a` a value in the functions domain. The __derivative of `f`__ (__with respect to `x`__) __at `x=a`__, denoted `f'(a)`, is defined by 
>
>![](images/def-1-3-3.png)
>
>provided this limit exists
>
>We say that `f(x)` is differentiable at `x=a` if it has a derivative at `x=a`.
>
>If the derivative exists at `x=a` we can draw the __tangent line__ at this point: This is the line with slope `f'(a)` that goes through point `(a, f(a))`
>
>![](images/tangent-line.png)

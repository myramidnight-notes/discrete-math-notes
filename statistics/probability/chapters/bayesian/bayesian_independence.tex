
\section{Conditional Independence Assumptions}
\begin{itemize}
\item \href{https://www.youtube.com/watch?v=iaY3isLZUGs}{Bayes' Nets - Independence (video)}
\end{itemize}
\begin{quote}
\begin{definition}[Conditionally Independent]
Each variable is conditionally \\
independent of all its non-descendants in the graph given the value of all its parents\\

The statement "X and Y are conditionally independent given Z" can also be written as
$$
X \indep Y | Z
$$
\end{definition}


\begin{extra}{Assumptions about independence}{}
Assumptions we are required to make to define the Bayesian network when given the graph:
$$
P(x_i| x_1... x_{i-1}) = P(x_i|parents(X_i))
$$
So probability of $x_i$ given anything before it, is the same as saying the probability of $x_i$ given just it's parents.

\end{extra}
\begin{extra}{Assumptions about Independence in Bayesian networks}{}
$$
P(x_1, x_2, x_3)
$$$$
= P(x_1\wedge x_2\wedge x_3)
$$$$
= P(x_1 | Parents(X_1)) \cdot P(x_2|Parents(X_2)) \cdot P(x_3|Parents(X_3))
$$

So the probability of all those variables will depend on their individual parents as well.
\end{extra}

\subsection{Chain rule in Bayesian network}
\begin{example}{Chain Rule in Bayesian Network}{}
$$
P(x_1,x_2,x_3)  = P(x_1|P(x_2|x_1) \cdot P(x_3 | x_1, x_2))
$$
\end{example}



%\begin{example}{More independence}{}
%Lets say that we know the value of \textit{Y}
%\begin{center}
%\includegraphics[width=0.3\textwidth]{images/bayesian-dependency2.png}
%\end{center}
%\begin{itemize}
%\item \textit{X} and \textit{Z} are conditionally independent, given \textit{Y}. 
%\item Since there is no edge between \textit{X} and \textit{Z}, then \textit{X} and \textit{Z} become independent when we know \textit{Y}.
%\end{itemize}
%\end{example}
%
%\begin{example}{More independence}{}
%\begin{center}
%\includegraphics[width=0.28\textwidth]{images/bayesian-dependency3.png}
%\end{center}
%\begin{itemize}
%\item \textit{U} and \textit{X} are \textbf{not} conditionally independent given \textit{Z}. Because \textit{X} depends on \textit{U} (they have a edge between them).
%\item 
%\end{itemize}
%\end{example}
%
%\begin{example}{More independence}{}
%\begin{center}
%\includegraphics[width=0.3\textwidth]{images/bayesian-dependency4.png}
%\end{center}
%\end{example}

\end{quote}

\section{Bayesian Network Configurations}
\subsection{Causal Chains}
\begin{quote}
\begin{myformula}{"Causal Chain" configuration}{}

\begin{center}
\includegraphics[width=0.5\textwidth]{images/bayesian-causal-chain.png}
\end{center}
This configuration is called a "causal chain", because each variable causes something for the next.
$$
P(x,y,z) = P(x)P(y|x)P(z|y)
$$
\begin{itemize}
\item $P(x)$ because \textit{x} has no parents.
\item $P(y|x)$ because \textit{x} is parent to \textit{y}
\item $P(z|y)$ because \textit{y} is parent to \textit{z}
\end{itemize}
It is \textbf{not} guaranteed that \textit{X} is independent of \textit{Z}.
\begin{itemize}
\item But \textit{X} guaranteed to be independent of \textit{Z} if \textit{Y} has been observed.
$$
P(z|x,y) = \dfrac{P(x,y,z)}{P(x,y)} = \dfrac{\cancel{P(x)P(y|x)}P(z|y)}{\cancel{P(x)P(y|z)}} = P(z|y)
$$
So any \textit{evidence} along the chain \textit{blocks} the influence of the condition.
\end{itemize} 
\end{myformula}

\begin{example}{Causal Chain}{}
\begin{small}
\begin{center}
\includegraphics[width=0.3\textwidth]{images/bayesian-dependency.png}
\end{center}
\begin{itemize}
\item Given \textit{Y}, does learning the value of \textit{Z} tell us nothing new about \textit{X}?
$$
P(X|Y,Z) = P(X|Y)?
$$
The answer is yes. Since we know the value of all of \textit{X}'s parents (namely, \textit{Y}), and \textit{Z} is not a descendent of \textit{X}, then \textit{X} is conditionally independent of \textit{Z}.
\item Also, since independence is symmetric:
$$
P(Z|Y,X) = P(Z|Y)
$$
\item Are \textit{X} and \textit{Z} \textbf{necessarily independent}? No. If the value of \textit{Z} is known, it will effect the probability of \textit{Y}, which in turn would effect the probability of \textit{X}.
\end{itemize}

{\footnotesize \href{http://www.cs.cmu.edu/~awm/15781/slides/bayesinf05a.pdf}{Slides 2-5 of Bayesian nteworks}}
\end{small}
\end{example}
\end{quote}
\subsection{Common Cause}
\begin{quote}
\begin{myformula}{"Common Cause" configuration}{}

\begin{small}
\begin{center}
\includegraphics[width=0.65\textwidth]{images/bayesian-common-cause.png}
\end{center}
$$
P(x,y,z) = P(y)P(x|y)P(z|y)
$$
\begin{itemize}
\item X is \textbf{not} guaranteed to be independent of Z
\item But if we observe Y, then X and Z are conditionally independent
$$
P(z|x,y) = \dfrac{P(x,y,z)}{P(x,y)} = \dfrac{\cancel{P(y)P(x|y)}P(z|y)}{\cancel{P(y)P(x|y)}} = P(z|y)
$$
\end{itemize}
\end{small}
\end{myformula}
\end{quote}

\subsection{Common Effect}
\begin{quote}
Having two children is simple, its when you have two parents when things get complicated.
\begin{myformula}{"Common Effect" configuration}{}

\begin{small}
\begin{center}
\includegraphics[width=0.5\textwidth]{images/bayesian-common-effect.png}
\end{center}
\begin{itemize}
\item Rain causes traffic
\item Ballgame causes traffic
\item Both rain and Ballgame could cause even more traffic
\item But rain doesn't cause the ballgame
\end{itemize}
X and Y are independent, because they have no parents, and no interactions between them.\\

However, here is the tricky part 
\begin{itemize}
\item If we observe Z, then it activates influence between possible causes, so X and Y are \textbf{no longer independent}.
\item This is what makes this configuration a bit backwards compared to the Causal Chain and Common Cause. 
\item One way to read this would be, if we observe heavy traffic, then we would assume that either there is rain or there must be a ballgame, probably not both (but possible). So we will start looking for the cause to explain the traffic.
\end{itemize}

\end{small}
\end{myformula}
\end{quote}

\subsection{The General case}
\begin{quote}

\begin{extra}{Algorithm to figure out Conditional independence}{}
\begin{small}
This is a algorithm to answer the question "Are X and Y conditionally independent given a set of evidence variables \{ Z \}?" for larger Bayesian networks (non-triples)\\

The answer will be a search problem, where we see whether there are any paths that connect X and Y up. And if all the paths are blocked, then that means they are independent (and we then can say that they are \textit{d-separated} by Z)

\begin{enumerate}
\item Consider all \textbf{undirected} paths from  X to Z
\item We will fill in all observed evidence nodes
\item For each path, we check whether it is active or not
\begin{itemize}
\item A path is active if every triple segment in the path is active
\item A triple is either a \textbf{causal chain}, \textbf{common cause} or \textbf{common effect}.
\end{itemize}
\item If there are no active paths, then independence is guaranteed. 
\item If there is at least one active path, then independence is not guaranteed.
\end{enumerate}
\subsubsection*{What defines active paths?}
\begin{center}
\includegraphics[width=0.6\textwidth]{images/bayesian-active-path.png}
\end{center}
\begin{itemize}
\item \textbf{Causal chain} $A\rightarrow B \rightarrow C$ where B is unobserved (in either direction).
\item \textbf{Common cause} $A\leftarrow B \rightarrow C$ where B is unobserved
\item \textbf{Common effect} (aka v-structure) $A \rightarrow B \leftarrow C$ where B or one of its descendent is observed.
\end{itemize}

All it takes to block a path is a single inactive segment, but in order to break X and Y apart, then all the potential paths need to be blocked.
\begin{center}
\includegraphics[width=0.8\textwidth]{images/bayesian-inactive-path.png}
\end{center}
\end{small}
{\footnotesize \href{https://www.youtube.com/watch?v=iaY3isLZUGs}{Bayes' Nets: Independence, the general case (video, 59 minutes in)}.}
\end{extra}

\begin{example}{Reachability with the algorithm}{}
\begin{small}
\begin{center}
\includegraphics[width=0.3\textwidth]{images/bayesian-alg-example.png}
\end{center}
\textbf{Is \textit{L} conditionally independent from \textit{T'} given \textit{T}?}
\begin{itemize}
\item Checking the triples on path $L\rightarrow R \rightarrow T \rightarrow T'$:
\begin{itemize}
\item The causal chain: $L\rightarrow R\rightarrow T$ is active, because R is unobserved.
\item The causal chain: $R\rightarrow T \rightarrow T'$ is inactive, because T is observed
\item So the path as a whole is inactive. And since it is the only path, then \textit{all paths are inactive}.
\end{itemize}
\item Conclusion: Yes, $L\indep T'|T$ is true
\end{itemize}
\textbf{Is L conditionally independent from B (given no evidence)?}
\begin{itemize}
\item Checking the path $L\rightarrow R\rightarrow T \leftarrow B$:
\begin{itemize}
\item Causal chain: $L\rightarrow R\rightarrow T$ is active
\item Common effect: $R\rightarrow T \leftarrow B$ is inactive
\end{itemize}
\item Conclusion: Yes, $L\indep B$ is true
\end{itemize}
\textbf{Is L conditionally independent of B given T?}
\begin{itemize}
\item Checking the path $L\rightarrow R\rightarrow T \leftarrow B$:
\begin{itemize}
\item Causal chain: $L\rightarrow R\rightarrow T$ is active
\item Common effect: $R\rightarrow T \leftarrow B$ is active
\end{itemize}
\item Conclusion: No, $L\indep B | T$ is not true
\end{itemize}
\end{small}
\end{example}
\end{quote}
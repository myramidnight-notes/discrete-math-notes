\begin{extra}{Probabilistic Belief}{}
Consider a world where a dentist agent \textit{D} meets with a new patient \textit{P} \\

\textit{D} is interested in only whether \textit{P} has a cavity; so, a state is described with a single proposition: Cavity\\

Before observing \textit{P}, \textit{D} does not know if \textit{P} has a cavity, but from years of practice, he believes $Cavity$ with some probability \textit{p} and $\neg Cavity$ with probability $1-p$.\\

The proposition is now a boolean \textbf{random variable} and $(Cavity, p)$ is a \textbf{probabilistic belief}
\end{extra}

\section{Belief state}
\begin{quote}

\begin{extra}{Probabilistic Belief State}{}

The world has only two possible states, which are respectively described by $Cavity$ and $\neg Cavity$\\

\begin{itemize}
\item The \textbf{probabilistic belief state} of an agent is a probabilistic distribution over all the states that the agent \textit{thinks possible}
\item The belief state is defined by the \textbf{full joint probability of propositions}.
\item In the dentist example, \textit{D}'s belief state is:
\begin{center}
\begin{tabular}{|c | c|}
\hline
$Cavity$ & $\neg Cavity$\\
\hline
p & 1-p\\
\hline
\end{tabular}
\end{center}
\end{itemize}
\end{extra}

\begin{example}{Adding variables to the Dentist's belief state}{}
If we take the dentist example and add variables $Toothache$ and $PCatch$ (for catching the cavity):
\begin{itemize}
\item $Cavity \wedge Toothache \wedge PCatch$
\item $\neg Cavity \wedge Toothache \wedge PCatch$
\item $Cavity \wedge \neg Toothache \wedge PCatch$
\item ... and so on
\end{itemize}


\begin{center}
\includegraphics[width=0.6\textwidth]{images/belief-state.png}
\end{center}
You can probably can see how the complexity of such a table will increase with each added variable.
\end{example}
\end{quote}

\subsection{Probabilistic Inference from Belief state}
\begin{quote}
\begin{example}{Probabilistic Inference from Belief state}{}
\begin{itemize}
\item $P(Cavity)$ just sums together the cells associated with $Cavity$ (in this case, a single row).
\begin{center}
\includegraphics[width=0.6\textwidth]{images/belief-state-inference2.png}
\end{center}
$$
P(Cavity) = 0.108 + 0.012 + 0.072 + 0.008 = 0.2
$$
This is called \textbf{marginalization}

\item Probability of a cavity or toothache. Since the variables are dependent, we need to subtract the overlap (by simply not counting cells you've already counted)
\begin{center}
\includegraphics[width=0.6\textwidth]{images/belief-state-inference.png}
\end{center}
$$
P(Cavity \vee Toothache) = P(Cavity) + P(Toothache) - P(Cavity \wedge Tootache)
$$
\end{itemize}
\end{example}
\end{quote}

\subsection{Marginalization}
\begin{quote}

\begin{definition}[Marginalisation]
Marginalisation in probability refers to "summing out" the probability of a random variable given the joint probability distribution of the other variable(s). It is a direct application of the law of total probability.
\end{definition}

\begin{extra}{Marginalization }{}
\begin{small}
\begin{center}
\includegraphics[width=0.6\textwidth]{images/belief-state-inference.png}
\end{center}
\begin{itemize}
\item Because each cell in the table is a joint probability over all the shared properties (depending on each other). 
\item In order to find only the probability of a single property (such as $Cavity$), we would need to sum up all the possible values/cells of that property.
\item If we wanted the probability of $PCatch$ regardless of the other properties, then we would be summing up both the columns of $PCatch$ (one under $Toothache$ and another under $\neg Toothache$).
\end{itemize}

If the property we want to find is called $c$, then the formula for marginalization is:
$$
P(c) = \Sigma_\dagger \Sigma_{pc} P(c\wedge \dagger \wedge pc)
$$
\begin{itemize}
\item using the conventions that $c$ = one property (whether we care about $Cavity$ or $\neg Cavity$, the formula will look the same, so we use the lower case letters here to indicate both cases)
\begin{quote}
We simply put $Cavity$ or $\neg Cavity$ in place of $c$.
\end{quote}
\item $\Sigma_{\dagger}$ is the sum over $\dagger = \lbrace Toothache, \neg Toothache\rbrace$
\item It is essentially to sum up all the values of $c$ within the scope (full table, unless the scope was smaller).
\end{itemize}
\end{small}
\end{extra}

\begin{example}{Another example of marginalization}{}
\begin{small}
Let's say that you want to compute $P(X = x)$, but you are not given a direct probability distribution over \textit{X} from which you can read this value. \\

You are instead given a \textbf{joint probability distribution} over \textit{X} and some other random variable(s).\\

Well, in that case you can just do
$$
P(X=x) = \Sigma_{Y} P(X= x,Y)
$$
That is, to find $P(X = x)$, we sum up all the probability values where $X = x$ occurs with all possible values of $Y$.\\

This makes sense, intuitively. To see how, let's say \textit{Y} can take on \textit{n} values:
$$
y_1, y_2,...,y_n
$$

We can find how often $X=x$ occurs if we consider how often $X=x$ occurs with each \textit{individual} value of $Y$, and sum up all such values to get the total value of the "often-ness" of \textit{X}
\end{small}

\begin{footnotesize}
\href{https://www.quora.com/What-is-marginalization-in-probability}{What is marginalization in probability? (Quora)}
\end{footnotesize}
\end{example}
\end{quote}

\subsection{Conditional Probability with Belief state}
\begin{quote}

\begin{example}{Conditional Probability from Belief state}{}
\begin{small}
This is an example of how we extract conditional information from the joint probability table of the belief state.
\begin{center}
\includegraphics[width=0.6\textwidth]{images/belief-state-condition.png}
\end{center}
\begin{itemize}
\item Probability of Cavity, given that there is toothache. So \textit{Toothache} becomes the scope (the 100\%) that we need to divide with to get the correct percentage.

$$
P(Cavity|Toothache) =\dfrac{P(Cavity \wedge Toothache)}{P(Toothache)}
$$
$$
= \dfrac{0.108+0.012}{0.108 + 0.012 + 0.016 + 0.064} = \dfrac{0.12}{0.2} = 0.6
$$
We are only summing up the cases of $Cavity$ within the scope of $Toothace$, and dividing it with the scope ($Toothache$) to get the correct percentage.
\item It works the same way for $P(\neg Cavity| Toothache)$

$$
P(\neg Cavity|Toothache) =\dfrac{P(\neg Cavity \wedge Toothache)}{P(Toothache)}
$$
$$
= \dfrac{0.016 + 0.064}{0.108 + 0.012 + 0.016 + 0.064} = \dfrac{0.08}{0.2} = 0.4
$$
\end{itemize}
\end{small}
\end{example}


\begin{extra}{Conditional Probability}{}
\begin{small}
The give conditions (the right-hand side of the bar $|$) are observations that we use to effect the probability of the variable we are searching for.
\begin{itemize}
\item  Multiplication with Dependent variables
\begin{align*}
P(A\wedge B) \\
\Rightarrow P(A|B) \cdot P(B)\\
\Rightarrow P(B|A) \cdot P(A)
\end{align*}
\item Let's complicate things, more (dependent) variables!
\begin{align*}
P(A\wedge B \wedge C) \\
\Rightarrow P(A|B,C)\cdot P(B\wedge C)\\
\Rightarrow P(A|B,C)\cdot P(B|C) \cdot P(C)\\
\end{align*}
Could also have treated A and B as one variable, since the conjunction doesn't really care how we decide to pair things.
\begin{align*}
P(A\wedge B \wedge C)  \Rightarrow (A\wedge B | C) \cdot P(C)
\end{align*}

\end{itemize}
\end{small}
\end{extra}
\end{quote}

\subsection{Updating the Belief state (joint probability table)}
\begin{quote}

\begin{example}{Updating the Belief state}{}\begin{small}

If we get some new information that changes probability, we will need to normalize the belief state with the new probability.
\begin{center}
\includegraphics[width=0.6\textwidth]{images/belief-state-update.png}
\end{center}
Currently the probability of a $Toothache$ is $0.2$, and $\neg Toothache$ is $0.8$ (the full table should always sum up to $1$ after all).\\

Let's say that some evidence shows that probability of $Toothache$ is actually $0.8$. 
\begin{enumerate}
\item If the current probability is \textit{a} and the probability we want is $b$, then \textit{x} is the scalar we want to multiply the values with to change the values to reflect this new probability.
\begin{align*}
ax = b \\
\Rightarrow 0.2 x = 0.8 \\
\Rightarrow x = \frac{0.8}{0.2} = 4
\end{align*}

So all the values in the yellow frame would be multiplied by $4$ to reflect their new probability
\item For the $\neg Toothache$, we will want to change it from $0.8$ to $0.2$, since we can't just update half the table, then it would not sum up to be \textit{1}
\begin{align*}
ax = b \\
\Rightarrow 0.8 x = 0.2 \\
\Rightarrow x = \frac{0.2}{0.8} = 0.25
\end{align*}
 All the values in the blue frame would then be multiplied by $0.25$ 
\item Now all the values should be updated to reflect the change in probability
\begin{center}
\includegraphics[width=0.65\textwidth]{images/belief-state-update2.png}
\end{center}
\end{enumerate}
\end{small}
\end{example}

\end{quote}

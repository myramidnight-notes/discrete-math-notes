\section{Fundamentals of Probability}
\subsection{Random Sampling}
\begin{quote}
A \textbf{simple random sample} of \textit{n} items is a sample in which every member of the population has an equal chance of being selected and the members of the sample are chosen independently.

\begin{example}{}{}
Consider a class of students as the population under study. If we select a sample of size 5, each possible sample size of 5 must have the same chance of being selected.
\begin{itemize}
	\item How do we go about randomly selecting five students from a class of 100?
	\item How likely is the sample to represent the population?
\end{itemize}
\end{example}

\begin{definition}[Sampling bias]
 Sampling bias is \textbf{non-random} and refers to some members having a tendency to be selected more readily than others. When the sample is biased, the statistics turn out to be poor estimates.
\end{definition}

\textbf{Probability models} are the mathematical representation of a random phenomenon. They are defined by its \textbf{sample space}, \textbf{events} and \textbf{probability} associated with each event.

\begin{itemize}
\item \textbf{Sample Space \textit{(úrtaksmengi/útkomurúm})}
\begin{itemize}
	\item[\textit{en}] Sample space $(S)$ for a random experiment is a set of all possible outcomes of the experiment.
	\item[\textit{is}] safn allra mögulegar útkomna úr ákveðinni tilraun	
\end{itemize}

\item \textbf{Event (\textit{atburður})}
\begin{itemize}
	\item[\textit{en}] An event (\textit{atburður}) is a collection/subset of outcomes. 
	\item[\textit{is}] Atburður er tiltekin útkoma eða mengi tiltekinna útkomna úr úrtaksmengi.
\end{itemize}

\item \textbf{Probability (\textit{líkur})}
\begin{itemize}
	\item[\textit{en}]Probabilities for each event in the sample space
	\item[\textit{is}] Líkurnar ákveðinnar útkomu er hlutfall þeirra tilvika sem þarf að gerast til atburðurinn gerist. Ef það er 50/50 að maður fengi hvora hlið á peningi til að snúa upp, þá eru líkurnar á að maður fái haus tvisvar sinnum: fyrst þarf maður að fá haus, sem er 1/2, og þaðan þarf aftur að fá haus, sem er aftur helmingur (núna af 1/2), sem er þá 1/4. 
\end{itemize}

\end{itemize}

\centerline{\includegraphics[width=0.7\textwidth]{images/joint-probability.png}}
\end{quote}

\section{Sample spaces and Probability}

\subsection{Classical probability}
\begin{quote}
Classical probability assumes that all outcomes are equally likely
\begin{myformula}{Classical probability}{}
 The probability of any event \textit{E} is
$$
\frac{\text{the number of outcomes in \textit{E}}}{\text{total number of outcomes in the sample space}}
$$

This probability is denoted by

$$
P(E) = \dfrac{n(E)}{n(S)}
$$

This probability is called \textbf{classical probability}, and it uses the sample space \textit{S}
\end{myformula}



\begin{myrule}{Probability Rule 1 (on the scale of percentage)}{}
The probability of any event \textit{E} is a number (either a fraction or decimal) between and including 0 and 1. \\

This is denoted by $0 \leq P(E) \leq 1$\\

This rule states that probabilities cannot be negative or greater than 1
\end{myrule}


\begin{myrule}{Probability Rule 2 (no chance)}{}
If an event \textit{E} cannot occur (such as the event contains no members in the sample space), its probability is 0
\end{myrule}

\begin{myrule}{Probability Rule 3 (certainty)}{}
If an event \textit{E} is certain, then the probability of \textit{E} is 1
\end{myrule}

\begin{myrule}{Probability Rule 4 (total 100\%)}{}
The sum of the probabilities of all outcomes in the sample space is 1\\

\begin{tabular}{l c c c c c c c c c c c c}
\textbf{Outcome} & 1 && 2 && 3 && 4 && 5 && 6  \\
\hline\\
\textbf{Probability} & $\frac{1}{6}$ && $\frac{1}{6}$ && $\frac{1}{6}$ && $\frac{1}{6}$ && $\frac{1}{6}$ && $\frac{1}{6}$ \\\\
\textbf{Sum} & $\frac{1}{6}$ &+& $\frac{1}{6}$ &+& $\frac{1}{6}$ &+& $\frac{1}{6}$ &+& $\frac{1}{6}$ &+& $\frac{1}{6}$  & $ = \frac{6}{6} = 1$
\end{tabular}
\end{myrule}
\end{quote}
\subsection{Set notation and Venn diagrams}
\begin{quote}
\centerline{\includegraphics[width=0.45\textwidth]{images/sets.png} \includegraphics[width=0.45\textwidth]{images/sets2.png}}

It is good to refresh on set notation, because that is what probability is, a set of events. 

\begin{extra}{Event Manipulations}{}
\begin{itemize}
	\item \textbf{Complement}: The complement of an event $A$, denoted as $A^{c}$ or $\overline{A}$, occurs if and only if $A$ does not occur
	\item \textbf{Mutually exclusive} events cannot occur at the same time
\end{itemize}
\end{extra}
\end{quote}



\subsection{Complementary Events}
\begin{quote}
When we are trying to figure out the probability of something \textbf{not} occurring, then we're looking at \textit{complementary events}, anything other than the given event. This can be used to find out the probability of the given event as well.



\begin{myrule}{Rule for Complementary Events}{}
If the probability of an event or the probability of its complement is known, then the other can be found by subtracting the probability from 1\\

\begin{center}
\begin{tabular}{l c c c r}
$P(\overline{E}) = 1 - P(E)$  & or & $P(E) = 1 - P(\overline{E})$ & or & $P(E) + P(\overline{E}) = 1$
\end{tabular}
\end{center}
\end{myrule}

\centerline{\includegraphics[width=0.7\textwidth]{images/probability-complement.png}}
This Venn diagram represents the probability of the complement of an event happening.

\begin{itemize}
\item The area of the rectangle equals the sample space $P(S) = 1$, and the circle represents the event $P(E)$. So the sample space includes the event.

\item The probability of something other than event $E$ happening is $P(\overline{E})$ (notation means everything that is not $E$).

\item We can find out what the probability of $P(E)$ if we know the the probability of all other events, $P(E) = 1 - P(\overline{E})$. 

\item The opposite is also true, finding out the probability of events outside of the circle being $P(\overline{E}) = 1 - P(E)$
\end{itemize}


\begin{example}{}{}
A person selects 3 cards from a ordinary deck and replaces each card after it is drawn. Find the probability that the person gets at least one heart.

\begin{itemize}
	\item We know that the events are independent (cards get replaced)
	\item We want the probability of at least 1 heart in hand, $P(H)$
	\item We can calculate the probability of getting no hearts,  $P(\overline{H})$
\end{itemize}

$$
P(\overline{H}) = \frac{3}{4} \cdot \frac{3}{4} \cdot \frac{3}{4}  = \dfrac{27}{64}
$$

$$
P(H) = 1 - P(\overline{H}) = 1 - \frac{27}{64} = \frac{64-27}{64} = \frac{37}{64}
$$
\end{example}

\end{quote}


\subsection{Empirical Probability}
\begin{quote}
In empirical probablity the chances of each event are not equal, unlike \textit{classical} probability. The sum of the probabilities will still equal 1.

\begin{myformula}{Empirical Probability}{}
\begin{small}
Given a frequency distribution, the probability of an event being in a given class is

$$
P(E) = \frac{\text{frequency for the class}}{\text{total frequencies in the distribution}} = \dfrac{f}{n}
$$

This probability is called \textbf{empirical probability} and is based on observation
\end{small}
\end{myformula}

\end{quote}

\subsection{The Law of Large numbers}
\begin{quote}
If we're tossing a coin, we expect the chances of getting head to be 50\% (if the coin is fair).

We might be calculating the empirical probability based on the results of our coin tossing, and doing the experiment a few times might result in getting heads 90\% or 10\% or any percentage, because that's just the luck of randomness. 

But when the number of experiments increases, the empirical probability of getting a head will approach that theoretical probability (of 50/50 chance of getting head).

This phenomenon is an example of the \textbf{law of large numbers}
\end{quote}


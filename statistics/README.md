# Statistics
### Handy source material and books
* [Tölfræði frá grunni (pdf bók á Íslensku)](https://notendur.hi.is/ahj/main.pdf)
* [Probability and Statistics EBook (wiki)](http://wiki.stat.ucla.edu/socr/index.php/Probability_and_statistics_EBook)
* __Elementary Statistics__ (by Bluman)


## Index
I like using _markdown_ for general notes, and LaTeX for the technical part (fancy equations that is hard to type out elsewhere), but I might stick to the tex notes for this section. I will add everything to this index list

* ###  [Statistics Glósur](tex_notes/glosur_statistics.pdf) 
    >My notes on statistics in this course, includes alot of Icelandic even though the course is taught in English.
* ### Individual chapters from the statistical glosur
    * [Probability ](tex_notes/chapters/glosur_probability.pdf) (pdf)

* ### Teacher notes
    >Notes from the teacher, that I or the teacher might have scribbled all over. They are based on the __Elementary Statistics__ book.

    * __Introduction__
        * [Chapter 1.1](statistics/Statistics_Ch1-1.pdf) (pdf)
    * __Frequency Distributions and Graphs__
        * [Chapter 2](statistics/Statistics_Ch2.pdf)  (pdf)
    * __Data Description__
        * [Chapter 3.1](statistics/Statistics_Ch3-1.pdf)  (pdf)
        * [Chapter 3.2](statistics/Statistics_Ch3-2.pdf) (pdf)
        * [Chapter 3.3](statistics/Statistics_Ch3-3.pdf) (pdf)
    * __The Normal Distribution__
        * [Chapter 6.2](statistics/Statistics_Ch6-2.pdf) (pdf)
        * [Chapter 6.3](statistics/Statistics_Ch6-3.pdf) (pdf)
    * __Confidence Intervals__
        * [Chapter 7.1](statistics/Statistics_Ch7-1.pdf) (pdf)
        * [Chapter 7.2](statistics/Statistics_Ch7-2.pdf) (pdf)
        * [Chapter 7.4](statistics/Statistics_Ch7-4.pdf) (pdf)
    * __Hypothesis testing__
        * [Chapter 8.1](statistics/Statistics_Ch8-1.pdf) (pdf)
        * [Chapter 8.2](statistics/Statistics_Ch8-2.pdf) (pdf)
        * [Chapter 8.3](statistics/Statistics_Ch8-3.pdf) (pdf)
        * [Chapter 8.5](statistics/Statistics_Ch8-5.pdf) (pdf)
\section{Hypothesis Testing - Traditional method}
\begin{quote}
\begin{definition}[Statistical hypothesis (\textit{tölfræðileg tilgáta})]
A statistical hypothesis is a \textbf{conjecture} about a population parameter. This conjecture may or may not be true

\textbf{Tilgáta} um population parameter, sem gæti eða gæti ekki verið sönn.
\end{definition}

\begin{definition}[Null hypothesis (\textit{núlltilgáta})]
The null hypothesis, symbolized by $H_{0}$, is a statistical hypothesis that states that there is no difference between parameter and a specific value, or that \textbf{there is no difference between two parameters}.
\end{definition}

\begin{definition}[Alternative hypothesis (\textit{gagntilgáta})]
The alternative hypothesis, symbolized by $H_{1}$, is a statistical hypothesis that states that existence of a difference between a parameter and a specific value, or states that \textbf{there is a difference between two parameters}
\end{definition}

\textbf{The null and alternative hypotheses are stated together}, and the null hypothesis contains the equal sign, as shown (where \textit{k} represents a specified number).

A claim should be stated as the alternative hypothesis, using $<$, or $>$ or $\neq$. Because of this, the alternative hypothesis is sometimes called \textbf{research hypothesis}

\begin{center}
\begin{tabular}{c | c | c}
\textbf{Two-tailed test} & \textbf{Right-tailed test} & \textbf{Left-tailed test} \\
\hline
$H_{0}: \mu = k$ & $H_{0}: \mu = k$ & $H_{0}: \mu = k$\\
$H_{1}: \mu \neq k$ & $H_{1}: \mu > k$ & $H_{1}: \mu < k$
\end{tabular}
\end{center}

A claim, though, can be stated as either null hypothesis or the alternative hypothesis; however, the statistical evidence can \textbf{only support the claim if it is the alternative hypothesis}. Statistical evidence can be used to \textit{reject} the claim if the claim is the null hypothesis.

These facts are important when you are stating the conclusion of a statistical study.

\begin{extra}{Hypothesis testing: Common phrases}{}
\begin{itemize}
\item[$>$]  Is greater than\\
			Is above\\
			Is higher than\\
			Is longer than\\
			Is bigger than\\
			Is increased
\item[$<$]	Is less than\\
			Is below\\
			Is lower than\\
			Is shorter than\\
			Is smaller than\\
			Is decreased or reduced from
\item[$=$]	Is equal to\\
			Is the same as\\
			Has not changed from\\
			Is the same as
\item[$\neq$] Is not equal to\\
			Is different from\\
			Has changed from\\
			Is not the same as
\end{itemize}
\end{extra}
Þannig að við getum ekki sagt að við styðjum \textit{null }
\end{quote}


\section{Statistical tests}
\begin{quote}
\begin{definition}[Statistical test]
A statistical test uses the data obtained from a sample to make a decision about whether the null hypothesis should be rejected.

The numerical value obtained from a statistical test is called \textbf{test value}
\end{definition}

In the hypothesis-testing situation, there are \textbf{four possible outcomes}. In reality, the null hypothesis may or may not be true, and a decision is made to reject or not reject it.

\begin{center}
\includegraphics[width=0.4\textwidth]{images/hypothesis_map.png}
\end{center}

There are two types of errors, as seen in that hypothesis map. 
\begin{definition}[Type I error]
 It is the mistaken rejection of an actual true null hypothesis, also called a \textbf{False Positive}
\end{definition}

\begin{definition}[Type II error]
It is the mistaken acceptance of an actually \\false null hypothesis, also called a \textbf{False Negative}
\end{definition} 
 
A false positive is like a guilty person not being convicted, while a false negative is like a guilty person not being convicted.

I think the simplest way to remember this is would be as if positive and negative are true and false values, then they match:
\begin{itemize}
\item False \textbf{Positive}: the null hypothesis was true (while rejected)
\item False \textbf{Negative}: the null hypothesis was false (while not rejected)
\end{itemize}


\begin{definition}[Level of significance]
The level of significance is the maximum probability of committing a type I error. This probability is symbolized by $\alpha$ (Greek letter alpha). That is, \textit{P(type I error) = $\alpha$}
\end{definition}
\end{quote}

\subsection{Critical Values and Region}
\begin{quote}


\begin{definition}[Critical Value]
The critical value separates the critical region from the non-critical region. The symbol for critical value is \textbf{C.V}.
\end{definition}
\begin{definition}[Critical Region]
The \textbf{critical} or \textbf{rejection region} is the range of values of the test value that indicates that there is a significant difference and that the \textbf{null hypothesis should be rejected}
\end{definition}
\begin{definition}[Non-critical Region]
The \textbf{non-critical} or \textbf{non-rejection region} is the range of test values that indicates that the difference was probably due to chance and that the \textbf{null hypothesis should not be rejected}
\end{definition}

The critical regions are always at the ends, or \textit{tails} of the curve. So depending on the type of test we are going to do, it's either a \textbf{one-tailed test} or \textbf{two-tailed test}
\begin{center}
\includegraphics[width=0.6\textwidth]{images/critical-region.png}
\includegraphics[width=0.6\textwidth]{images/critical-region-2tail.png}
\end{center}
\end{quote}

\subsection{Tailed Tests}
\begin{quote} %BEGIN TAILED TESTS

\begin{definition}[One-tailed test]
A one-tailed test indicates that the null hypothesis should be rejected when the test value is in the critical region on one side of the mean.  A one-tailed test is either a \textbf{right-tailed test} or a \textbf{left-tailed test}, depending on the direction of the inequality of the alternative hypothesis
\end{definition}
\begin{definition}[Two-tailed test]
In a two-tailed test, the null hypothesis should be rejected when the test value is in either of the two critical regions.
\end{definition}
\begin{center}
\includegraphics[width=0.8\textwidth]{images/tailed-tests.png}
\end{center}


\begin{example}{Two tailed test}{}
A medical researcher is interested in finding out whether a new medication will have any undesirable side effects. The researcher is particularly concerned with the pulse rate of the patients who take the medication. Will the pulse rate \textbf{increase, decrease, or remain unchanged} after a patient takes the medication?

\begin{center}
\begin{tabular}{l c r}
$H_{0}: \mu = k$ & vs. & $H_{1}: \mu \neq k$\\
\end{tabular}
\end{center}
\end{example}
\begin{example}{Left tailed test}{}
A chemist invents an additive to \textbf{increase} the life of an auto-mobile battery. If the mean lifetime of the auto-mobile battery without the additive is 36 months
\begin{center}
\begin{tabular}{l c r}
$H_{0}: \mu = 36$ & vs. & $H_{1}: \mu > 36$\\
\end{tabular}
\end{center}

\end{example}
\begin{example}{Left tailed test}{}
A contractor wishes to lower heating bills by using special type of insulation in houses. If the average of the monthly heating bills is \$78
\begin{center}
\begin{tabular}{l c r}
$H_{0}: \mu = 78$ & vs. & $H_{1}: \mu < 78$\\
\end{tabular}
\end{center}
\end{example}
\end{quote}%BEGIN TAILED TESTS

\section{Z-tests and T-tests}
\begin{quote}
There are two types of statistical tests that we'll be covering, \textbf{z-test} and \textbf{t-test}. Z-tests are when the population is known, and t-tests are for when the population is unknown (or sample size is too small).

$$
\text{Test value} = \frac{\text{(observed value) - (expected value)}}{\text{standard error}}
$$

The \textit{observed value} is a statistic (such as the mean) that is computed from the sample data. The \textit{expected value} is the parameter (such as the mean) that you would expect to obtain if the null hypothesis were true, in other words, the hypothesized value. The \textit{denominator is the standard error} of the statistic being tested (in this case, the standard error of the mean). 

\begin{myformula}{Z-test}{}
The z-test is a statistical test for the mean of population. It can be used when the sample size is $\geq$ 30, or when the population is normally distributed and $\sigma$ is known.
$$
z = \frac{\overline{X} - \mu}{\sigma / \sqrt{n}}
$$
Where
\begin{itemize}
\item[$\overline{X}$] = sample mean
\item[$\mu$] = hypothesized population mean
\item[$\sigma$] = population standard deviation
\item[$n$] = sample size
\end{itemize}

If $n < 30$, population must be normally distributed.
\end{myformula}

\begin{myformula}{T-test}{}
It is essentially the same as the z-test, we just use the standard deviation of sample instead.
$$
z = \frac{\overline{X} - \mu}{s / \sqrt{n}}
$$
degrees of freedom = n - 1
\end{myformula}
\end{quote}

\section{Solving Hypothesis-Testing problems}
\begin{quote}

\subsection{The Traditional method} 
\begin{enumerate}
\item State the null and alternative hypothesis
\item Find the critical value(s) using tables or software\\
Depends on the $\alpha$ and the distribution
\item Compute the test value \\
Depends on the sample, $\overline{X}$
\item Make the decision to reject or not reject the null hypothesis
\item Summarize the results
\end{enumerate}


\subsection{The P-Value method (Probability)}
\begin{definition}[P-value (Probability value)]
The p-value is the probability of getting a sample statistic (such as the mean) or a more extreme sample statistic in the direction of the alternative hypothesis \textbf{when the null hypothesis is true}
\end{definition}

\begin{enumerate}
\item State the null and alternative hypothesis
\item compute the test value \\
Depends on the sample $\overline{X}$ (mean)
\item Find the P-Value 
\item Make the decision to reject or not reject the null hypothesis
\item summarize the results
\end{enumerate}

\begin{example}{P-value example}{}
\begin{itemize}
\item[] Suppose that an alternative hypothesis is $H_{1}: \mu > 50$ and the mean of sample is $\overline{X} = 52$. If the computer printed a \textit{P-value} of 0.0356 for a statistical test, then the probability of getting a sample mean of 52 or greater is 3.56\% if the true population mean is 50 (for the given sample size and standard deviation). The relationship between the \textit{P-value} and the $\alpha$ value can be explained in this manner. 
\item[] For $P=0.0356$, the null hypothesis wouold be rejected at $\alpha = 0.05$ but not at $\alpha = 0.01$
\item[] When the hypothesis test is two-tailed, the area in one tail must be doubled. For a two-tailed test, if $\alpha$ is 0.05 and the area in one tail is 0.0356, the \textit{P-value} will be $2(0.0356) = 0.0712$ (to take into account the area of the two tails).
\end{itemize}
\end{example}

\end{quote}
\subsection{Outcomes of Hypothesis Testing}
\begin{quote}
\begin{center}
\includegraphics[width=0.8\textwidth]{images/claim-not-enough-evidence.png}

When there is not enough evidence to prove a hypothesis 

\includegraphics[width=0.8\textwidth]{images/claim-evidence.png}

When there is enough evidence to prove a hypothesis

\end{center}
\end{quote}


\begin{quote}
There are three main features of all populations (or data samples) that are always critical in understanding and interpreting their distributions.

These characteristics are \textbf{Center}, \textbf{Spread} and \textbf{Shape}.
\end{quote}

\section{Measures of Central Tendency}
The main measures of centrality are \textbf{mean}, \textbf{median} and \textbf{mode}.

\subsection{The Mean (\textit{Meðaltal})}
\begin{quote} %BEGINNING OF MEAN
Meðaltalið er án vafa algengasta lýsingarstærðin á miðju mælinga, en þau eru viðkvæm fyrir frávikum (outliers) sem geta komið fyrir, sem eru óvenju há eða lá gildi fyrir mælingarnar.

The \textit{mean} is a very typical way to find the average number, but is \textit{very effected by extremely high and low values}.

\begin{myformula}{Calculating the Mean}{}
The \textit{mean} is the sum of all the values divided by the number of values. We use $\overline{X}$ to represent the \textbf{sample mean}

$$
\overline{X} = \dfrac{X_1 + X_2 + X_3 + ... + X_n}{n} = \dfrac{\sum X}{n}
$$

$n$ represents the total number of values in the sample

To represent the \textbf{population mean}, we use the greek letter $\mu$ (pronounced "mew") 

$$
\mu = \dfrac{X_1 + X_2 + X_3 + ... + X_N}{N} = \dfrac{\sum X}{N}
$$

$N$ represents the total number of values in the population
\end{myformula}

\begin{itemize}
\item \textbf{Population Mean $\mu$}
\begin{quote}
The \textit{population mean}, denoted by {$\mu$}, is calculated by using all the values from the population. 
\textbf{The population mean is a parameter}.
\end{quote}

\item \textbf{Sample mean $\overline{X}$}
\begin{quote}
The \textit{sample mean}, denoted by {$ \overline{X}$} (pronounced "X bar"), is calculated by using sample data. 
\textbf{The sample mean is a statistic}.
\end{quote}
\end{itemize}


\end{quote}%END OF MEAN

\subsection{The Median (\textit{Miðgildi})}
\begin{quote} % BEGINNING OF MEDIAN
The \textit{sample-median} can be thought of as the point that divides a distribution in half (50/50). 

\textit{Miðgildi} er sú mæling sem er í miðju mælisafnsins. Ef þú raðar öllum gildum í stærðarröð, þá tekur þú það sem er í miðjunni á röðinni. Ef það eru tvær tölur í miðjunni (sléttur fjöldi gilda) þá tökum við meðaltalið á þeim tveim. 

\begin{myformula}{Calculating the Median (for even)}{}
For even number of numbers, we will use the following formula to find the median
$$
\text{median(x)} = \dfrac{x_{n/2} + x_{(n/2) + 1}}{2}
$$

Simply finding the \textit{mean} of the two middle numbers
\end{myformula}

\begin{itemize}
	\item The median is used to find the center or middle value of a data set.
	\item The median is used when it is necessary to find out whether the data values fall into the upper or lower half of the distribution.
	\item The median is used for an open-ended distribution.
	\item The median \textbf{is less affected than the mean by extremely high or low values} (outliers).
\end{itemize}
\end{quote}% END OF MEDIAN

\subsection{The Mode (\textit{Tíðasta gildi})}
\begin{quote} %BEGINNING OF MODE
The \textit{mode} represents the most frequently occuring values (the numbers that appear most often). The term mode is applied both to probability distributions and to collections of experimental data.

\textit{Tíðasta gildið} er sú útkoma sem kemur fyrir í mælingunum okkar. Ef fleirri en ein tala koma jafn oft fyrir sem tíðasta gildi, þá eru þær allar tíðustu gildin.

\begin{definition}[Mode]
The value that occurs most often in a data set is called the \textbf{mode}. 
\end{definition}
\begin{itemize}[leftmargin=1in]

\item[\textbf{unimodal}] Data set has only a single value that occurs with the greatest frequency

\item[\textbf{bimodal}] Data set has two values that occur with the same greatest frequency

\item[\textbf{multimodal}] Data set has multiple values that occur with the same greatest frequency

\item[\textbf{no mode}] Data sat has no value that occurs more than once
\end{itemize}


\end{quote}%BEGINNING OF MODE

\subsection{The Midrange (\textit{Miðja spannar})}
\begin{quote} %BEGINNING OF MIDRANGE
\textit{Miðja spannar} er meðaltal hæsta og lægsta gildis.

\begin{myformula}{Calculating the midrange}{}
The \textbf{midrange} is defined as the sum of the lowest and higest values in the data set, divided by 2

$$
\text{midrange} = \frac{\text{lowest value} + \text{highest value}}{2}
$$
\end{myformula}

\end{quote}%END OF MIDRANGE

\subsection*{Summary of Central Tendencies}
\begin{quote}

\begin{extra}{Properties and Uses of Central Tendency}{}
\begin{small}
\subsubsection*{The Mean}
\begin{enumerate}
	\item The mean is found by \textbf{using all the values of the data}
	\item The mean varies less than the median or mode when samples are taken from the same population and all three measures are computed for these samples.
	\item The mean is used in computing other statistics, such as variance (\textit{frávik}).
	\item The mean for the data set is unique and not necessarily one of the data values.
	\item The mean cannot be computed for the data in a frequency distribution that has an open-ended class.
	\item The mean \textbf{is affected by extremely high or low values}, called outliers (\textit{undantekningar}), and may not be the appropriate average to use in these situations.
\end{enumerate}

\subsubsection*{The Median}
\begin{enumerate}
	\item The median is used to find the center or middle value of a data set
	\item The median is used when it is necessary to find out whether the data values fall into the upper half or the lower half of the distribution
	\item The median is used for an open-ended distribution
	\item The median is \textbf{less affected than the mean by extremely high or extremely low values}
\end{enumerate}

\subsubsection*{The Mode}
\begin{enumerate}
	\item The mode is the only measure of central tendency that can be used in finding the most typical case when the data are \textit{nominal} or \textit{categorical}. Such as religious preference, gender, or political affiliation.
	\item The mode is used when the most typical case is desired.
	\item The mode is the easiest average to compute. 
	\item The mode is not always unique. A data set can have more than one mode, or the mode may not exist for the data set.
\end{enumerate}

\subsubsection*{The Midrange}
\begin{enumerate}

	\item The midrange is easy to compute
	\item The midrange gives the midpoint
	\item The midrange \textbf{is effected by extremely high or low values} in a data set.
\end{enumerate}

\end{small}
\end{extra}

\subsection*{Central Tendency and distribution shape}
\centerline{\includegraphics[width=1\textwidth]{images/central-tendency-distribution.png}}
\end{quote}


\section{Shape of distributions}
\begin{quote}
The \textit{shape} of distributions appear when the data is put into graphs. 

\centerline{\includegraphics[width=0.7\textwidth]{images/distribution-shape.png}}
\end{quote}


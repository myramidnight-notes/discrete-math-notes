

\section{Measure of Variation and Dispersion}
\begin{quote}
There are many measures of (population or sample) variation, such as the range, the variance, the standard deviation, mean absolute deviation and so on. These are used to assess the dispersion or spread of the population.
\end{quote}

\subsection{Range}
\begin{quote}
\begin{myformula}{Range}{}
The \textbf{range} is the highest value minus the lowest value. The symbol $R$ is used for the range.
$$
R = \text{highest value }- \text{ lowest value}
$$
\end{myformula}
The range is always a non-negative number, and it can be zero (if there is only a single variable in the range).
\end{quote}

\subsection{Variance and Standard Deviation}
\begin{quote}
The \textbf{variance} is the average of the squares of the distance each value is from the mean. And the \textbf{standard deviation} is just the square root of the variance. So if you know one, you can easily find the other.

\begin{myformula}{Variance and Standard deviation of Population}{}
 The symbol for the population variance is $\sigma^{2}$ (it is the lower case of $\Sigma$ (\textit{sigma})).

$$
\sigma^{2} = \dfrac{\Sigma (X - \mu )^{2} }{N}
$$
\begin{itemize}
	\item[$X =$] individual value
	\item[$\mu =$] population mean
	\item[$N =$] population size
\end{itemize}

The symbol for the population standard deviation is $\sigma$.

The corresponding formula for the population standard deviation is

$$
\sigma = \sqrt{\sigma^{2}} = \sqrt{\dfrac{\Sigma (X - \mu)^{2}}{N}}
$$
\end{myformula}

\begin{myformula}{Standard deviation of Sample}{}

$$
s = \sqrt{\frac{\Sigma (X - \overline{X})^{2}}{n-1}}
$$
\begin{center}
or
\end{center}
$$
s = \sqrt{\frac{n(\Sigma X^{2}) - (\Sigma X)^{2}}{n(n-1)}}
$$
Where
\begin{itemize}
\item[$s$] = Standard deviation of Sample
\item[$\overline{X}$] = Sample mean
\item[$X$] = individual value in sample
\item[$n$] = number of observations/items in sample
\end{itemize} 
\end{myformula}

\end{quote}

\subsection{Coefficient of Variation}
\subsubsection{Quartiles and Interquartile Range (IQR)}
\begin{quote}


The \textbf{interquartile range} (IQR) describes the middle 50\% of values when ordered from lowest to highest. In other words, the IQR finds the range where most of the values are found. This makes it easier to identify outliers.

\centerline{\includegraphics[width=0.9\textwidth]{images/iqr_boxplot.png}}

The wider the IQR, the more variant the data.

To find this range, we first need to find the \textit{median} (middle value) of the lower and upper half of the data. 

\begin{enumerate}
	\item First we order the values by size, from min to max.
	\item Then we find the $Q_{2}$ with \textbf{median}.
	\item Then we can find the $Q_{1}$ by applying the \textit{median} on the remaining values on the left of $Q_{2}$
	\item Finally we can find $Q_{3}$ by applying the \textit{median} on the remaining values on the right of $Q_{2}$
	\item[IQR] We can then find the Interquartile Range: $Q_{3} - Q_{1}$
\end{enumerate}


\begin{example}{}{}
\begin{small}
\textbf{Find the IQR of the following values}
$$
1, \frac{5}{4}, \frac{1}{2}, \frac{1}{4}, 0, 2, 1, 1, 0, 2, \frac{1}{2}
$$
First we put them in order from smallest to largest
$$
0, 0,\frac{1}{4}, \frac{1}{2},\frac{1}{2},  1, 1, 1,\frac{5}{4},2,2
$$
\begin{enumerate}
	\item We find the \textbf{median}, it gives us the $Q_{2} = 1$
	\item Then we just find the median of either side of remaining values (we do not include the values that were used to find $Q_{2}$):
$$
0, 0,\frac{1}{4}, \frac{1}{2},\frac{1}{2} \mid 1, 1,\frac{5}{4},2,2
$$

	\item $Q_{1} = \frac{1}{4}$ and $Q_{3} = \frac{5}{4}$
	\item so the IQR = $\frac{5}{4}-\frac{1}{4} = 1$
\end{enumerate}
\end{small}
\end{example}



\end{quote}



\subsubsection{The Five-number summary}
\begin{quote}
It is simply a list of 5 numbers: 

\textit{Min value}, $Q_{1}$, $Q_{2}$, $Q_{3}$, \textit{Max value}

\end{quote}

\subsection{Definitions}
\begin{quote}
Variables can be summarized using statistics
\begin{itemize}
	\item A \textbf{statistic} is a numerical measure (or a function) that describes a characteristic of the \textit{sample}
	\item A \textbf{parameter} is a numerical measure that describes a characteristic of the \textit{population}. We use statistics to estimate parameters.
	\item We use \textbf{sample-statistics} to estimate/understand \textbf{population parameters} of characteristics! 
\end{itemize}

\subsubsection*{Notation}
\begin{itemize}
	\item Population parameters are typically denoted by lower-case Greek letters (such as $\mu$, $\sigma$, $\pi$...)
	\item Random variables are always denoted by capital letters (such as X, Y, U, W... ) and specific measurements (an instance of a data sample) are denoted by lower-case letters (such as x, y, u, w... ).
	\item Data (sample) driven estimates (for specific population parameters) are always denoted by a corresponding symbol with an overline or a hat (such as $\overline{X}$, $\hat{\mu}$...) 
\end{itemize}

\end{quote}

\begin{quote}%BEGINNING OF CONFIDENCE INTERVAL
\begin{definition}[Interval estimate]
An interval estimate of a parameter is an interval or a \textbf{range of values} used to estimate the parameter. This estimate may or may not contain the value of the parameter being estimated.
\end{definition}
In an interval estimate, the parameter is specified as being between two values. So we are estimating the probability of a value being within that interval.

\begin{definition}[Confidence level]
The confidence level of an interval estimate of a parameter is \textbf{the probability that the interval estimate will contain the parameter}, assuming that a large number of samples are selected and that the estimation process on the same parameter is repeated.
\end{definition}

How confident are we that our parameter will fall be in that estimated range?

\begin{definition}[Confidence interval]
A confidence interval is a \textbf{specific interval estimate} of a parameter determined by using data obtained from a sample and by using the specific confidence level of the estimate
\end{definition}
The range in which we are very confident that the value of the parameter will be found.

The Greek letter $\alpha$ (alpha) represents the \textbf{total area in both tails of the standard normal distribution curve}, and $\alpha /2$ represents the area in each of the tails, since one tail is just half of $\alpha$.
\end{quote}
\section{CI for the Mean when $\sigma$ and sample size is known}
\begin{quote} %BEGIN
You might wonder why other measures of central tendency, such as the median and mode, are not used to estimate the population mean. The reason is that the means of samples vary less than other statistics (such as medians and modes) when many samples are selected from the same population. 

\begin{definition}[Point estimate]
A point estimate is a \textbf{single numerical value} estimate of a parameter. The best point estimate of the population mean $\mu$ is the sample mean $\overline{X}$
\end{definition}

\begin{definition}[Interval estimate]
An interval estimate gives you a range of values where the parameter is expected to lie.
\end{definition} 
A confidence interval is the most common type of interval estimate
\end{quote} %END

\section{Critical Values}
\begin{quote}
\begin{definition}[Critical Value]
A critical value is the value of the test statistic which defines the upper and lower bounds of a confidence interval. It is denoted as: 
$$
z_{\alpha /2}
$$
\end{definition}
\end{quote}
\subsection{Z-distribution}
\begin{quote}

\begin{definition}[Z-distribution]
Z-distribution is a way of naming the \textbf{Standard} normal distribution. 
\end{definition}



\subsubsection{Finding Critical Value in Normal Distribution table}
\begin{example}{Finding the Critical Value in z-distribution}{}
\begin{small}
It is essentially \textbf{reverse engineering the z-value}, instead of using the table to find the percentage, we use the table to find the z.

\begin{enumerate}
\item Find the $\alpha$ level
\item Subtract $\frac{\alpha}{2}$ from 100\%
\item find that percentage in the z-table
\item the critical value is on the edges of the z-table, pinpointing the percentage.
\end{enumerate}

One example of this told me to subtract from 50\% to get the same results, so it might depend on the type of table you have.
\begin{enumerate}
\item $\alpha$ of a 95\% confidence interval will be 0.05. 
\item divide that by 2, getting 0.025
\item 1-0.025 = 0.975
\item locating that in the z-table gives me critical value of 1.96
\end{enumerate}
\end{small}
\end{example} 
\end{quote}


\subsection{Maximum error of estimate}
\begin{quote}
\begin{myformula}{Standard Error}{}
$$
\text{Error (known $\sigma$)} =  \dfrac{\sigma}{\sqrt{n}} 
$$

When $\sigma$ is unknown, then find the standard error with the sample standard deviation.
$$
\text{Error (unknown $\sigma$)} = \dfrac{s}{\sqrt{n}}  = \frac{1}{\sqrt{n}} \cdot \sqrt{\Sigma \frac{(X_{i} - \overline{X})^{2}}{(n-1)}}
$$
\end{myformula}
\begin{myformula}{Maximum error of estimate (or margin of error)}{}
When $\sigma$ (standard deviation of \textbf{population}) is known
$$
z_{\alpha /2} \cdot \bigg(\frac{\sigma}{\sqrt{n}} \bigg) = \text{margin of error}
$$

When $\sigma$ is unknown, then we use $s$ instead (the standard deviation of sample)
$$
z_{\alpha /2} \cdot \bigg(\frac{s}{\sqrt{n}} \bigg) = \text{margin of error}
$$

where $\alpha$ is the size of the critical region
\end{myformula}


\begin{myformula}{Confidence interval of the mean for a specific $\alpha$}{}

$$
\overline{X} - z_{\alpha / 2} \cdot E < \mu < \overline{X} + z_{\alpha /2} \cdot E
$$

where $E$ is the \textbf{margin of error}

\begin{itemize}
\item a 90\% confidence interval, $z_{\alpha /2}$ = 1.65 
\item a 95\% confidence interval, $z_{\alpha /2}$ = 1.96
\item a 99\% confidence interval, $z_{\alpha /2}$ = 2.58.
\end{itemize}
\end{myformula}

\begin{center}
\includegraphics[width=0.6\textwidth]{images/confidence_interval.png}

The confidence interval of 95\% will be 95\% of the sample

\includegraphics[width=0.6\textwidth]{images/confidence_interval_ends.png}

The remaining percentage will naturally then be divided between the two ends of the distribution (in this example being confidence interval of 98\% )
\end{center}

\begin{example}{Find point estimate}{}
Market researchers use the \textit{number of sequences per advertisement} as a measure of readability for magazine advertisements. A random sample of number of sequences found in 30 magazine advertisements are listed. Use this sample to find the \textbf{point estimate} of the population mean $\mu$
\begin{center}
16,9,14,11,17,12,99,18,13,12,5,9,17,6,11,17,18,20,6,14,7,11,12,5,18,6,4,19,11,12
\end{center}

\begin{enumerate}
\item point estimate = sample mean = $\frac{443}{30} = 14.77$
\item interval estimate for $\mu$ 
\item first we need to decide \textbf{how confident} we want to be (deciding a confidence level) that the interval estimate contains the population mean.
\item reacall critical values for standard normal distribution:
\begin{itemize}
\item[80\%] confidence (0.80), $\frac{\alpha}{2}$ = 0.1, z = 1.28
\item[90\%] confidence (0.90), $\frac{\alpha}{2}$ = 0.05, z = 1.645
\item[95\%] confidence (0.95), $\frac{\alpha}{2}$ = 0.025, z = 1.96
\item[99\%] confidence (0.99), $\frac{\alpha}{2}$ = 0.005, z = 2.575
\end{itemize}

\item Is the population variance known? It decides if we use the standard error or not. We have only have samples, so we go with SE (Standard Error).

\item Let's calculate for a known population by assuming that $\mu =  16$

\begin{itemize}
\item for 80\% confidence level:
$$
\overline{X} \pm 1.28 \cdot SE(\overline{X}) = 14.77 \pm 1.28 \frac{16}{\sqrt{30}} = [11.03; 18.51]
$$
\item for 90\% confidence level:
$$
\overline{X} \pm 1.645 \cdot SE(\overline{X}) = 14.77 \pm 1.645 \frac{16}{\sqrt{30}} = [9.96;19.57]
$$
\item for 99\% confidence level:
$$
\overline{X} \pm 2.575 \cdot SE(\overline{X}) = 14.77 \pm 2.575 \frac{16}{\sqrt{30}} = [7.24;22.29]
$$
\end{itemize}

\item[] Our confidence decreases, the higher the confidence level is, because we are narrowing our range, there for reducing the probability of $\mu$ falling into the range.
\end{enumerate}

\end{example}

\end{quote} %END OF CONFIDENCE INTERVAL

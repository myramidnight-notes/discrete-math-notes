
\section{Sample Size (Small samples)}
\begin{quote}

A small sample contains typically \textbf{less than 30} observations. Naturally the point estimates are less precise and the interval estimates produce wider intervals, compared to the case of large samples.
\end{quote}
\subsection{Point Estimation of Population mean}

\begin{quote}
The population mean may also be estimated by the sample mean using a small sample. 
$$
\overline{X} = \frac{\sum_{i=1}^{n} X_{i} }{n}
$$
But since it's a small sample, keep in mind that it may be susceptible to outliers.
\end{quote}
\subsection{Interval Estimation of Population mean}
\begin{quote}
\begin{example}{Interval estimation of population mean (small sample)}{}
\begin{small}
For small samples, interval estimations of the population means (or Confidence Intervals) are constructed as follows.

\begin{enumerate}
\item Choose a confidence level $(1-\alpha )100\%$, where $\alpha$ is small (0.1, 0.05, 0.025... ).
\item Then a $(1-\alpha )100\%$ confidence interval for $\mu$ is defined in terms of the t-distribution
$$
\text{CI}(\alpha ): \overline{X}) \pm t_{\alpha / 2} \cdot E
$$
Where E is the \textbf{standard error}
$$
E = \frac{\sigma}{\sqrt{n}} \text{ if }\sigma \text{ is known}
$$ 
$$
E = \frac{s}{\sqrt{n}} \text{ if }\sigma \text{ is unknown}
$$

Standard error of the estimate is obtained by replacing the unknown population standard deviation by the sample standard deviation
$$
\frac{s}{\sqrt{n}} = SE = \frac{1}{\sqrt{n}}\cdot \sqrt{\Sigma \frac{(X_{i} - \overline{X})^{2}}{n-1}}
$$
\item $t_{\alpha /2}$ is the Critical Value for the T(df=sample size - 1) distribution at $\alpha /2$
\end{enumerate}
\end{small}
\end{example}
\end{quote}


\subsection{Student's T-distribution}
\begin{quote}
The \textit{Student's t-distribution} arises in the problem of estimating the mean of a normally distributed population when the sample size is small and the population variance is \textbf{unknown}. 

It is the basis of the popular Student's t-tests for statistical significance of the difference between two sample means, and for confidence intervals for the difference between the two population means.

\begin{definition}[T-distribution]
T-distribution is defined by the degrees of freedom, which are related to the sample size. The t-distribution is most useful for small sample sizes, when the population standard deviation is not known, or both.
\end{definition}
The t-distribution has been described as being similar to normal distributions, just with fatter tails.  The \textit{probability of getting values very far from the mean is larger} with a t-distribution than a normal distribution.


\begin{definition}[Degrees of Freedom]
Degrees of freedom refers to the maximum number of logically independent values, which are values that have freedom to vary, in the data sample.
\end{definition}

It is not quite the same as the number of items in the sample. We use it when consulting the \textbf{t-distribution table}.

\begin{myformula}{Degrees of Freedom (df)}{}
$$
df = n - 1
$$
where $n$ is the number of observations/items in the sample
\end{myformula}

\subsubsection{Consulting the T-distribution table}
\begin{example}{Finding Critical Value in T-distribution table}{}
\begin{enumerate}
\item Locate column of the confidence interval you are interested in
\item locate the correct row in the df (degrees of freedom) column
\item The value in the intersecting cell is the Critical Value 
\end{enumerate}
\end{example}

\subsubsection{T-score vs. Z-score}
\begin{myrule}{When to use the z-score}{}

There is a requirement for using the z-score:
\begin{itemize}
\item You have to know the \textbf{$\sigma$ (standard deviation of population)}.
\item You also \textbf{should} have a sample size larger than 30. 
\end{itemize} 

If you do not know the $\sigma$, then you are forced to use the \textit{t-score} instead.
\end{myrule}
\begin{myrule}{When to use the T-score}{}
You should use the t-score when you have a \textbf{sample size smaller than 30}, or when you don't know the $\sigma$ (standard deviation of population).
\begin{itemize}
\item If you know the $\sigma$, but have a small sample size, then you can use the $\sigma$ instead of $s$ in the formula. But you still use the \textit{t-score}
\end{itemize}
\end{myrule}

\includegraphics[width=0.8\textwidth]{images/t-score-vs-z-score.png}

\end{quote}


\subsection{Minimum Sample size}
\begin{quote}
\begin{myformula}{Minimum sample size for mean}{}
$$
n = \bigg( \frac{z_{\alpha /2} \cdot \sigma}{E}\bigg)^{2}
$$
Where $E$ is the maximum error of estimate. If necessary, round the answer up to obtain a whole number. That is, if there is \textbf{any} fraction or decimal portion in the answer, use the next whole number for sample size $n$
\end{myformula}
\end{quote} %END SAMPLE SIZE


\section{Applications of the Normal Distribution}
\begin{quote}
\textbf{What is considered normal?} It is the most likely outcomes, and if our case happens to be very likely, then we could say it's a very normal outcome. 

In mathematics, curves can be represented by equations. 

The normal distribution will tell us what is considered normal, based on the data, allowing us to put a value on a scale from 0 to 100\% 
\begin{myformula}{Mathematical equation for normal distribution}{}
$$
y = \dfrac{e^{-(X-\mu )^{2} / (2\sigma^{2})}}{\sigma \sqrt{2\pi}}
$$
\begin{itemize}
\item[$e\approx$] 2.718 ($\approx$ means "is approximately equal to"), Euler's number.
\item[$\pi \approx$] 3.14 
\item[$\mu =$] population mean
\item[$\sigma = $] population standard deviation
\end{itemize}
\end{myformula}

The formula can look formidable, but in applied statistics, tables or technology is used for specific problems instead of an equation.

\begin{definition}[Normal distribution]
A normal distribution is a continuous, symmetric, bell-shaped distribution of a variable.
\end{definition}
\centerline{\includegraphics[width=0.7\textwidth]{images/area-under-normal-curve.png}}

\begin{extra}{Summary of properties of Theoretical Normal Distribution}{}
\begin{enumerate}
\item A normal distribution curve is bell-shaped
\item The mean, median and mode are equal and are local at the center of the distribution. 
\item A normal distribution curve is unimodal (only has one mode)
\item The curve is symmetric about the mean, which is equivalent to saying its shape is the same on both sides of a vertical line passing through the center.
\item The curve is continuous; that is, there are no gaps or holes. For each value of X, there is a corresponding value of Y.
\item the curve never touches the X axis. Theoretically, no matter how far in either direction the curve extends, it never meets the x axis, but it gets increasingly closer.
\item The total area under a normal distribution curve is equal to 1 or 100\%. This fact may seem unusual, since the curve never touches the x axis, but one can prove it mathematically by using calculus.
\item The area under the part of a normal curve that lies within 1 standard deviation of the mean is approximately 0.68, within 2 standard deviations, about 0.95, and within 3 standard deviations, about 0.997 

\end{enumerate}
\end{extra}
\end{quote}

\section{The standard normal distribution}
\begin{quote}%BEGIN STANDARD NORMAL DISTRIBUTIOn

\begin{definition}[Standard normal distribution] 
The standard normal distribution is a \\normal distribution with a mean of 0 and a standard deviation of 1.
\end{definition}

\begin{myformula}{Standard normal distribution equation}{}
$$
y = \dfrac{e^{-z^{2}/2}}{\sqrt{2\pi}}
$$
\textit{z} would be the standard score.
\end{myformula}

All normally distributed variables can be transformed into the standard normally distributed variable by using the formula for standard score

\begin{myformula}{Standard score (z-score/value)}{}
$$
z-score = \frac{\text{value - mean}}{\text{standard deviation}} = \dfrac{X - \mu}{\sigma}
$$
\end{myformula}


Once the X values are transformed into z-scores, they are called \textit{z} values. The \textbf{\textit{z} value} is actually the number of standard deviations that a particular X value is away from the mean.

\end{quote} %END STANDARD NORMAL DISTRIBUTION

\section{Finding the area under the Standard Normal Distribution curve}
\begin{quote} %BEGIN AREA UNDER STANDARD CURVE

\begin{enumerate}
\item First we draw the normal distribution curve and shade the desired area
\item Convert the values of X to \textit{z} values, using the formula for standard score, $z=\dfrac{X-\mu}{\sigma}$
\item Find the corresponding area, using a table, calculator or software
\end{enumerate}
\begin{center}
\includegraphics[width=1\textwidth]{images/finding-area-under-curve.png}
\end{center}
\begin{enumerate}
\item To find the probability of value landing on the \textbf{left side} of z: then z is the probability (it goes from zero to z after all).
\item To find the probability of a value landing on the \textbf{right side} of z: then we're subtracting z from 1.
\item To find the probability of a value landing within a given range, then we find the z values of both sides.
\end{enumerate}

\subsubsection*{How to use the table}
\begin{quote}

\centerline{\includegraphics[width=0.5\textwidth]{images/z-table-lookup.png}}
What we're doing is to look up what z =1.39 would be on the scale of 0 to 1, horizontally we have the extra decimals.

Simply put, we're changing the z into a percentage, telling us the how \textit{normal} that outcome is. The higher the percentage, the more normal it is (being close to the top of the curve).

\end{quote}

\begin{example}{Liters in blood}{}
An adult has an average of 5.2 liters of blood. Assume the variable is normally distributed and has a standard deviation of 0.3. Find the percentage of people who have less than 5.4 liters of blood in their system.

\includegraphics[width=0.5\textwidth]{images/example-curve-1.png}

\begin{itemize}
\item[$\mu =$] 5.2 liters of blood
\item[$\sigma = $]0.3
\end{itemize}
 We want to find $P(X < 5.4)$

$$
P(X < 5.4) = P(z< \frac{5.4-5.2}{0.3})
$$
$$
=P(z<\dfrac{0.2}{0.3}) = P(z<0.67) = 0.7486 = 74.86 \text{\%}
$$
\end{example}

\begin{example}{}{}
Find $P(0 < z < 2.32)$

\begin{itemize}
\item It means find the area under the standard normal distribution curve between 0 and 2.32. 
\item First we look up the area corresponding to 2.32 and that is 0.9898. 
\item Then we look up the area corresponding to z = 0. It is 0.500. 
\item Subtract the two areas: $0.9898 - 0,5 = 0.4898$. 
\item Hence the probability is 48.98\%
\end{itemize}

\begin{center}
\includegraphics[width=0.5\textwidth]{images/example-curve-2.png}
\end{center}

\end{example}

\begin{myformula}{Finding X}{}
When you must find the value of X, you can use the following formula:
$$
X = z \cdot \sigma + \mu
$$
\end{myformula}
It is obtained from the formula for \textit{standard score}, isolating the X.
$$
z = \frac{X - \mu}{\sigma}
$$

\end{quote}%BEGIN AREA UNDER STANDARD CURVE



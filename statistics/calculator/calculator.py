import tkinter as tk
from tkinter.constants import BOTH

#from PIL import ImageTk, Image
#image = ImageTk.PhotoImage('filepath') #and then pack it like any other widget

from assets import CriticalValueT, Zdistribution, ZtableLookup, CriticalValueZlookup


class StatisticsCalculator:
    def __init__(self):
        # Define the window
        self.window = tk.Tk()
        self.window.title("My Statistics Calculator")
        #self.window.geometry('400x200')

        self.criticalFrame = tk.LabelFrame(
            text="Critical Values",
            background="orange"
        )

        self.normalFrame = tk.LabelFrame(
            text="Normal Distribution",
            background="cornflowerblue"
        )

        # Add the assets
        self.tcritical = CriticalValueT(self.criticalFrame)
        self.zdistribution = Zdistribution(self.normalFrame)
        self.ztable = ZtableLookup(self.normalFrame)
        self.p2zScore = CriticalValueZlookup(self.criticalFrame)

        
    def run(self):
        """Open the GUI window"""
        self.criticalFrame.pack(fill=BOTH,padx=10, pady=10)
        self.normalFrame.pack(fill=BOTH,padx=10, pady=10)
        self.tcritical.pack(padx=5, pady=5)
        self.p2zScore.pack(padx=5, pady=5)
        self.zdistribution.pack(padx=5, pady=5)
        self.ztable.pack(padx=5, pady=5)
        self.window.mainloop()

if __name__ == "__main__":
    gui = StatisticsCalculator()
    gui.run()
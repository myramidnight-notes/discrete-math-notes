import time

from sys import stdin, stdout
def restart_line():
    stdout.write('\r')
    stdout.flush()



# sys.stdout.write('some data')
# sys.stdout.flush()
# time.sleep(2) # wait 2 seconds...
# restart_line()
# sys.stdout.write('other different data')
# sys.stdout.flush()

# Import sys module

# '\r' seems to move the cursor back to the start of line, overwriting things as it goes
# could probably use something that reads the width of the window and prints whitespace to clear the line
# stdout.flush() flushes things from the buffer to the terminal
# Using this to reuse a terminal line has the issue of "if the new line is shorter than previous line", then we can still see the previous output
while True:
    restart_line()
    line = stdin.readline()    
    restart_line()
    if not line:
        break
    stdout.write("%d" % int(line)**2)

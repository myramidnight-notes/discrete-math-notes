import tkinter as tk
from tkinter.constants import BOTH
from scipy.stats import norm
import numpy as np
from scipy.stats.stats import zscore

class CriticalValueZlookup:
    """This is a version of the Critical Value lookup that would have radio buttons for one or two tailed"""
    def __init__(self,window): 
        # When I only want to get a value from Z-score
        self.frame = tk.LabelFrame(
            window,
            text="Scipy z-score (critical value)"
        )

        self.zScoreOutput = tk.StringVar()
        self.zScoreOutputPrint = tk.Label(
            self.frame,
            textvariable=self.zScoreOutput
        )

        self.radioValue = 1

        self.tailedOptions = [
            ("One tailed",1),
            ("Two tailed", 2)
        ]

        self.pValueVar = tk.StringVar()
        self.pValueLabel = tk.Label(
            self.frame,
            textvariable=self.pValueVar
        )

        self.probabilityLabel = tk.Label(
            self.frame,
            text="Probability"
        )
        self.probabilityInput = tk.Entry(
            self.frame
        )
        self.getZscoreButton = tk.Button(
            self.frame,
            text="Get Z-score",
            command=self.__submitAction
        )

        self.zscoreLabel = tk.Label(
            self.frame,
            text="Z-score = "
        )

        # Keybindings
        self.probabilityInput.bind('<Return>',func=lambda x:self.__submitAction())


    def __submitAction(self):
        try:
            pvalue = float(self.pValueVar.get())
            zscore = norm.ppf(pvalue)
            self.zScoreOutput.set("{:.3f}".format(zscore)) 
        except Exception as err:
            print(f'z-score critical value Error: {err}')

    def __radioAction(self):
        try:
            probability = float(self.probabilityInput.get())
            alpha = 1 - probability
            pscore = 0
            if self.radioValue == 1:
                pscore = 1-(alpha/2)
            elif self.radioValue == 2:
                pscore = 1-alpha
                
            self.pValueVar.set(pscore)
        except Exception as err:
            print(f'z-score critical value Error: {err}')


    def pack(self, pady=0, padx=0):
        radioRows = 0
        self.frame.pack(fill=BOTH,pady=pady,padx=padx,expand=1)
        self.probabilityLabel.grid(row=0, column=0)
        self.probabilityInput.grid(row=0, column=1)
        self.getZscoreButton.grid(row=0, column=2)
        # Radio buttons
        for option,value in self.tailedOptions:
            currRadio = tk.Radiobutton(
                self.frame,
                text=option,
                value=value,
                variable=self.radioValue,
                command=self.__radioAction
            )
            currRadio.grid(row=1+radioRows, column=0)
            radioRows+= 1
        # Pscore
        self.pValueLabel.grid(row=1, column=2, rowspan=radioRows)
        # Output
        self.zscoreLabel.grid(row=2+radioRows, column=0)
        self.zScoreOutputPrint.grid(row=2+radioRows,column=1, columnspan=2)

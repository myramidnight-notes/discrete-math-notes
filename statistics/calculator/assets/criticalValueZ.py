import tkinter as tk
from tkinter.constants import BOTH, LEFT
from scipy.stats import norm
import numpy as np

class CriticalValueZlookup:
    """Just creating a GUI to get the t-score from SciPy"""
    def __init__(self,window): 
        # When I only want to get a value from Z-score
        self.frame = tk.LabelFrame(
            window,
            text="Scipy z-score (critical value)"
        )

        # Output label (that displays the output)
        self.zScoreOutput = tk.StringVar()
        self.zScoreOutputPrint = tk.Label(
            self.frame,
            textvariable=self.zScoreOutput
        )

        # Label for the probability input
        self.probabilityLabel = tk.Label(
            self.frame,
            text="P-value"
        )
        
        # Probability input
        self.probabilityInput = tk.Entry(
            self.frame
        )

        # submit button
        self.getZscoreButton = tk.Button(
            self.frame,
            text="Get Z-score",
            command=self.__submitAction
        )
        
        # label for explaination
        self.labelExplaination = tk.Label(
            self.frame,
            wraplength=400,
            justify='left',
            text="P-value = 1 - (alpha/2)"
        )

        # label for the output zScoreOutputPrint
        self.zscoreLabel = tk.Label(
            self.frame,
            text="Z-score = "
        )

        # Keybindings
        self.probabilityInput.bind('<Return>',func=lambda x:self.__submitAction())

    #=============== SUBMIT ACTION  =======================
    def __submitAction(self):
        try:
            probability = float(self.probabilityInput.get())
            zscore = norm.ppf(probability)
            self.zScoreOutput.set("{:.3f}".format(zscore)) 
        except Exception as err:
            print(f'z-score critical value Error: {err}')

    #=============== PACK THE WIDGETS =======================
    def pack(self, pady=0, padx=0):
        self.frame.pack(fill=BOTH,pady=pady,padx=padx,expand=1)
        self.labelExplaination.grid(row=0, column=0, columnspan=2)
        self.probabilityLabel.grid(row=1, column=0)
        self.probabilityInput.grid(row=1, column=1)
        self.getZscoreButton.grid(row=1, column=2)
        self.zscoreLabel.grid(row=2, column=0)
        self.zScoreOutputPrint.grid(row=2,column=1, columnspan=2)

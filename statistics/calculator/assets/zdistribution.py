import tkinter as tk
from tkinter.constants import BOTH
from scipy.integrate import quad
import numpy as np



class Zdistribution:
    """Just creating a GUI to get the t-score from SciPy"""
    def __init__(self,window): 
        self.frame = tk.LabelFrame(
            window,
            text="SciPy Z-score (standard score)"
        )

        # The answer (when calculating z-score)
        self.output = tk.StringVar()
        self.output.set("")
        self.outputPrint = tk.Label(
            self.frame,
            textvariable=self.output
        )
        # Input for Sample Size
        self.rawScoreLabel = tk.Label(
            self.frame,
            text="Raw Score (X)"
        )
        self.rawScoreInput = tk.Entry(
            self.frame
        )

        # Input for Population Mean
        self.popMeanLabel = tk.Label(
            self.frame,
            text="Population Mean"
        )
        self.popMeanInput = tk.Entry(
            self.frame
        )

        # Input for Standard deviation
        self.standardDevLabel = tk.Label(
            self.frame,
            text="Standard Deviation"
        )
        self.standardDevInput = tk.Entry(
            self.frame
        )
        
        self.zscoreLabel = tk.Label(
            self.frame,
            text="Z-score = "
        )

        self.getZscore = tk.Button(
            self.frame,
            text="Get z-score",
            padx=5,
            pady=5,
            command=self.__submitAction
        )

        #Key bindings
        self.standardDevInput.bind('<Return>',func=lambda x:self.__submitAction())
        self.popMeanInput.bind('<Return>',func=lambda x:self.__submitAction())
        self.rawScoreInput.bind('<Return>',func=lambda x:self.__submitAction())

    def __submitAction(self):
        try:
            value = float(self.rawScoreInput.get())
            popMean = float(self.popMeanInput.get())
            standardDev = float(self.standardDevInput.get())

            score = round(((value - popMean)/standardDev),2)
            self.output.set("{:.2f}".format(score))
            
        except Exception as err:
            print(f'T-dist Error: {err}')

    def pack(self, pady=0, padx=0):
        self.frame.pack(fill=BOTH,pady=pady,padx=padx,expand=1)
        self.rawScoreLabel.grid(row=0, column=0)
        self.rawScoreInput.grid(row=0, column=1)
        self.popMeanLabel.grid(row=1, column=0)
        self.popMeanInput.grid(row=1, column=1)
        self.standardDevLabel.grid(row=2, column=0)
        self.standardDevInput.grid(row=2, column=1)
        self.getZscore.grid(row=0, column=3,rowspan=3)
        self.zscoreLabel.grid(row=3, column=0)
        self.outputPrint.grid(row=3, column=1)
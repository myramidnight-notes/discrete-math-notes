import tkinter as tk
from tkinter.constants import BOTH
from scipy.integrate import quad
import numpy as np

class ZtableLookup:
    """Just creating a GUI to get the t-score from SciPy"""
    def __init__(self,window): 
        # When I only want to get a value from Z-score
        self.frame = tk.LabelFrame(
            window,
            text="Z-table lookup"
        )

        self.onlyZtableVal = tk.StringVar()
        self.onlyZtablePrint = tk.Label(
            self.frame,
            textvariable=self.onlyZtableVal
        )
        self.onlyZtableScoreLable = tk.Label(
            self.frame,
            text="Z-score"
        )
        self.onlyZtableInput = tk.Entry(
            self.frame
        )
        self.onlyZtableButton = tk.Button(
            self.frame,
            text="Get value",
            command=self.__submitAction
        )

        self.zscoreLabel = tk.Label(
            self.frame,
            text="Percentage under the curve = "
        )
        
        # Keybindings
        self.onlyZtableInput.bind('<Return>',func=lambda x:self.__submitAction())


    def __submitAction(self):
        try:
            zscore = float(self.onlyZtableInput.get())
            percentage = self.zTableValue(zscore)
            self.onlyZtableVal.set("{:.2f}%".format(percentage)) 
        except Exception as err:
            print(f'Z-table Error: {err}')


    def zTableValue(self,zscore):
        def normalProbabilityDensity(x):
            constant = 1.0 / np.sqrt(2*np.pi)
            return (constant * np.exp((-x**2) / 2))
        
        percentage,_ = quad(normalProbabilityDensity, np.NINF, zscore)
        return percentage*100


    def pack(self, pady=10, padx=10):
        self.frame.pack(fill=BOTH,pady=pady,padx=padx,expand=1)
        self.onlyZtableScoreLable.grid(row=0, column=0)
        self.onlyZtableInput.grid(row=0, column=1)
        self.onlyZtableButton.grid(row=0, column=2)
        self.zscoreLabel.grid(row=1, column=0)
        self.onlyZtablePrint.grid(row=1,column=1, columnspan=2)

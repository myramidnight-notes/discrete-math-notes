import tkinter as tk
from tkinter.constants import BOTH
from scipy.stats import t as tdist


class CriticalValueT:
    """Just creating a GUI to get the t-score from SciPy"""
    def __init__(self, window): 
        self.frame = tk.LabelFrame(
            window,
            text="SciPy t-score (Student's t critical value)"
        )

        # The answer
        self.output = tk.StringVar()
        self.output.set("")
        self.outputPrint = tk.Label(
            self.frame,
            textvariable=self.output
        )

        # Input for Sample Size
        self.sampleSizeLabel = tk.Label(
            self.frame,
            text="Sample size"
        )
        self.sampleSizeInput = tk.Entry(
            self.frame
        )

        # Input for Confidence interval
        self.confIntervalLabel = tk.Label(
            self.frame,
            text="Confidence Interval"
        )
        self.confIntervalInput = tk.Entry(
            self.frame
        )

        self.tscoreLabel = tk.Label(
            self.frame,
            text="t-score = "
        )

        self.getTscore = tk.Button(
            self.frame,
            text="Get t-score",
            padx=5,
            pady=5,
            command=self.__submitAction
        )

        # Key bindings
        self.sampleSizeInput.bind('<Return>',func=lambda x:self.__submitAction())        
        self.confIntervalInput.bind('<Return>',func=lambda x:self.__submitAction())




    def __submitAction(self):
        try:
            df = int(self.sampleSizeInput.get()) - 1
            ci = float(self.confIntervalInput.get())/100
            alpha = 1 - ci
            tscore = tdist(df).ppf(alpha/2)

            self.output.set("{:.4f}".format(tscore))


            
        except Exception as err:
            print(f'T-dist Error: {err}')

    def pack(self, pady=0, padx=0):
        self.frame.pack(fill=BOTH,pady=pady,padx=padx,expand=1)
        self.sampleSizeLabel.grid(row=0, column=0)
        self.sampleSizeInput.grid(row=0, column=1)
        self.confIntervalLabel.grid(row=1, column=0)
        self.confIntervalInput.grid(row=1, column=1)
        self.getTscore.grid(row=0, column=3,rowspan=2)
        self.tscoreLabel.grid(row=3, column=0)
        self.outputPrint.grid(row=3, column=1,columnspan=2)

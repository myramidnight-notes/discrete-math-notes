# My Statistical calculator
>Since using `scipy` was allowed to figure out standard and critical scores, I decided to just create my own calculator for those scores, rather than spending time looking it up in a table, or finding a website to do it for me.

Though using this, you still have to understand what's going on to show calculations, so this calculator won't save you without having some understanding.

It was mainly an excuse to play with `tkinter` to create a GUI

## Running the calculator
Requirements:
* Python3 at least
* `scipy` package
* `numpy` package

And then run the `calculator.py` script

## Features
* Get the __T-score__ by providing sample size and confidence interval
* Get the __Z-score__ by providing the raw score, population standard deviation and sample mean.
* Look up t-score in the __z-table__ 
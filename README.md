>This is part of [Glósubankinn](https://gitlab.com/myramidnight-notes) 

# Math notes
These are just my notes regarding math related subjects, and some of it might be in _Icelandic_ or contain localized notes, depending on the language the course was taught in.

These notes are compiled using LaTeX, for the mathmatical formulas. I like the fact that you can import and include files from other folders, to keep things organized.

> If the index is missing in the notes ('Yfirlit') when it has a lot of content, then it's probably a victim of a weird bug where the table of contents doesn't render (sometimes it stops working, sometimes it pops back in) when I was compiling the pdf.

### Localization (Icelandic)
>So I managed to find the dictionary for localized math. 
>* [__Orðaskrá Íslenska Stærðfræðafélagsins__](http://www.stae.is/os)

## Index
* ### [Discrete Mathematics](discrete_math/README.md) (`discrete_math`)
    > The first course in Discrete Mathmatics. Each chapter was compiled individually and can be rendered individually in their respected folders, and then it was combined into one big note collection. 
    * [Main discrete math notes](discrete_math/glosur_str1.pdf) (pdf)
        * These notes are mainly in Icelandic (with english words for reference)
        * Logic (Propositional and Predicate), Set theory, Functions, Sequences and Summaries, Matrices, Number theory, Induction, Counting, Relations
* ### [Mathematical analysis](math_analysis/README.md) (`math_analysis`) 
    > Stærðfræðigreining og tölfræði / Active Calculus and Statistics (CAST)
    >
    > Mostly just added knowledge towards the teacher's notes.
    * [Main mathematical analysis notes](math_analysis/tex_notes/glosur_math_analysis.pdf) (pdf)
        * Derivative, Integrals/Integration, Related rates
    * Teacher's Notes (`teacher_notes`)
        >digtally handwritten notes from teacher on the subject, with some added notes by myself sometimes. The title numbers match their section number in the [Active Calculus](https://activecalculus.org/single/book-1.html) book
* ### [Statistics](statistics/README.md) (`statistics`)
    * [Main statistical notes](statistics/tex_notes/glosur_statistics.pdf) (pdf)
        * Statistics, Distribution, Probability, Counting (Permutations/Combinations), Confidence Intervals, Hypothesis Testing, Correlation... 
        * Did include relevant things from the discrete math notes.
    * The __Probability__ notes are available individually
* ### [Linear Algebra](linear_algebra/README.md) (`linear_algebra`)
    > Functions
    * [Main linear algebra notes](linear_algebra/glosur_linear_algs.pdf) (pdf)
* ### [Logic and Proof with Computer Science](logic/README.md) (`logic`)
    >Felt it was getting a bit crowded to keep in one file, might combine it again later.
    * Propositional logic (Yrðingarrökfræði)
    * Natural Deduction and Proofs
    * Predicate Logic (Umsagnarrökfræði)
    * Modal Logic
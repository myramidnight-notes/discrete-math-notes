
\subsection{Writing proofs}
\begin{quote}
This is certainly not the only way to write proofs, but it's the one we will be using. It's all about the premises and reaching a conclusion, no matter how you go about it. Most of the different ways to write proofs seem to follow a similar pattern with the final line being the statement we wanted to prove. This can be both useful and confusing when trying to find material and examples to guide you, because they would be explaining the same things with different syntax.

\begin{example}{Prove that $p\wedge q, r \vdash q \wedge r$ is valid}{}
\begin{small}

We have the expression:
$$
p\wedge q, r \vdash q \wedge r
$$
\begin{enumerate}
\item We start by writing down the premises (that are listed before the $\vdash$)
\item then we leave a gap and write the conclusion (that comes after the $\vdash$)
\begin{quote}
\begin{tabular}{l}
$p\wedge q$\\
$r$ \\
\\
$q\wedge r$
\end{tabular}
\end{quote}
The task of constructing the proof is to fill the gap between the premises and the conclusion by applying a suitable sequence of proof rules.
\item In this case we can apply the rule $\wedge e_{2}$ to the first premise, $p \wedge q$. It will give us $q$.
$$
\frac{p \wedge q}{q} \wedge e_{2}
$$
\item Then we can apply $\wedge i$ to the $q$ we just got in previous step and to the second premise, $r$. It will give us $q \wedge r$.
$$
\frac{q \ \ \ r}{q\wedge r}\wedge i
$$

\item And that's it! We write the numbers of each line , and write the justification for each line, which will produce the following list/table:

\begin{logicproof}{1}
p \wedge q & premise\\
r & premise \\
q & $\wedge e_{2}$ 1\\
q\wedge r & $\wedge i$ 3,2
\end{logicproof}
\item[] How you write that last column (the justifications) isn't set in stone, you simply have to get across how you came to that conclusion, and what lines you used as premises for the rule you used. You could write the line numbers first and then what rule you used, or as in the examples, writing the rule name first and then the lines you used as premises.
\end{enumerate}
\end{small}
\end{example}
\href{https://www.youtube.com/watch?v=kFPlW-Lr6NA}{Natural deduction proof method for propositional logic (video)}: lots of examples that will make sense of the example above

\begin{example}{Assumptions and sub proofs}{}
\begin{small}
We sometimes have to assume something is true before we can actually prove it. That is where we get into sub-proofs

Let's prove that the following argument is valid
$$
(q \rightarrow r) \rightarrow ((\neg q \rightarrow \neg	p) \rightarrow (p \rightarrow r))
$$
Since we do not get any premises this time, we need to make assumptions about what's actually true, based on what we're trying to argue. This is recursive, so each assumption dives deeper into the conclusion we want to prove.

\begin{logicproof}{3}
\begin{subproof}
q \rightarrow r & assumption\\
\begin{subproof}
\neg q \rightarrow \neg p & assumption\\
\begin{subproof}
p & assumption \\
\neg\neg p & $\neg\neg i$ 3\\
\neg\neg q & MT 2,4 \\
q & $\neg\neg e$ 5 \\
r & $\neg\neg e$ 1, 6
\end{subproof}
p \rightarrow r & $\rightarrow i$ 3-7
\end{subproof}
(\neg q \rightarrow \neg p ) \rightarrow (p \rightarrow r ) & $\rightarrow i$ 2-8
\end{subproof}
(q \rightarrow r) \rightarrow ((\neg q \rightarrow \neg	p) \rightarrow (p \rightarrow r)) & $\rightarrow i$ 1 - 9
\end{logicproof}

Each box shows us the scope of the sub proof, the assumptions they make. And as mentioned, this is \textbf{recursive}, so it's not until we reach line 4 that we start applying rules to prove our point. Let's go through it.
\begin{enumerate}
\item We will assume that $q \rightarrow r$ is true
\item We will assume that $\neg q \rightarrow \neg p$ is true
\item We will assume $p$ is true (because of $(p \rightarrow r)$)
\item We can introduce $\neg\neg p$ from $p$ in line 3
\item If $\neg q \rightarrow \neg p$ is valid (2) which is equivalent to $\neg\neg q \rightarrow \neg\neg p$, and that gives us the $\neg\neg q$ through the \textit{Modus Tollus} rule.
\item We can get $q$ by removing the double negative in line 5
\item If $q\rightarrow r$ is valid, and we have proved $q$ (based on the assumption), then we can use the implication elimination to get $r$
\item We have now concluded that $p$ implies $r$ through the previous assumption, we can formally introduce $p \rightarrow r$
\item We have also concluded that we can get $p\rightarrow r$ from $\neg q \rightarrow \neg p$, so we can introduce $(\neg q\rightarrow \neg p) \rightarrow (p \rightarrow r)$
\item and again, coming out of this recursion, we've shown that we can get $(\neg q\rightarrow \neg p) \rightarrow (p \rightarrow r)$ from $q\rightarrow r$, so we can introduce $(q \rightarrow r) \rightarrow ((\neg q \rightarrow \neg	p) \rightarrow (p \rightarrow r))$
\end{enumerate}
\end{small}
\end{example}
\begin{center}

\includegraphics[width=0.6\textwidth]{images/example_proof_recursion.png}
This image shows us the recursion of previous example
\end{center}

\begin{myformula}{Base case: a One-line proof}{}
If the proof has length 1 (k=1), then it must be of the form

\begin{logicproof}{1}
\phi &premise
\end{logicproof}
\end{myformula}

\end{quote}
\section{Derived rules}
\begin{quote}
These rules of inference can be used as building blocks to construct more complicated valid argument forms. They are derivable from the base rules

The purpose of these derived rules is essentially to allow us to make more complicated proofs by skipping the steps that the rule itself proves.

\subsubsection*{What if we aren't allowed to use them?}
\begin{quote}
It might be the case that we have to prove everything, which would disallow the use of complicated derived rules. 

But that doesn't mean these rules can't help us. It can be difficult to plan how to prove something if we don't have a goal.

If we know the rule, \textbf{we can use it as a guideline} of what we're trying to prove, even if we have to include every step that we would have skipped by using that rule.
\end{quote}

\end{quote}

\subsection{Modus Tollens (MT)}
\begin{quote}

\begin{myrule}{Modus Tollens (MT)}{}
$$
( \phi\rightarrow\psi ), \neg\psi \vdash \neg\phi
$$

This rule goes by the latin name \textbf{Modus Tollens} 
$$
\dfrac{\phi \rightarrow \psi \ \ \ \neg \psi}{\neg \phi} MT
$$
"If $\phi$, then $\psi$. Not $\psi$. Therefore, not $\phi$"
\end{myrule}

The \textit{Modus Tollens} is like the $\rightarrow e$ rule in that it eliminates an implication. 

\begin{extra}{Proof that Modus tollens is valid}{}
Modus Tollens is not a primative rule of natural deduction, but it can be derived from some of the other rules. Here is the derivation of the Modus Tollens from the rules $\rightarrow e$, $\neg e$ and $\neg i$.

\begin{logicproof}{1}
\phi \rightarrow \psi & premise\\
\neg \psi & premise\\
\begin{subproof}
\phi & assumption\\
\psi & $\rightarrow e$ 1,3\\
\perp & $\neg e$ 4,2
\end{subproof}
\neg \phi & $\neg i$ 3-5
\end{logicproof}
If we assume $\phi$ is true, it should give us $\psi$, but the premise $\neg\psi$ makes it a contradiction, hence $\neg\phi$
\end{extra}

\begin{example}{Using Modus Tollens}{}
Let's prove that $p \rightarrow (q \rightarrow r), p, \neg r \vdash \neg q$
\begin{logicproof}{1}
p \rightarrow (q \rightarrow r ) & premise \\
p & premise\\
\neg r & premise\\
q \rightarrow r & $\rightarrow e$ 1, 2\\
\neg q	& MT 4,3
\end{logicproof}
\end{example}
\end{quote}

\subsection{Hypothetical Syllogism (HS)}
\begin{quote}
\begin{myrule}{Hypothetical Syllogism (HS)}{}
$$
(\phi\rightarrow\psi ),(\psi\rightarrow\chi ) \vdash  (\phi\rightarrow\chi )
$$
If we have both $\phi\rightarrow\psi$ and $\psi\rightarrow\chi$, that is essentially the same as $\phi\rightarrow\psi\rightarrow\chi$, which gives us $\phi\rightarrow\chi$
$$
\dfrac{\phi \rightarrow \psi \ \ \ \psi\rightarrow\chi}{\phi\rightarrow\chi} HS
$$
\end{myrule}
\begin{extra}{Proof of Hypothetical Syllogism is valid}{}
\begin{logicproof}{1}
\phi\rightarrow\psi & premise\\
\psi\rightarrow\chi & premise\\
\begin{subproof}
\phi & assumption\\
\psi & 1,3 $\rightarrow e$\\
\chi & 2,4 $\rightarrow e$
\end{subproof}
\phi \rightarrow \chi & 3-5 $\rightarrow i$
\end{logicproof}
\end{extra}
\end{quote}
\subsection{Disjunctive Syllogism (DS)}
\begin{quote}
\begin{myrule}{Disjunctive Syllogism (DS)}{}
$$
(\phi\vee\psi ), \neg\phi \vdash \psi
$$
If we know $\phi\vee\psi$ is valid, and that $\phi$ is false, then $\psi$ must be true
$$
\dfrac{\phi\vee\psi \ \ \ \neg\phi}{\psi} DS
$$
\end{myrule}

\begin{extra}{Proof that Disjunctive Syllogism is valid}{}
\begin{logicproof}{2}
\phi\vee\psi & premise\\
\neg\phi & premise\\
\begin{subproof}
\phi & assumption\\
\perp & 2,3 $\neg e$\\
\psi & 4 $\perp e$\\
\neg\phi \wedge\psi & 2,5 $\wedge i$
\end{subproof}
\begin{subproof}
\psi & assumption\\
\neg\phi \wedge \psi & 2,7 $\wedge i$
\end{subproof}
\neg\phi \wedge \psi & 1,3-6,7-8 $\vee e$\\
\psi & 9 $\wedge e_{2}$
\end{logicproof}

Because if $\phi$ is true, then that's a contradiction, and in order for $\phi\vee\psi$ to be valid, then $\psi$ needs to be true
\end{extra}
\end{quote}

\subsection{Resolution}
\begin{quote}
\begin{myrule}{Resolution}{}
$$
(\phi\vee\psi ), (\neg\phi\vee\chi ) \vdash (\psi \vee \chi )
$$
If $\phi\vee\psi$ and $\neg\phi\vee\chi$ is valid, and $\phi$  is a contradiction (cannot be both true and false), so either $\psi$ or $\chi$ must be true
$$
\dfrac{\phi\vee\psi \ \ \  \neg\phi\vee\chi}{\psi\vee\chi} R
$$
\end{myrule}

\end{quote}

\subsection{Constructive dilemma (CD)}
\begin{quote}
The \textit{\href{Constructive Dilemma}{Constructive Dilemma}} is a valid rule of inference of propositional logic. It is the disjunctive version of \textit{modus ponens}
\begin{myrule}{Constructive Dilemma (CD)}{}
$$
(P\rightarrow Q), (R\rightarrow S), (P\vee R) \vdash (Q\vee S)
$$
if $P$ implies $Q$ and $R$ implies $S$, and either $S$ and either $P$ or $R$ is true, then either $Q$ or $S$ has to be true.
$$
\dfrac{(P\rightarrow Q), (R\rightarrow S), (P\vee R)}{Q\vee S} DS
$$
\end{myrule}
\end{quote}

\subsection{Proof by Contradiction (PBC)}
\begin{quote}
It also has the latin name \textit{Reductio ad absurdum} (meaning "reduction to absurdity") which we simply call \textit{proof by contradiction}.

Because proof by contradiction does not prove a result directly, it is another type of indirect proof.  

\begin{myrule}{Proof by Contradiction (PBC)}{}
\begin{center}
\includegraphics[width=0.2\textwidth]{images/rule_pbc.png}
\end{center}
The rule says: if from $\neg\phi$ we obtain a contradiction, then we are entitled to deduce $\phi$
\end{myrule}
\begin{example}{Proof that \textit{Proof by Contradiction} is valid}{}
Suppose we have a proof of $\perp$ from $\neg\phi$. By $\rightarrow i$, we can transform this into a proof of $\neg\phi \rightarrow \perp$ and proceed as follows
\begin{logicproof}{1}
\neg\phi\rightarrow\perp & given\\
\begin{subproof}
\neg\phi & assumption\\
\perp	& $\rightarrow e$ 1,2
\end{subproof}
\neg\neg\phi & $\neg i$ 2-3\\
\phi & $\neg\neg e$ 4
\end{logicproof}
This example shows that \textit{PBC} can be derived from $\rightarrow i$, $\neg i$, $\rightarrow e$ and $\neg\neg e$


So this rule simply lets us skip a step of having to go through the double negation
\end{example}
\begin{extra}{Proving conditional statements with contradictions}{}
\begin{small}
Proof by contradiction can be used to prove conditional statements. In such proofs, we first assume the negation of the conclusion. We then use the premises of the theorem and the negation of the conclusion to arrive at a contradiction.

The reason that such proofs are valid on the logical equivalence of $p\rightarrow q$ and $(p\wedge\neg q)\rightarrow \textbf{F}$. To see that these statements are equivalent, simply note that each is false in exactly one case, namely, when $p$ is true and $q$ is false
\end{small}
\end{extra}
\begin{example}{Prove "If 3\textit{n}+2 is odd, then \textit{n} is odd" with contradiction}{}
\begin{small}
\begin{enumerate}
\item[] Let $p$ be "3\textit{n} + 2 is odd" and $q$ be "\textit{n} is odd".
\item To construct a proof by contradiction, assume both $p$ and $\neg q$ are true. That is, assume that $3n + 2$ is odd and that $n$ is not odd. (we assume the negation of the conclusion)
\item[] Because $n$ is not odd, we know that it is even. 
\item[] Because $n$ is even, there is an integer $k$ such that $n=2k$. 
\item This implies that $3n+2 = 3(2k)+2 = 6k + 2 = 2(3k +1)$
\item[] Because $3n+2$ is $2t$, where $t=3k+1$, then $3n+2$ is even.
\item[] Note that the statement "3\textit{n}+2 is even" is equivalent to the statement $\neg p$, because an integer is even if and only if it is not odd.
\item Because both $p$ and $\neg p$ are true, we have a contradiction.
\item[]This completes the proof by contradiction, proving that if $3n+2$ is odd, then $n$ is odd.
\end{enumerate}
\end{small}
\end{example}
\begin{footnotesize}
This example came from the book \textit{Discrete Mathematics} by Kenneth H. Rosen
\end{footnotesize}
\end{quote}

\subsection{Law of the Excluded middle (LEM)}
\begin{quote}
It also has a Latin name, \textit{tertium non datur}. It is also called "law of non-contradiction" 

This law states that for every proposition, either the proposition or its negation is true. 

Basically saying that a statement is either true or false, it can't be neither.
\begin{myrule}{Law of the Excluded Middle (LEM)}{}
These two versions of the rule essentially show that it doesn't matter if we have a contradiction or not, that $\phi\vee\neg\phi$ still stands.
$$
\dfrac{\top}{\phi\vee\neg\phi} LEM
$$
\begin{center}
or
\end{center}
$$
\dfrac{}{\phi\vee\neg\phi} LEM
$$
$\phi \vee \neg\phi$ is true, whatever $\phi$ is.
\end{myrule}
\href{https://proofwiki.org/wiki/Law_of_Excluded_Middle/Proof_Rule}{ProofWiki: Law of excluded middle}
\begin{example}{LEM}{}
\begin{itemize}
\item[True] The world is flat
\item[False] The world is flat
\end{itemize}
Conclusion, the claim is false (because science has proven that it is indeed not flat).
\end{example}

\begin{extra}{Proving that LEM is valid}{}
Here we show that LEM is a valid rule, with the use of contradictions.

\begin{logicproof}{2}
\begin{subproof}
\neg (\phi\vee\neg\phi ) & assumption\\
\begin{subproof}
\phi & assumption \\
\phi \vee\neg\phi & $\vee i_{1}$ 2\\
\perp & $\neg e$ 3,1
\end{subproof}
\neg\phi & $\neg i$ 2-4\\
\phi\vee\neg\phi & $\vee i_{2}$\\
\perp & $\neg e$ 6,1
\end{subproof}
\neg\neg (\phi\vee\neg\phi ) & $\neg i$ 1-7\\
\phi\vee\neg\phi & $\neg\neg e$ 8
\end{logicproof}

\begin{itemize}

\item we assume that $\phi\vee\neg\phi$ is not true
\item then assume $\phi$ is true 
\item using or-introduction to get $\phi\vee\neg\phi$, we get that it is a contradiction with the assumption in line 1
\item we infer that $\neg\phi$ is valid (because of the contradiction in 2-4)
\item  introduce $\phi\vee\neg\phi$ based on that conclusion, which also happens too be a contradiction.
\item So $\neg (\phi\vee\neg\phi )$ is not true, becomes a double negative, which comes to the final conclusion that $\phi\vee\neg\phi$ is actually a valid statement
\end{itemize}
\end{extra}

\begin{example}{Using LEM to prove $p\rightarrow q\vdash \neg p\vee q$ is valid}{}
We want to show that $p\rightarrow q\vdash \neg p\vee q$ is valid:
\begin{logicproof}{1}
p\rightarrow q & premise\\
\neg p\vee p	& LEM\\
\begin{subproof}
\neg p & assumption\\
\neg p\vee q & 3 $\vee i_{1}$
\end{subproof}
\begin{subproof}
p & assumption \\
q & 1,5 $\rightarrow e$\\
\neg p \vee q & 6 $\vee i_{2}$
\end{subproof}
\neg p \vee q & 2,3-4,5-7 $\vee e$
\end{logicproof}
\end{example}
The LEM simply gives us that either $p$ is true or it is false: $\neg p \vee p$, and we get to use that with our assumptions to prove our case
\end{quote}
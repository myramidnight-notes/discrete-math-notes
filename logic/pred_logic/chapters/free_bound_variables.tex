\section{Free and Bound variables}
\begin{quote}
\begin{itemize}
\item \href{https://www.youtube.com/watch?v=drQoITU5zs0}{Logic: Free and Bound variables (video)}
\end{itemize}
\begin{definition}[Free variable]
Any variable in an expression that is \textbf{not} bound \\by quantifiers ($\forall$ or $\exists$) is called a free variable.\\

Let $\phi$ be a formula in predicate logic.
An occurrence of \textit{x} in $\phi$ is free if it is a leaf node in the parse tree of $\phi$ such that there is no path upwards from that node \textit{x} to a node $\forall x$ or $\exists x$
\end{definition}

\begin{definition}[Bound variable]
Variables in the scope of some quantifier ($\forall$ or $\exists$) are called bound variables
\end{definition}
So basically the quantifier only binds the associated variable within the brackets of it's scope. Meaning that somewhere above variable \textit{x} in the parse tree of formula $\phi$ is a node $\forall x$ or $\exists x$, which puts the variable within it's scope.
\begin{definition}[Scope of quantifiers]
For $\forall x \phi$, or $\exists x\phi$, we say that $\phi$, minus any of $\phi$'s subformulas $\exists x \psi$ or $\forall x \psi$, is the scope of $\forall x$, respectively $\exists x$\\
\end{definition}

Thus, if \textit{x} occurs in $\phi$, then it is bound if, and only if, it is in the scope of some $\exists x$ or some $\forall x$; otherwise it is free.

\begin{extra}{Tree of Bound and Free variables}{}
\begin{small}
\begin{center}
\includegraphics[width=0.6\textwidth]{images/free_bound_var_tree.png}
\end{center}
The tree in the picture is the parsing of the following formula:
$$
(\forall x (Px\wedge Qx)) \rightarrow (\neg Px\wedge Qy)
$$
\begin{itemize}
\item $\forall x$ only effects the scope on the left-side.
\item Since \textit{x} cannot be both free and bound at the same time. So if the bound part of the expression is true, then that binding effects the right-side as well (because of the implication).
\end{itemize}
\end{small}
\end{extra}
\end{quote}
\subsection{Well formed sentences}
\begin{quote}
\begin{definition}[Well formed sentence]
A sentence is well formed if and only if  there are  \textbf{no occurrences of free variables}  in a sentence, and it follows all other syntactic rules.
\end{definition}

We don't like free variables, we like constants 


\begin{extra}{What is a well formed sentence?}{}
\begin{small}
Let's say we had sentences that have $\forall x$ or $\exists x$. \\

To qualify for a well formed sentence, then the following needs to apply:
\begin{enumerate}
\item There cannot be both bound and free instances of \textit{x}.
\begin{quote}
All instances of \textit{x} need to be within the scope of the quantifier (bound to it)
\end{quote}
\item There exists no free variable \textit{x}. 
\begin{quote}
No instances of \textit{x} actually qualifies, since that even if there are no bound instances, there are also no free instances either.
\end{quote}
\end{enumerate}
\end{small}
\end{extra}



\begin{example}{Well formed sentences}{}
\begin{small}
The following sentence is fine, because there is \textbf{no free variable \textit{x}}.
$$
\exists x (Dx \wedge Gx ) \wedge Pa
$$
\begin{itemize}
\item  There is nothing trying to bind \textit{a} to a quantifier, so we don't worry about that.
\end{itemize}
\end{small}
\end{example}

\begin{example}{Not a well formed sentence}{}
\begin{small}
The following sentence has a problem, we now have \textbf{both free and bound variable x}
$$
\exists x (Dx\wedge Gx) \wedge Px
$$ 
\begin{itemize}
\item $\exists$ is binding \textit{x}, but $Px$ is outside of the scope, which leaves that occurrence of \textit{x} free
\item it makes the sentence vague. We don't know what the bounds of the free variable \textit{x}
\item is it the same \textit{x} or a different \textit{x}? Because scope means everything to variables. 
\item We don't know because it's just a variable, and it's not bound by a scope of a quantifier.
\item Binding variables lets us know what they are referring to
\end{itemize}

We can have propositions mixed in with our predicates, since they are not variables, but statements.
\end{small}
\end{example}
\begin{example}{Quantifier with no corresponding variables}{}
\begin{small}
What if there are quantifiers without their corresponding variables appearing in the sentence?
$$
\exists x (Pa)
$$
This sentence is actually well formed
\begin{itemize}
\item We have technically bound all the \textit{x} variables in that sentence, there is no free occurrence of it (it just happens there are no occurrences of \textit{x})
\item The point of well formed sentences is not that there exists bound variables, but that \textbf{there exists no free variables \textit{x}}.
\end{itemize}
\end{small}
\end{example}

\begin{example}{Incorrect use of quantifier}{}
This is a nonsense sentence, because quantifiers require you to associate it with a variable
$$
\exists (Px)
$$
so it would have to be like this:
$$
\exists x (Px)
$$
\end{example}


\end{quote}
\subsection{Intuitive look at quantifiers}
\begin{quote}
\begin{extra}{$\forall$ is like conjunction}{}
$$
\forall x (Px) = P_1\wedge P_2 \wedge ... \wedge P_n
$$
What it is saying, is that all \textit{x} have the property \textit{P}
\end{extra}

\begin{extra}{$\exists$ is like disjunction}{}
$$
\exists x (Px) = P_1 \vee P_2 \vee ... \vee P_n
$$
What we are saying, is that there is at least one \textit{x} that has the property \textit{P}, which would make the sentence true.
\end{extra}

\begin{example}{Negating quantifiers (Proof of De Morgan's Law for quantifiers)}{}
Since we can represent the quantifiers as conjunctions and disjunctions. So if we are negating either of the quantifiers, we can use \textit{De Morgan's Law} to invert the operators and distribute the negation.

$$
\neg \forall (Px) \leftrightarrow \exists x (\neg Px)
$$
Looking at the negation of $\forall$
\begin{align*}
\neg \forall x (Px) = \neg(P_1 \wedge ... \wedge P_n)\\
= \neg P_1 \vee ... \vee \neg P_n\\
= \exists x (\neg Px)
\end{align*}

Looking at the negation of $\exists$
\begin{align*}
\neg \exists x (Px) = \neg(P_1 \vee ... \vee P_n)\\
= \neg P_1 \wedge ... \wedge \neg P_n\\
= \forall x (\neg Px)
\end{align*}
\end{example}

\begin{example}{Another trick for negation with quantifiers}{}

\begin{align*}
\forall x = \neg\exists x \neg\\
\exists x = \neg\forall x \neg
\end{align*}
\begin{enumerate}
\item Think of this in truths (let's use + to indicate truth in this example), and we changes the plusses to minuses when we negate.
\begin{align*}
+ \forall x  + Px\\
- \exists x - Px\\
\end{align*}
\item And the minuses are negation (because not true is negation): $\neg \exists x \neg Px$.
\item We of course don't actually write the + into our solutions, we only specify if something is not true.
\end{enumerate}

We get the same thing by using De Morgan's rule
\begin{align*}
\neg \exists x + Px\\
+ \forall x \neg Px
\end{align*}
This actually works the same in Modal logic (later). You can think of this example when you spot it.
\end{example}
\end{quote}


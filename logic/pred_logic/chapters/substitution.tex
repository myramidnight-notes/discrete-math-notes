
\section{Substitution (replacement of terms)}
\begin{quote}
Variables are just place-holders, to allow us to replace them with more concrete information. We could replace \textit{x} with another statement, a value or even a complex statements. If you think of it as the tree structure, then you could replace the variable node with another tree.

\begin{definition}[Substitution]
To apply a substitution to an expression means to consistently replace its variable, or placeholder, symbols by other expressions.\\

Given a variable \texttt{x}, a term \textit{t} and a formula $\phi$. We define $\phi [t/x]$ to be the formula obtained by replacing each \textbf{free} occurrence of variable \texttt{x} in $\phi$ with \texttt{t}
\begin{itemize}
\item Simply: $\phi[t/x]$ translates to replacing all free \texttt{x} with \texttt{t} in the formula $\phi$ 
\item "$\phi$ will have \texttt{t} instead of \texttt{x}" if ordered by how we define it.
\end{itemize}
\end{definition}

\begin{definition}[Substitution instance]
The expression that results from applying \\substitution is called a \textbf{substitution instance} of the original expression.
\end{definition}

\begin{example}{Substituting with predicates}{}
Let \textit{f} be a function symbol with two arguments, and $\phi$ is the formula
$$
(\forall x (Px\wedge Qx)) \rightarrow (\neg Px\wedge Qy)
$$
\begin{itemize}
\item $f(x,y)$ is a term
\item $\phi[f(x,y)/x]$ means that we are replacing all \textit{x} in $\phi$ with $f(x,y)$
\item The definition mentions that we only replace \textbf{free} variables when substituting
\item This means not all \textit{x} in $\phi$ get replaced, only the free instances.
\end{itemize}
\begin{center}
\includegraphics[width=0.7\textwidth]{images/parse_tree_substituded.png}
\end{center}
$$
(\forall x (Px\wedge Qx)) \rightarrow (\neg P(f(x,y))\wedge Qy)
$$
\begin{itemize}
\item So all \textit{x} on the left-hand side of the implication is left alone, since that is bound by the scope of $\forall x$. 
\item This is still not a \textit{well formed sentence}, because there are still free variables of \textit{x}. 
\item If we wanted to prevent \textit{x} being left free, then we could have substituted all instances of \textit{x} from $f(x,y)$ with something else, before we used it to substitute \textit{x} in the example sentence. Or do another substitution afterwards that replaces the free \textit{x} with some different named variable.
\end{itemize}

{\footnotesize I decided to remove the brackets of the predicates when it's clear what variable it associates with, just to make things a bit more readable.}
\end{example}

\begin{extra}{Possible side-effects of substituting}{}

But there can be undesirable side-effects when substituting, because the replacement term could include anything. 
\begin{itemize}
\item Let's consider performing a substitution $\phi[t/x]$ (replacing \textit{x} with \textit{t}), where all occurences of \textit{x} are under the scope of $\exists y$ or $\forall y$
\item The term \textit{t} might contain a variable \textit{y}, which might have been fixed by a concrete context
\item Then by carrying out the substitution $\phi[t/x]$ would introduce \textit{y} that gets caught in the scope of the quantifiers in $\phi$
\item This binding capture overrides the context specifications of the concrete value of \textit{y}
\end{itemize}
\end{extra}

\begin{definition}["term is free for variable in formula"]
Given a term \texttt{t}, a variable \texttt{x} and a formula $\phi$. We say that \texttt{t} is free for \texttt{x} in $\phi$ if no free \texttt{x} leaf in $\phi$ occurs in the scope of $\forall y$ or $\exists y$ for any variable \texttt{y} occurring in \texttt{t}
\begin{itemize}
\item What "\texttt{t} is free for \texttt{x} in $\phi$" means is that the variable leaf-nodes of the parse tree of \texttt{t} won't become bound if placed into the bigger parse tree of $\phi[t/x]$
\item So it is a way to say that nothing introduced by \texttt{t} will be caught in the scopes of quantifiers from $\phi$
\end{itemize}
\end{definition}




\subsection{Renaming variables before substituting}
\begin{extra}{Renaming variables to avoid capture when substituting}{}
To avoid any captures of variables when substituting with $\phi[t/x]$, then we should rename any variables within \textit{t} that might match existing variables in $\phi$, so that when you plug in \textit{t}, then they wont get captured in the scopes of $\phi$.\\

Let's say we have the following formula
$$
(\forall x (P(x)\wedge Q(x))) \rightarrow \exists y(\neg P(f(x,y))\wedge Q(y))
$$
\begin{itemize}
\item It is the same formula from previous example (with the parse tree), but we added a $\exists y$ quantifier to the right-side
\item This means that \textit{x} is still a free variable on the right-side, but \textit{y} is bound by $\exists y$
\end{itemize}
And we want to substitute $\phi[f(x,y)/x]$, but you might notice that $f(x,y)$ has a \textit{y}, which will become captured once we replace \textit{x} with \textit{f(x,y)}.\\
\begin{itemize}
\item To avoid this future capture, we would first do substitution on $f(x,y)$: $f(x,y)[v/y]$, so now we have $f(x,v)$ instead. \\While the function itself stays intact, their variables would have different names (because the formula doesn't care what name you pick for the variables, just that they are unique).
\item Then we would be doing $\phi[f(x,v)/x]$, all captures avoided.
\end{itemize}
\end{extra}

\end{quote}
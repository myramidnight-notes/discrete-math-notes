\subsection{Rules for universal quantification: $\forall$}
\begin{quote}
\begin{myrule}{For all elimination: $\forall x e$}{}
$$
\dfrac{\forall x \phi}{\phi[t/x]} \forall x e
$$
It says: If $\forall x \phi$ is true, then you could replace the \textit{x} in $\phi$ by any term \textit{t} (given, as usual, the side condition that \textit{t} be free for \textit{x} in $\phi$) and conclude that $\phi[t/x]$ is true as well.
\end{myrule}

\begin{myrule}{For all introduction: $\forall x i$}{}
\begin{small}
\begin{center}
\includegraphics[width=0.25\textwidth]{images/rule_forall.png}
\end{center}

It says: If, starting with a \textit{fresh} variable $x_0$, you are able to prove some formula $\phi[x_0 / x]$ with $x_0$ in it, then (because $x_0$ is \textit{fresh}) you can derive $\forall x \phi$
\begin{itemize}
\item The important thing to keep in mind is that $x_0$ is a new variable which does not occur anywhere outside of its box. We think of it as an \textbf{arbitrary} term
\item Since we assume nothing about this $x_0$, anything would work in its place
\end{itemize}
\end{small}
\end{myrule}

Notice that, although the square brackets representing substitution appear in the rules $\forall i$ and $\forall e$, they do not appear when we use those rules. The reason is because the rule itself carries out the substitution.

\begin{extra}{Analogy to understand $\forall x i$ }{}
\begin{small}
If you want to prove to someone that you can, say, split a tennis ball in your hand by squashing it.
\begin{enumerate}
\item You might say "\textit{OK, give me a tennis ball and I'll split it}"
\item So we give you one and you do it. 
\item But how can we be sure that you could split \textit{any} tennis ball this way?
\item Of course, we can't give you \textit{all of them}, so how could we be sure that you could split any one?
\item Well, we assume that the one you did split was an \textit{arbitrary (or 'random')} one. That it wasn't special in any way (like a ball which you may have \textit{prepared} beforehand).
\item This is enough to convince us that you could split \textit{any} tennis ball
\end{enumerate}
Our rule says that if you can prove $\phi$ about an $x_0$ that isn't special in any way, then you could prove it for any \textit{x} whatsoever
\end{small}
\end{extra}

\begin{example}{The realm of birds}{}
The assumption \textit{bird(x)} confines \textit{x} to a realm of birds and anything we can prove about \textit{x} using this formula will have to be a statement restricted to birds and not about anything else we might have had in mind.
\end{example}

\begin{example}{Using the $\forall x$ rules in proofs}{}

\begin{small}
We have the following sequent we want to prove
$$
\forall x(P(x)\rightarrow Q(x)), \forall x P(x) \vdash \forall x Q(x)
$$
{
\setlength\subproofhorizspace{2em}
\begin{logicproof}{1}
\forall x (P(x)\rightarrow Q(x)) & premise\\
\forall x P(x) & premise\\
\begin{subproof}
\llap{$x_{0}\quad$} P(x_{0}) \rightarrow Q(x_{0}) &$\forall x e$ 1  \\
P(x_0) &$\forall x e$  2 \\
Q(x_0) &$\rightarrow e$  3,4 
\end{subproof}
\forall x Q(x) &$\forall x i$ 3-5 
\end{logicproof}
}
\begin{itemize}
\item So the $x_0$ represents \textit{x} within the subproof, in line 3 they remove the scope of $\forall x$ to see how things would play out without it.
\item Because of the premises we get, we can also extract that we have $P(x)$, and therefore we will get $Q(x)$ 
\item that proves that $Q(x)$ is in the scope of $\forall x$
\end{itemize}
\end{small}
\end{example}

\begin{example}{Simple example, match the terms }{}
\begin{small}
$$
P(t), \forall x (P(x)\rightarrow \neg Q(x))\vdash \neg Q(t)
$$
We want to prove the validity of the sequent above for any term \textit{t}
\begin{logicproof}{1}
P(t)&premise\\
\forall x (P(x)\rightarrow \neg Q(x)) & premise\\
P(t)\rightarrow \neg Q(t) & $\forall x e$ 2\\
\neg Q(t) & $\rightarrow e$ 3,1
\end{logicproof}
\begin{itemize}
\item We invoked $\forall x e$ with the same instance \textit{t} as in the premise $P(t)$. 
\item If we had invoked $\forall x e$ with \textit{y}, $P(y)\rightarrow Q(y)$, then that would have been valid, but it would not been helpful towards a proof with \textit{t}
\item We should make our choice on the basis of consistent pattern matching.
\end{itemize}
\end{small}
\end{example}

\begin{extra}{Compare $\forall$ rules to conjunction $\wedge$ rules}{}
\begin{small}
A helpful way to understand universal quantifier ($\forall$) rules is to compare the rules for $\forall$ with those for $\wedge$.\\

The rules for $\forall$ are in some sense generalisations of those for $\wedge$
\begin{itemize}
\item whereas $\wedge$ has just two conjuncts, $\forall$ acts like it conjoins lots of formulas (one for each substitution instance of its variable).
\item whereas $\wedge i$ has two premises, $\forall x i$ has a premise $\phi[x_0/x]$ for each possible 'value' of $x_0$
\item $\wedge e$ allows you to deduce from $\phi \wedge \psi$ whichever of $\phi$ and $\psi$ you like, \\
$\forall x e$ allows you to deduce $\phi[t/x]$ from $\forall x \phi$, for whichever \textit{t} you (and the side condition) like
\item think of $\forall x i$  as saying: to prove $\forall x \phi$, you have to prove $\phi[x_0/x]$ for every possible value $x_0$; while $\wedge i$ says that to prove $\phi_1 \wedge \phi_2$ you have to prove $\phi_i$ for every possible \textit{i} = 1,2 
\end{itemize}
\end{small}
\end{extra}

\end{quote}
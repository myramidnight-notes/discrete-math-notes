
\subsection{Rules for existential quantification: $\exists$}
\begin{quote}
\begin{myrule}{Exists-introduction: $\exists x i$}{}
\begin{small}
$$
\dfrac{\phi[t/x]}{\exists x \phi} \exists x i
$$
It simply says that we can deduce $\exists x \phi$ whenever we have $\phi[t/x]$ for some term \textit{t}
\begin{itemize}
\item Side-condition: \textit{t} needs to be free for \textit{x} in $\phi$
\end{itemize}
\end{small}
\end{myrule}

So basically, if $\forall$ is valid, then every single instance of the variable associated to the quantifier needs to be true, hence the similarity to conjunction ($\wedge$).

\begin{myrule}{Exists-elimination: $\exists x e$}{}
\begin{small}
\begin{center}
\includegraphics[width=0.28\textwidth]{images/rule_exists.png}
\end{center}
Like $\wedge e$, the $\exists e$ rule involves a case analysis (a sub-proof). The reasoning:
\begin{enumerate}
\item We know $\exists x \phi$ is true
\item So $\phi$ is true for at least one \textit{value} of \textit{x}
\item So we do a case analysis over all those possible values, writing $x_0$ as a generic value representing them all.
\item If assuming $\phi[x_0/x]$ allows us to prove some $\chi$ which doesn't mention $x_0$, then this $\chi$ must be true whichever $x_0$ makes $\phi[x_0/x]$ true
\end{enumerate}
That is precisely what the rule $\exists e$ allows us to deduce
\begin{itemize}
\item \textbf{Side-condition}: $x_0$ can't occur outside of its box (in particular, it cannot occur in $\chi$).
\end{itemize}
\end{small}
\end{myrule}

\begin{extra}{Compare $\exists$ rules to disjunction $\vee$ rules}{}
\begin{small}
The analogy between $\forall$ and $\wedge$ extends also to $\exists$ and $\vee$.\\

We saw that the rules for or-introduction were a sort of dual of those for and-elimination; to emphasise this, we could write them as 

\begin{center}
\includegraphics[width=0.5\textwidth]{images/and-or-rules.png}
\end{center}
Where \textit{k} can be chosen to be either 1 or 2. So basically, if the $\vee$ statement is true, then at least $\phi_1$ or $\phi_2$ is true. So we can deduce that if at least one of them are true, then there exists at least one case where the statement is true.\\

And for the $\vee e$ rule is like the $\exists e$, because you have the statement, and a case (sub-proof) that proves $\chi$, which allows you to get the statement $\chi$ that the case proved.
\end{small}
\end{extra}

So the fact that $\exists$ implies that at least one instance of the variable is true, then one thing OR another needs to be true. Hence the similarity to disjunction. They could all be true, but only one needs to be true.


\begin{example}{If $\forall x\phi$ then $\exists x\phi$}{}
\begin{small}
It is kind of obvious that if all are true, then at least one is true. But let's prove that.
\begin{logicproof}{1}
\forall x \phi & premise\\
\phi[x/x] &$\forall x e$ 1\\
\exists x \phi & $\exists x i$ 2
\end{logicproof}
\begin{itemize}
\item If $\phi[x/x]$ is true, then $\phi$ exists
\item Note that \textit{x} is free for \textit{x} in $\phi$ (in line 2, we have removed the quantifier) , and that $\phi[x/x]$ is simply $\phi$ again.
\end{itemize}
\end{small}
\end{example}


\begin{example}{Proving another case of $\forall x \vdash \exists x$ }{}
\begin{small}
{
\setlength\subproofhorizspace{2em}

\begin{logicproof}{1}
\forall x (Qx \rightarrow Rx) & premise\\
\exists (Px \wedge Qx) & premise\\
\begin{subproof}
\llap{$x_{0}\quad$} P(x_0) \wedge Q(x_0) & assumption\\
Q(x_0) \rightarrow R(x_0) & $\forall x e$ 1\\
Q(x_0) & $\wedge e_2$ 3\\
R(x_0) & $\rightarrow e$ 4,5\\
P(x_0) & $\wedge e_1$ 3\\
P(x_0) \wedge R(x_0) & $\wedge i$ 7,6\\
\exists (P(x) \wedge R(x)) & $\exists x i$ 8
\end{subproof}
\exists (P(x) \wedge R(x)) &$\exists x e$ 2, 3-9
\end{logicproof}
}
What happened? 
\begin{enumerate}
\item Well, we assumed that the statement of the $\exists x$ scope in line 2 was true (since it exists). 
\item Then we derived the statement of line 1 (without the $\forall$) from that assumption, and say it exists.
\item The $x_0$ was simply replaced by the $\exists$ introduction itself, in line 9.
\item Finally we are giving ourselves the conclusion of that sub-proof by using $\exists$ elimination. 
\end{enumerate}
\end{small}
\end{example}

\begin{example}{Proving the validity of a sequent with $\exists$ rules}{}
\begin{small}
We will be proving the validity of the sequent 
$$
\forall x(P(x)\rightarrow Q(x)), \exists x P(x)\vdash \exists x Q(x)
$$
{
\setlength\subproofhorizspace{2em}
\begin{logicproof}{1}
\forall x (P(x)\rightarrow Q(x)) & premise\\
\exists x P(x) & premise\\
\begin{subproof}
\llap{$x_{0}\quad$} P(x_0) & assumption\\
P(x_0) \rightarrow Q(x_0) &$\forall x e$ 1 \\
Q(x_0) &$\rightarrow e$ 4,3 \\
\exists x Q(x) &$\exists x i$ 5  
\end{subproof}
\exists x Q(x) &$\exists x e$ 2,3-6 
\end{logicproof}

}
\end{small}
\end{example}
\begin{extra}{Illegal version of the previous example}{}
\begin{small}

The following extra example 'proof' is nearly identical to the previous example, but \textbf{it is illegal}
{
\setlength\subproofhorizspace{2em}
\begin{logicproof}{1}
\forall x (P(x)\rightarrow Q(x)) & premise\\
\exists x P(x) & premise\\
\begin{subproof}
\llap{$x_{0}\quad$} P(x_0) & assumption\\
P(x_0) \rightarrow Q(x_0) &$\forall x e$ 1\\
Q(x_0) & $\rightarrow e$ 4,3
\end{subproof}
Q(x_0) 	& $\exists x e$ 2,3-5\\
\exists x Q(x) & $\exists x i$ 6
\end{logicproof}
}
Why is it illegal? Because line 6 allows the \textit{fresh parameter} $x_0$ to escape the scope of the box which declares it. This is not permitted.
\end{small}
\end{extra}

\begin{extra}{How $\exists$ can become $\forall$ through implication}{}
\begin{small}
$$
\exists x A(x)\rightarrow C
$$
is equivalent to 
$$
\forall x (A(x)\rightarrow C)
$$
Because the later is basically an \textit{if-statement}.
\begin{itemize}
\item If $A(x)$ isn't true, then $\forall(A(x)\rightarrow C)$ is still true
\item $A(x)\rightarrow C$ would be true 
\end{itemize}



 It says: For all x, if $A(x)$ then we get $C$. But implication allows $C$ to be true without $A(x)$, so therefore $A(x)\rightarrow C$ exists (that there exists a case where the statement is true), regardless whether $A(x)$ happens to be true or not.
\end{small}
\end{extra}

\end{quote}
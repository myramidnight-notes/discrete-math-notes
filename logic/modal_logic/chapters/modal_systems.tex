
\section{Modal Logic Systems}
\begin{itemize}
\item \href{https://www.youtube.com/watch?v=FacUHU_gjPw}{Modal Logic: Basics (video)}
\end{itemize}
\subsection{System \texttt{K} (Kripke semantics)}
\begin{quote}
\begin{definition}[Modal logic System \texttt{K}]
The most familiar logics in the modal family are constructed from a weak logic called \texttt{K} (after Saul Kripke). A variety of different systems may be developed using \texttt{K} as a foundation
\end{definition}

\subsubsection{Components of System K}
This system is essentially composed of 3 things
\begin{itemize}
\item Necessitation rule
\item Modal Distribution
\item Definition of Possibility
\end{itemize}
\begin{myrule}{Axiom N: Necessitation rule (NR)}{}
\begin{small}

If A is an axiom of \texttt{K}, then $\square A$ is as well
$$
A \rightarrow \square A 
$$
\begin{itemize}
\item It basically states that the laws of logic are necessary
\item Only if A is an Axiom of the system (has been inferred solely from the laws of logic)
\end{itemize}
\end{small}
\end{myrule}


\begin{myrule}{Axiom K: Modal Distribution (MDist) }{}
\begin{small}
$$
\square(A\rightarrow B) \rightarrow (\square A \rightarrow \square B)
$$
\textit{"It is necessary that (A implies B) implies that it is necessary that A implies that it is necessary that B"}

\begin{itemize}
\item If a implication statement is necessary, then the two participants of that implication statement are necessary.
\item We can basically distribute the necessity of that statement
\end{itemize}
\end{small}
\end{myrule}


\begin{myformula}{MDist for conjunction}{}
\begin{small}
$$
\square (A\wedge B) \leftrightarrow (\square A \wedge	\square B)
$$
\textit{"It is necessary that A and B"} therefore \textit{"it is necessary that A and it is necessary that B"}
\begin{itemize}
\item It goes both ways
\end{itemize}
\end{small}
\end{myformula}

\begin{myformula}{MDist for disjunction}{}

\begin{small}
$$
(\square A \vee \square B) \rightarrow \square ( A \vee B)
$$
\textit{"It is necessary that A or it is necessary B, implies that it is necessary that A or B"}
\begin{itemize}
\item It goes the opposite direction of the implication distribution.
\end{itemize}
\end{small}
\end{myformula}

\begin{myrule}{Definition of Possibility / Change of Modal Quantifier (CMQ)}{}
\begin{small}

$$
\lozenge A \equiv \neg\square\neg A
$$
\textit{"It is possible that A"} therefore \textit{"It is not necessarily the case that not A"} 

\end{small}
\end{myrule}

\begin{myformula}{Corollary of CMQ}{}
This also applies to $\square$
$$
\square A \equiv \neg\lozenge\neg A
$$
\textit{"It is necessarily the case that A"} therefore \textit{"It is not possible that not A"}
\end{myformula}

\begin{extra}{Important for Natural Deduction}{}
When using modal logic in natural deduction, $\lozenge$ essentially doesn't exist, and we use $\neg\square\neg$ instead. 
\end{extra}
\end{quote}
\subsection{System \texttt{D}}
\begin{quote}
It has everything that we had in system \texttt{K}, with the addition of Axiom \texttt{D}. It is the weakest system that we'll be looking at.

\begin{myrule}{Axiom D}{}
\begin{small}
$$
\square A \rightarrow \lozenge A
$$
\textit{"It is necessary that A implies that it is possible that A"}
\begin{itemize}
\item Whatever is necessary is possible
\item Corresponding relation is \textbf{serial}
\end{itemize}

\end{small}
\end{myrule}

\end{quote}
\subsection{System \texttt{T} (sometimes known as System \texttt{M})}
\begin{quote}
It is system \texttt{K} with axiom \texttt{M}.

\begin{myrule}{Axiom \texttt{M}}{}
\begin{small}

$$
\square A \rightarrow A
$$
\textit{"It is necessary that A implies A"}
\begin{itemize}
\item Whatever is necessary is the case
\item corresponding relation is \textbf{reflexive}
\end{itemize}
\end{small}
\end{myrule}

\begin{myformula}{Corollary to Axiom \texttt{M}}{}
\begin{small}
$$
A \rightarrow\lozenge A 
$$
\textit{"A implies that it is possible that A"}
\begin{itemize}
\item If A exists, then of course A is possible
\end{itemize}
\end{small}
\end{myformula}

\begin{extra}{System \texttt{T} replaces system \texttt{D}}{}
{\small  It is a stronger version of system \texttt{D} for modal logic. We don't need \texttt{D} in this case, we can just directly add \texttt{M} to \texttt{K}. }
\end{extra}
\end{quote}

\subsection{System \texttt{S4} (system \texttt{T} and axiom \texttt{4})}
\begin{quote}
\begin{myrule}{Axiom \texttt{4}}{}
\begin{small}
$$
\square A \rightarrow \square\square A
$$
\textit{"It is necessary that A implies that it is necessary that it is necessary that A"}
\begin{itemize}
\item Whatever is necessary is necessarily necessary
\item Corresponding relation is \textbf{transitive}
\end{itemize}
\end{small}
\end{myrule}

\begin{myformula}{Corollary to Axiom \texttt{4}}{}
\begin{small}
$$
\lozenge\lozenge A \rightarrow \lozenge A
$$
\textit{"It is possible that it is possible that A implies that it is possible that A "}
\end{small}
\end{myformula}

\end{quote}
\subsection{System \texttt{S5} (system \texttt{T} and axiom \texttt{5})}
\begin{quote}
Axiom \texttt{5} is a stronger version of Axiom \texttt{4}, so we don't need to have both. Just like how we don't need system D if we are using \texttt{T}
\begin{myrule}{Axiom \texttt{5}}{}
\begin{small}
$$
\lozenge A \rightarrow \square \lozenge A
$$
\textit{"it is possible that A implies that it is necessary that it is possible that A"}
\begin{itemize}
\item Whatever is possible is necessarily possible
\item Corresponding relation is \textbf{Euclidean} 
\end{itemize}
\end{small}
\end{myrule}
\begin{myformula}{Corollary to Axiom \texttt{5}}{}
\begin{small}
$$
\lozenge \square A \rightarrow \square A
$$
\textit{"It is possible that it is necessary that A implies that it is necessary that A"}
\begin{itemize}
\item Whatever is possibly necessary is necessary
\end{itemize}
\end{small}
\end{myformula}

\begin{myformula}{Consequence of \texttt{K}}{}
\begin{small}
$$
\square (\lozenge A \rightarrow \lozenge A)
$$
\textit{"It is necessary that it is possible that A implies that it is possible that A"}
\begin{itemize}
\item Necessarily, whatever is possible is possible
\item This just makes more sense than Axiom \texttt{5}
\end{itemize}
\end{small}
\end{myformula}

\end{quote}
\subsection{System \texttt{B} (system \texttt{T} and axiom \texttt{B})}
\begin{quote}


\begin{myrule}{Axiom \texttt{B}}{}
\begin{small}
$$
A \rightarrow \square \lozenge A
$$
\textit{"A implies that it is necessary that it is possible that A"}
\begin{itemize}
\item Whatever is the case is necessarily possible
\item Corresponding relation is \textbf{symmetric}
\end{itemize}
\end{small}
\end{myrule}

\begin{myformula}{Corollary to axiom B}{}
\begin{small}
$$
\lozenge \square A \rightarrow A
$$
\textit{"It is possible that it is necessary that A implies A"}
\begin{itemize}
\item If a necessary thing is possible, then that necessary thing exists
\end{itemize}
\end{small}
\end{myformula}

\begin{myformula}{Consequence of \texttt{M} and \texttt{K}}{}
\begin{small}
$$
\square (A\rightarrow \lozenge A)
$$
\textit{"It is necessary that A implies that it is possible that A"}
\begin{itemize}
\item Necessarily, whatever is the case is possible
\item This just makes more sense than Axiom \texttt{B}
\end{itemize}
\end{small}
\end{myformula}
\end{quote}


\subsection{System Recap}
\begin{quote}
\begin{extra}{Summarising the systems, mixing of axioms}{}
\begin{small}
The systems and their components

\begin{center}
\includegraphics[width=0.7\textwidth]{../images/modal-logic-diagram.png}
\end{center}
\begin{itemize}
\item System K = K
\begin{quote}
K is the basis for many systems in Modal logic
\end{quote}
\item System D = K + D
\item System T = K + M
\begin{quote}
M is a stronger version of D, so we don't need D if we're using M
\end{quote}
\item System S4 = K + M + axiom 4
\item System S5 = (K + M + axiom 5) or (K + M + B + axiom 4)
\begin{quote}
We can get axiom 4 from 5, so there is no need to restate it (so we just use 5)
\end{quote}
\item System B = K + M + B
\end{itemize}

Apparently the \texttt{S} systems are more controversial, you might not need to use them, but knowing about them is good. 
\end{small}
\end{extra}
\begin{extra}{Rules of Inference}{}
\begin{small}
The rules you will most likely be using from the previously mentioned systems
\begin{multicols}{2}

\begin{itemize}
\item Axiom N: Necessitation Rule (NR) 
\begin{align*}
A \rightarrow \square A
\end{align*}
\item Change of Modal Quantifier (CMQ)
\begin{align*}
\lozenge A \leftrightarrow \neg\square\neg A
\end{align*}
\item Axiom K: Distribution (MDist)
\begin{align*}
\square (A\rightarrow B)\rightarrow (\square A \rightarrow \square B)\\
\square (A\wedge B) \leftrightarrow (\square A \wedge \square B)\\
(\square A \vee \square B) \rightarrow \square (A \vee B)
\end{align*}
\item Axiom M (or T)
\begin{align*}
\square A \rightarrow A\\
A\rightarrow \lozenge A
\end{align*}
\item Axiom 4
\begin{align*}
\square A \rightarrow \square\square A\\
\lozenge\lozenge A \rightarrow \lozenge A
\end{align*}
\item Axiom 5 
\begin{align*}
\lozenge A \rightarrow \square \lozenge A \\
\lozenge\square A\rightarrow \square A
\end{align*}
\item Axiom B
\begin{align*}
A \rightarrow \square\lozenge A \\
\lozenge \square A \rightarrow A
\end{align*}
\end{itemize}
\end{multicols}
\end{small}
\end{extra}
\end{quote}
\begin{small}

\begin{definition}[Modal Logic ('háttarökfræði')]
Modal logic is a collection of formal systems developed to represent statements about necessity and possibility. 
\end{definition}
\begin{definition}[Axiom ('frumsenda/frumsetning')]
A statement or proposition which is regarded as being established, accepted, or self-evidently true. 
\end{definition}
\begin{definition}[Corollary ('fylgisetning')]
Corollary is an inference that follows directly from the proof of another proposition.
\end{definition}
\begin{definition}[Models]
We say a truth assignment \texttt{v} \textbf{models} a formula $\phi$
$$
\texttt{v} \Vdash \phi
$$
\end{definition}
\begin{definition}[Satisfiability]
A formula is satisfiable if it is true under some assignments of values to its variables.  The dual concept to satisfiability is \textbf{validity}
\end{definition}
\begin{definition}[Validity]
A formula is valid if every assignment of values to its variables makes the formula true (making it a tautology).
\end{definition}
\begin{definition}[Tautology]
A tautology is a formula or assertion that is true in every possible interpretation.
\end{definition}
\begin{definition}[Proof]
If there is a proof of $\phi$ then we write 
$$
\vdash \phi
$$
\end{definition}
\begin{definition}[Provable]
If $\phi$ is provable, then $\phi$ is true under all truth assignments. 
$$
\vdash \phi \text{ implies } \Vdash \phi
$$
\end{definition}
\begin{definition}[Completeness]
If $\phi$ is true under all truth assignments, then $\phi$ is provable.
$$
\Vdash \phi \text{ implies } \vdash \phi
$$
\end{definition}

\end{small}


\section{Basics modal language}
\begin{quote}
\begin{itemize}
\item \href{https://www.youtube.com/watch?v=sW0SQA386p0}{Quick introduction to Modal logic (video)}
\item \href{https://www.youtube.com/watch?v=4MO3AHeyL7s}{Modal propositional logic: Symbols and Syntax}
\item \href{https://plato.stanford.edu/entries/logic-modal/}{Modal Logic (Standford Encyclopedia of Philosophy)}
\end{itemize}
\begin{definition}[Truth assignment]
A truth assignment is a function 
$$
v: \mathcal{P} \rightarrow \lbrace T, F \rbrace
$$


\end{definition}

\begin{extra}{Symbols of Modal logic}{}
\begin{itemize}
\item[$\square$] "it is necessary that"\\
 in LaTeX: \texttt{{\textbackslash}square}
\item[$\lozenge$] "it is possible that", "it is not necessarily not" \\
 in LaTeX: \texttt{{\textbackslash}lozenge}
\end{itemize}
\end{extra}

So $\square$ is the $\forall$ in Modal logic, and $\lozenge$ is the $\exists$

\begin{example}{Modals}{}
A \textbf{Modal} is anything that qualifies the truth of a sentence
\begin{center}
John \_\_\_ happy
\end{center}
It might not be \textbf{necessary} that John is happy, or perhaps he might \textbf{possibly} become happy
\end{example}

\begin{extra}{Models}{}
A model \textit{M} = $\langle W, R, V\rangle$ consists of three objects:
\begin{itemize}
\item \textit{W} is a non-empty set. \textit{W} is called our \textbf{universe} and elements of \textit{W} are called \textbf{worlds}
$$
W \neq \emptyset
$$
\item \textit{R} is a relation on \textit{W}. \textit{R} is called our \textbf{accessibility relation}. The interpretation if $w_1$ is \textit{R}-related to $w_2$ then $w_1$ "knows about" $w_2$ and must consider it in making decisions about whether something is possible or necessary.
$$
R \subseteq W \times W
$$
\item \textit{V} is a function mapping the set of atomic propositional variables 
$\mathcal{P}$ to $P(W)$. The interpretation is the if \textit{P} is mapped into a set containing \textit{w} then \textit{w} thinks that the variable \textit{P} is true
$$
V: At \rightarrow \mathcal{P}(W)
$$
It allows us to specify which atomic propositions are true at which states
\end{itemize}
\end{extra}

\begin{extra}{Truth of Modal formulas}{}
Truth at a state in a model: $M, w \Vdash \phi$  
\begin{itemize}
\item $M, w \Vdash p$ iff $w \in V(p)$\\
$p$ is true at state $w$, iff our evaluation function \textit{V} says its true
\item $M, w \Vdash \neg\phi$ iff $M, w\not\Vdash \phi$\\
$p$ isn't true if it's not true for \textit{w}
\item $M, w \Vdash \phi\wedge\psi$ iff $M, w \Vdash \phi$ and $M,w\Vdash \psi$\\
$\phi$ and $\psi$ need to be true individually in \textit{w} for them to be true together
\item $M, w \Vdash \square\phi$ iff for all $v\in W$, if $wRv$ then $M, v\Vdash \phi$\\
$\phi$ is \textbf{necessary} if $\phi$ is true in all accessible states \textit{v}
\item $M, w \Vdash \lozenge \phi$ iff there is a $v\in W$ such that $wRv$ and $M,v\Vdash \phi$\\
$\phi$ is \textbf{possible} if $\phi$ is true in at least one accessible state \textit{v}
\end{itemize}
\end{extra}

\end{quote}

# Logic Notes!
Similar to how I did the notes for discrete mathematics, I worked on these as separate files, then created a large file containing all of them. It is quite easy with the imports, so everything can stay relative (image imports being as I defined it for the sub-notes, and I don't have to change it when importing in different directories)

I cannot promise that I update them equally (if working on sub-notes, and forgetting to recompile the combined notes), that is why I always have the "last modified" on the title pages.
## Index
* [Combined Logic Notes](glosur_logic_cs.pdf) (pdf)
    * [Propositional Logic Notes](prop_logic/glosur_propositional_logic_cs.pdf)(pdf)
        * Logical equivalences
        * includes Natural Deduction and Proofs
        * Normal form 
        * Validity, Soundness and Completeness
    * [Predicate Logic Notes](pred_logic/glosur_predicate_logic_cs.pdf)(pdf)
    * [Modal Logic](modal_logic/glosur_modal_logic_cs.pdf)(pdf) 
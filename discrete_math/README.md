Þessar tilteknu glósur eru alfarið á Íslensku, því það var kennt á því tungumáli. Svo tók ég smá úrbeint úr bókinni hér fyrir neðan sem var lýsingin á hvernig þú gætir notað Strjála Stærðfræði

## Discrete Mathematics (Strjál Stærðfræði)
The course book that I used was _Discrete Mathematics and its applications_ by _Kenneth H. Rosen_, which was usually refered to as __KHR__. I own the 8th edition, international student edition. 

> Love this book. All the math you might use for computer science gathered into a single book, and it is quite obvious that it was created with Computer Scienece in mind.

### Kind of problems solved using discrete mathmetics
> This was taken from the intro of the book. page _xix_.
* How many ways are there to choose a valid password on a computer system?
* What is the probability of winning a lottery?
* Is there a link between two computers in a network?
* How can I identify spam e-mail messages?
* How can I encrypt a message so that no unintended recipient can read it?
* What is the shortest path between two cities using a transportation system?
* How can a list of integers be sorted so that the integers are in increasing order?
* How many steps are required to do such a sorting?
* How can it be proven that a sorting algorithm correctly sorts a list?
* How can a circuit that adds two integers be designed?
* How many valid Internet addresses are there?

## PDF notes index

* ### [Strjál Stærðfræði glósur](glosur_str1.pdf)
    >Stakir kaflar (sem ég notaði þegar ég var að vinna í hverjum kafla fyrir sig, þetta er allt sameinað í PDF hér að ofan), Þetta er númerað eftir köflunum í bókinni Discrete Mathematics  eftir Kenneth H. Rosen (KHR)
    * [__kafli 1__: Yrðingar Rökfræði ('propositional logic')](khr1/glosur_khr_1.pdf)
    * [__kafli 2__: Mengjafræði ('set theory')](khr2/glosur_khr_2.pdf)
    * [__kafli 4__: Talnafræði ('number theory')](khr4/glosur_khr_4.pdf)
    * [__kafli 5__: Þrepun ('induction')](khr5/glosur_khr_5.pdf)
    * [__kafli 6__: Talningarfræði ('counting')](khr6/glosur_khr_6.pdf)
    * [__kafli 9__: Vensl / Tengsl ('relation')](khr9/glosur_khr_9.pdf)
    * [__kafli 10__: Netafræði](khr10/glosur_khr_10.pdf)